package another.planet.clutch.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;
import another.planet.clutch.util.Construct;

/**
 * Copyright 2014 Deblur. All rights reserved.
 *
 * @author Shin Gwangsu (maruroid@gmail.com)
 * @since 14. 12. 5.
 */
public class DefaultClutchListAdapter extends ArrayAdapter<Construct.MainCard> {

    private Context context;

    public DefaultClutchListAdapter(Context context) {
        super(context, 0);
        this.context = context;
    }
}
