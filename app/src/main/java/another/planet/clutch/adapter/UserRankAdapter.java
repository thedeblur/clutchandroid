package another.planet.clutch.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import another.planet.clutch.R;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.UnitSizeUtil;
import another.planet.clutch.widget.RoundedImageView;

import java.util.ArrayList;

/**
 * Copyright 2014 Deblur. All rights reserved.
 *
 * @author Shin Gwangsu (maruroid@gmail.com)
 * @since 14. 12. 12.
 */
public class UserRankAdapter extends ArrayAdapter<Construct.UserBallonRank> {

    private Context context;
    private ArrayList<Construct.UserBallonRank> array;

    public UserRankAdapter(Context context, int resource, ArrayList<Construct.UserBallonRank> array) {
        super(context, resource, array);

        this.context = context;
        this.array = array;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = new ViewHolder();

        convertView = View.inflate(context, R.layout.item_user_rank_row, null);

        holder.tvUserName = (TextView)convertView.findViewById(R.id.text_user_name);
        holder.tvBallonCount = (TextView)convertView.findViewById(R.id.text_user_ballon_count);
        holder.ivUserProfile = (RoundedImageView)convertView.findViewById(R.id.img_user_profile);

        if(convertView != null) convertView.getTag();
        else convertView.setTag(holder);

        Construct.UserBallonRank rankList = array.get(position);

        holder.tvUserName.setText(rankList.name);
        holder.tvBallonCount.setText(UnitSizeUtil.numberDecimalFormat(rankList.star));
        ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(rankList.profileURL, 300), holder.ivUserProfile, ImageLoaderUtil.imageLoaderOptions());

        return convertView;
    }

    @Override
    public int getCount() {
        return array.size();
    }

    public class ViewHolder{
        public TextView tvUserName, tvBallonCount;
        public RoundedImageView ivUserProfile;
    }
}
