package another.planet.clutch.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import another.planet.clutch.R;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;

import java.util.ArrayList;

/**
 * Created by Marutian on 2014. 4. 17..
 */
public class ImagesAdapter extends PagerAdapter {

    private Context context;
    private int pageCount;
    private ArrayList<String> fileList;

    public ImagesAdapter(Context context, ArrayList<String> fileList) {
        this.context = context;
        this.fileList = fileList;

        if (fileList != null) {
            this.pageCount = fileList.size();
        } else {
            this.pageCount = 0;
        }

        ImageLoaderUtil.setDefImageConfig(context);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_pager_imageview, null);
        ImageView imageView = (ImageView) v.findViewById(R.id.image);
        ImageLoaderUtil.imageLoader.displayImage(fileList.get(position), imageView, ImageLoaderUtil.imageLoaderOptions());

        container.addView(v);
        return (v);
    }

    /* Set ViewPager Properties */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return pageCount; //페이지수
    }

    @Override
    public void startUpdate(View arg0) {

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

}