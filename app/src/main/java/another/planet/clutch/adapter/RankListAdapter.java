package another.planet.clutch.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.UnitSizeUtil;
import another.planet.clutch.widget.NumbericImageView;

import java.util.ArrayList;

/**
 * Copyright 2014 Deblur. All rights reserved.
 *
 * @author Shin Gwangsu (maruroid@gmail.com)
 * @since 14. 12. 11.
 */
public class RankListAdapter extends ArrayAdapter<Construct.RankList> {

    Context context;
    ArrayList<Construct.RankList> array;

    public RankListAdapter(Context context, int textViewResourceId, ArrayList<Construct.RankList> rankArrayList) {
        super(context, textViewResourceId);
        this.context = context;
        this.array = rankArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = new ViewHolder();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Construct.RankList rankList = array.get(position);

        if (position < 10) {
            convertView = inflater.inflate(R.layout.item_rank_top10_row, null);

            holder.tvRankNumber = (NumbericImageView)convertView.findViewById(R.id.rank_no);
            holder.tvArtistName = (TextView)convertView.findViewById(R.id.artist_name);
            holder.tvMyBallon = (TextView)convertView.findViewById(R.id.my_ballon_count);
            holder.tvTotalBallon = (TextView)convertView.findViewById(R.id.total_ballon_count);
            holder.ivArtistImage = (ImageView)convertView.findViewById(R.id.artist_image);

            ((TextView)convertView.findViewById(R.id.rank_no_full)).setText(Integer.toString(position + 1) + "위: ");

            DisplayMetrics dm = context.getResources().getDisplayMetrics();

            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(rankList.profile,dm.widthPixels/2), holder.ivArtistImage, ImageLoaderUtil.imageLoaderOptions());

            holder.tvRankNumber.setNumberTextStyle(NumbericImageView.RANK_NUMBER_TOP);

            if(position == 9){
                holder.tvRankNumber.setNumberText("0"); //스타일 RANK_NUMBER_TOP에서의 0 이미지는 10으로 되어있으니 No.10 은 0을 쓰도록 하자
            }else{
                holder.tvRankNumber.setNumberText(Integer.toString(position + 1));
            }

        } else {
            convertView = inflater.inflate(R.layout.item_rank_row, null);

            holder.tvRankNumber = (NumbericImageView)convertView.findViewById(R.id.rank_no);
            holder.tvArtistName = (TextView)convertView.findViewById(R.id.artist_name);
            holder.tvMyBallon = (TextView)convertView.findViewById(R.id.my_ballon_count);
            holder.tvTotalBallon = (TextView)convertView.findViewById(R.id.total_ballon_count);
            holder.ivArtistImage = (ImageView)convertView.findViewById(R.id.artist_image);

            holder.tvRankNumber.setNumberTextStyle(NumbericImageView.RANK_NUMBER_DEFAULT);
            holder.tvRankNumber.setNumberText(Integer.toString(position + 1));
        }

        if(convertView == null) convertView.setTag(holder);
        else convertView.getTag();


        holder.tvArtistName.setText(rankList.name);
        holder.tvTotalBallon.setText(UnitSizeUtil.numberDecimalFormat(rankList.star));
        holder.tvMyBallon.setText(UnitSizeUtil.numberDecimalFormat((int)(Math.random() * 100)));

        convertView.findViewById(R.id.btn_vote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "풍선추가", Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return array.size();
    }


    private class ViewHolder {
        private TextView tvArtistName, tvTotalBallon, tvMyBallon;
        private ImageView ivArtistImage;
        private NumbericImageView tvRankNumber;
    }
}
