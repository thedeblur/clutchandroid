package another.planet.clutch.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import another.planet.clutch.R;
import another.planet.clutch.activity.MainDetailActivity;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;

/**
 * Copyright 2014 Deblur. All rights reserved.
 *
 * @author Shin Gwangsu (maruroid@gmail.com)
 * @since 14. 12. 5.
 */
public class FavoriteClutchAdapter  extends ArrayAdapter<Construct.MainCard> {

    private Context context;
    private Construct.MainCard mc;

    public FavoriteClutchAdapter(Context context) {
        super(context, 0);
        this.context  = context;
    }

    @Override
    public View getView(int oriPosition, View convertView, ViewGroup parent) {
        LayoutInflater inflate = LayoutInflater.from(getContext());

        ViewHolder holder = new ViewHolder();

        //헤더로 못하길래 대충 꾸겨넣은 0번,1번포지션. 역할은 다른거 없고 단순 빈공간 ^^
        if (oriPosition == 0) {
            convertView = inflate.inflate(R.layout.item_blank, null);
        } else if (oriPosition == 1) {
            convertView = inflate.inflate(R.layout.item_blank, null);
        } else {
            //여기서부터 진짜 아이템을 사용해야하는데, position 을 기본적으로 -2 로 해서 그냥 기존의 포지션 개념처럼 사용하면됨
            //@param position
            final int position = oriPosition - 2;
            mc = getItem(position);

            convertView = inflate.inflate(R.layout.item_main, null);

            convertView.findViewById(R.id.filter_card).setVisibility(View.GONE);
            convertView.findViewById(R.id.content_card).setVisibility(View.VISIBLE);

            holder.ivCard = (ImageView) convertView.findViewById(R.id.card_image);
            holder.ivProfile = (ImageView) convertView.findViewById(R.id.profile_image);
            holder.tvMent = (TextView) convertView.findViewById(R.id.text_card_ment);
            holder.tvLikeCount = (TextView) convertView.findViewById(R.id.text_card_like_count);
            holder.tvClutchCunt = (TextView) convertView.findViewById(R.id.text_card_clutch_count);
            holder.tvCommentCount = (TextView) convertView.findViewById(R.id.text_card_comment_count);
            holder.tvProfileName = (TextView) convertView.findViewById(R.id.profile_name);
            holder.tvBookName = (TextView) convertView.findViewById(R.id.cb_name);

            convertView.setTag(holder);

            holder.tvMent.setText(mc.mentMessage);
            holder.tvLikeCount.setText(String.valueOf(mc.likeCount));
            holder.tvClutchCunt.setText(String.valueOf(mc.clutchCount));
            holder.tvCommentCount.setText(String.valueOf(mc.commentCount));
            holder.tvProfileName.setText(mc.profileName);
            holder.tvBookName.setText(mc.cb_name);

            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(mc.content_pictures.get(0), 300), holder.ivCard, ImageLoaderUtil.imageLoaderOptions());
            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(mc.profileImage, 100), holder.ivProfile, ImageLoaderUtil.imageLoaderOptions());

            holder.ivCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, MainDetailActivity.class);
                    intent.putExtra("postId", getItem(position).postId);
                    ((Activity)context).startActivity(intent);
                }
            });

            holder.ivCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Vibrator vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(50);
                    return true;
                }
            });
        }

        return convertView;
    }

    public class ViewHolder {
        public TextView tvMent, tvLikeCount, tvClutchCunt, tvCommentCount, tvProfileName, tvBookName;
        public ImageView ivCard, ivProfile;
    }

}
