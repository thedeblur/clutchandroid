package another.planet.clutch.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.activity.MainDetailActivity;
import another.planet.clutch.activity.OtherClutchActivity;
import another.planet.clutch.dialog.FilterDialog;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import another.planet.clutch.widget.RoundedImageView;

/**
 * Copyright 2014 Deblur. All rights reserved.
 *
 * @author Shin Gwangsu (maruroid@gmail.com)
 * @since 14. 12. 5.
 */
public class FilterClutchListAdapter  extends ArrayAdapter<Construct.MainCard> {

    private Context context;
    private Construct.MainCard mc;
    private LinearLayout sortNew, sortHit;
    private Handler handler;

    public FilterClutchListAdapter(Context context, Handler handler) {
        super(context, 0);
        this.context  = context;
        this.handler = handler;
    }

    @Override
    public View getView(final int oriPosition, View convertView, ViewGroup parent) {
        LayoutInflater inflate = LayoutInflater.from(getContext());

        final ViewHolder holder = new ViewHolder();

        if (oriPosition == 0) {

            convertView = inflate.inflate(R.layout.item_filter, null);

            convertView.findViewById(R.id.filter).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FilterDialog dialogFilter = new FilterDialog(context);
                    dialogFilter.show();
                }
            });

            sortNew = (LinearLayout) convertView.findViewById(R.id.tab_sort_new);
            sortHit = (LinearLayout) convertView.findViewById(R.id.tab_sort_hit);

            selectTab(TaskUtil.getSort(context));

            sortNew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "최신순", Toast.LENGTH_SHORT).show();
                    int sort = 1;
                    selectTab(sort);
                    TaskUtil.saveSort(context, sort);
                    handler.sendEmptyMessage(0);

                }
            });

            sortHit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "인기순", Toast.LENGTH_SHORT).show();
                    int sort = 0;
                    selectTab(sort);
                    TaskUtil.saveSort(context, sort);
                    handler.sendEmptyMessage(0);
                }
            });
        } else{

            convertView = inflate.inflate(R.layout.item_card, null);

            holder.ivCard = (ImageView) convertView.findViewById(R.id.card_image);
            holder.ivProfile = (RoundedImageView) convertView.findViewById(R.id.profile_image);
            holder.ivPlayVideo = (ImageView) convertView.findViewById(R.id.play_video);
            holder.tvMent = (TextView) convertView.findViewById(R.id.text_card_ment);
            holder.tvLikeCount = (TextView) convertView.findViewById(R.id.text_card_like_count);
            holder.tvClutchCunt = (TextView) convertView.findViewById(R.id.text_card_clutch_count);
            holder.tvCommentCount = (TextView) convertView.findViewById(R.id.text_card_comment_count);
            holder.tvProfileName = (TextView) convertView.findViewById(R.id.profile_name);
            holder.tvBookName = (TextView) convertView.findViewById(R.id.cb_name);

            if(convertView == null) convertView.setTag(holder);
            else convertView.getTag();

            final int position = oriPosition - 1;  // 필터 카드가 이미 그리드뷰의 0번 인덱스를 쳐먹고 있으므로 1 씩 빼서 포지션을 맞춰주자
            mc = getItem(position);

            holder.tvMent.setText(mc.mentMessage);
            holder.tvLikeCount.setText(Integer.toString(mc.likeCount));
            holder.tvClutchCunt.setText(Integer.toString(mc.clutchCount));
            holder.tvCommentCount.setText(Integer.toString(mc.commentCount));
            holder.tvProfileName.setText(mc.profileName);
            holder.tvBookName.setText(mc.cb_name);

            DisplayMetrics dm = context.getResources().getDisplayMetrics();


            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(mc.content_pictures.get(0), dm.widthPixels/2), holder.ivCard, ImageLoaderUtil.imageLoaderOptions());
            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(mc.profileImage, 100), holder.ivProfile, ImageLoaderUtil.imageLoaderOptions());

            if (mc.contentType == 1) holder.ivPlayVideo.setVisibility(View.VISIBLE);

            holder.ivCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, MainDetailActivity.class);
                    intent.putExtra("postId", getItem(position).postId);
                    context.startActivity(intent);
                }
            });

            holder.ivCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Vibrator vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(50);
                    return true;
                }
            });

            holder.ivPlayVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri uri = Uri.parse(mc.content_link);
                    Intent i = new Intent(Intent.ACTION_VIEW, uri);
                    context.startActivity(i);
                }
            });

             convertView.findViewById(R.id.writer_info).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, OtherClutchActivity.class);
                    intent.putExtra("cbId", mc.cb_id);
                    intent.putExtra("cbName", mc.cb_name);
                    intent.putExtra("userId", mc.userId);
                    context.startActivity(intent);
                }
            });

        }
        return convertView;
    }

    // 현재 보이는 탭을 포커스처리함
    public void selectTab(int position) {
        switch (position) {
            case 0:
                initTabImage();
                sortHit.setSelected(true);
                break;
            case 1:
                initTabImage();
                sortNew.setSelected(true);
                break;
        }
    }

    public void initTabImage() {
        //탭의 배경을 초기화한다.
        sortNew.setSelected(false);
        sortHit.setSelected(false);
    }

    public class ViewHolder {
        public TextView tvMent, tvLikeCount, tvClutchCunt, tvCommentCount, tvProfileName, tvBookName;
        public ImageView ivCard, ivProfile, ivPlayVideo;
    }

}
