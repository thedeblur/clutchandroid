package another.planet.clutch.adapter;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.util.Construct;

import java.util.ArrayList;

/**
 * Copyright 2014 Deblur. All rights reserved.
 *
 * @author Shin Gwangsu (maruroid@gmail.com)
 * @since 14. 12. 4.
 */
public class FavoriteSpinnerAdapter extends ArrayAdapter<Construct.ArtistList> {

    private Context context;
    private Handler handler;
    private ArrayList<Construct.ArtistList> array;
    private Message msg;
    private Bundle data;

    public FavoriteSpinnerAdapter(Context context, Handler handler, ArrayList<Construct.ArtistList> array) {
        super(context, 0);
        this.context = context;
        this.handler = handler;
        this.array = array;

        msg = new Message();
        data = new Bundle();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_favorite_spinner_row, null);

        ((TextView) convertView.findViewById(R.id.name)).setText(array.get(position).name);
        ((View) convertView.findViewById(R.id.list_item)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, array.get(position).name, Toast.LENGTH_SHORT).show();
                data.putString("selected_name", array.get(position).name);
                msg.setData(data);
                handler.sendMessage(msg);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return array.size();
    }
}
