package another.planet.clutch.util;

import android.content.Context;
import android.graphics.Bitmap;
import another.planet.clutch.R;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 * Copyright 2014 Deblur. All rights reserved.
 *
 * @author Shin Gwangsu (maruroid@gmail.com)
 * @since 14. 12. 12.
 */
public class ImageLoaderUtil {

    public static Context mContext;
    public static ImageLoader imageLoader;
    public static DisplayImageOptions options;
    public static int MEMORY_CLEAR = 0;
    public static int DISC_CLEAR = 1;
    public static int ALL_CACHE_CLEAR = 2;

    public static ImageLoader setImageLoader(Context c) {
        mContext = c;

        if (ImageLoader.getInstance().isInited()) {
            ImageLoader.getInstance().destroy();
        }
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs()
                .build();

        ImageLoader.getInstance().init(config);

        options = new DisplayImageOptions.Builder()
                //.showImageOnLoading(R.drawable.unknown_type1)
                //.showImageForEmptyUri(R.drawable.unknown_type1)
                .showImageOnFail(R.drawable.unknown_type1)
                .cacheInMemory(true)
                .cacheOnDisc(false)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();

        return imageLoader;
    }

    public static void setStaticImageLoader(Context c) {
        mContext = c;

        if (ImageLoader.getInstance().isInited()) {
            ImageLoader.getInstance().destroy();
        }
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs()
                .build();

        ImageLoader.getInstance().init(config);

        options = imageLoaderOptions();

        imageLoader = ImageLoader.getInstance();
    }

    public static void setDefImageConfig(Context c) {
        mContext = c;
        reinitImageLoader();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs()
                .build();

        ImageLoader.getInstance().init(config);
    }

    public static void setPickImageConfig(Context c) {
        mContext = c;
        reinitImageLoader();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .threadPoolSize(4)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .memoryCacheExtraOptions(96, 96)
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
    }

    public static DisplayImageOptions imageLoaderOptions() {

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.unknown_type1)
                .showImageForEmptyUri(R.drawable.unknown_type1)
                .showImageOnFail(R.drawable.unknown_type1)
                .cacheInMemory(true)
                .cacheOnDisc(false)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        imageLoader = ImageLoader.getInstance();

        return options;
    }

    public static String getImageURL(String metaDataID) {
        return ClutchUtil.ENDPOINT + "/images/" + metaDataID;
    }

    public static String getImageURL(String metaDataId, int width, int height) {
        return ClutchUtil.ENDPOINT + "/images/" + metaDataId + "/" + Integer.toString(width) + "/" + Integer.toString(height);
    }

    public static String getImageURL(String metaDataId, int width){
        return ClutchUtil.ENDPOINT + "/images/" + metaDataId + "/" + Integer.toString(width) + "/" + Integer.toString(width);
    }

    public static String getImageURL(String metaDataId, float width){
        return ClutchUtil.ENDPOINT + "/images/" + metaDataId + "/" + Float.toString(width) + "/" + Float.toString(width);
    }

    public static void cleanImageCache(int clearType) {
        if (clearType == MEMORY_CLEAR) {
            imageLoader.clearMemoryCache();
        } else if (clearType == DISC_CLEAR) {
            imageLoader.clearDiscCache();
        } else if (clearType == ALL_CACHE_CLEAR) {
            imageLoader.clearDiscCache();
            imageLoader.clearMemoryCache();
        }
    }

    public static void reinitImageLoader() {
        if (ImageLoader.getInstance().isInited()) {
            ImageLoader.getInstance().destroy();
        }
    }
}
