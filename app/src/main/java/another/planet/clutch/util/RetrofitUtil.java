package another.planet.clutch.util;

import retrofit.mime.TypedString;

/**
 * Created by toast on 2014. 2. 21..
 */
public class RetrofitUtil {
    public static TypedString T(String string) {
        return new TypedString(string);
    }

    public static TypedString T(int i) {
        return new TypedString(String.valueOf(i));
    }

    public static TypedString T(long l) {
        return new TypedString(String.valueOf(l));
    }

    public static TypedString T(boolean b) {
        return new TypedString(b ? "true" : "false");
    }
}
