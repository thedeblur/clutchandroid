package another.planet.clutch.util;

import android.graphics.drawable.BitmapDrawable;

import java.util.ArrayList;

/**
 * Created by toast on 2014. 4. 6..
 */
public class Construct {

    public static class MainCard {
        public MainCard(String postId, String mentMessage, int likeCount,
                        int clutchCount, int commentCount, String profileName,
                        String cb_name, String profileImage, ArrayList<String> content_pictures,
                        String userId, int contentType, String content_link, String cb_id, String date,
                        int content_picture_width, int content_picture_height) {
            this.postId = postId;
            this.mentMessage = mentMessage;
            this.likeCount = likeCount;
            this.clutchCount = clutchCount;
            this.commentCount = commentCount;
            this.profileName = profileName;
            this.cb_name = cb_name;
            this.profileImage = profileImage;
            this.content_pictures = content_pictures;
            this.userId = userId;
            this.contentType = contentType;
            this.content_link = content_link;
            this.cb_id = cb_id;
            this.date = date;
            this.content_picture_width = content_picture_width;
            this.content_picture_height = content_picture_height;
        }

        public String postId;
        public String mentMessage;
        public int likeCount;
        public int clutchCount;
        public int commentCount;
        public String profileName;
        public String cb_name;
        public String profileImage;
        public String imageURL;
        public String userId;
        public String cb_id;
        public int contentType;
        public ArrayList<String> content_pictures;
        public String content_link;
        public String date;
        public int content_picture_width;
        public int content_picture_height;
    }

    public static class ClutchBook {
        public ClutchBook(
                String cb_id, String name, String description,
                ArrayList<String> last_photos, int clutches_count, int followers_count) {
            this.cb_id = cb_id;
            this.name = name;
            this.description = description;
            this.last_photos = last_photos;
            this.clutches_count = clutches_count;
            this.followers_count = followers_count;
        }

        public String cb_id;
        public String name;
        public String description;
        public ArrayList<String> last_photos;
        public int clutches_count;
        public int followers_count;
    }

    public static class Profile {
        public Profile(
                String id, String name, String status_message, String picture, int followers_count,
                int followings_count, int liked_count, int clutch_count,
                int cb_count, ArrayList<String> last_photos, int isFollowing) {
            this.id = id;
            this.name = name;
            this.status_message = status_message;
            this.picture = picture;
            this.followers_count = followers_count;
            this.followings_count = followings_count;
            this.liked_count = liked_count;
            this.clutch_count = clutch_count;
            this.cb_count = cb_count;
            this.last_photos = last_photos;
            this.isFollowing = isFollowing;
        }

        public String id;
        public String name;
        public String status_message;
        public String picture;
        public int followers_count;
        public int followings_count;
        public int liked_count;
        public int clutch_count;
        public int cb_count;
        public ArrayList<String> last_photos;
        public int isFollowing;
    }

    public static class RecentItems {
        public RecentItems(String bookId, String bookName, BitmapDrawable bookCover) {
            this.bookId = bookId;
            this.bookName = bookName;
            this.bookCover = bookCover;
        }

        public String bookId;
        public String bookName;
        public BitmapDrawable bookCover;
    }

    public static class TagItems {
        public TagItems(String bookId, String bookName, String bookCover) {
            this.bookId = bookId;
            this.bookName = bookName;
            this.bookCover = bookCover;
        }

        public String bookId;
        public String bookName;
        public String bookCover;
    }

    public static class RankList {
        public RankList(String rank, String name, String profile, int star, String artistId) {
            this.rank = rank;
            this.name = name;
            this.profile = profile;
            this.star = star;
            this.artistId = artistId;
        }

        public String rank, name, profile, artistId;
        public int star;
    }

    public static class UserBallonRank {
        public UserBallonRank(String name, String userId, String profileURL, String artistId, int star) {
            this.name = name;
            this.userId = userId;
            this.profileURL = profileURL;
            this.artistId = artistId;
            this.star = star;
        }

        public String name, userId, profileURL, artistId;
        public int star;
    }

    public static class ArtistList {
        public ArtistList(String artistId, String name, String profileURL, int category, int starCount, int weeklyStarCount) {
            this.artistId = artistId;
            this.name = name;
            this.profileURL = profileURL;
            this.category = category;
            this.starCount = starCount;
            this.weeklyStarCount = weeklyStarCount;
        }

        public String artistId;
        public String name;
        public String profileURL;
        public int category;
        public int starCount;
        public int weeklyStarCount;
    }

    public static class Notification {
        public Notification(String ownerId, String message, String action, String actionBundle, String actionBundleName, String date) {
            this.ownerId = ownerId;
            this.message = message;
            this.action = action;
            this.actionBundle = actionBundle;
            this.date = date;
        }

        public String ownerId;
        public String message;
        public String action;
        public String actionBundle;
        public String actionBundleName;
        public String date;
    }
}
