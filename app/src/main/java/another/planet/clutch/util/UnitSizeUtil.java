package another.planet.clutch.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import java.text.DecimalFormat;

/**
 * Created by Marutian on 2014. 4. 10..
 */
public class UnitSizeUtil {

    public static int px2dp(Context context, int px) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, px, metrics);
    }

    public static int dp2px(Context context, float dp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

    public static String numberDecimalFormat(String number){
        DecimalFormat df = new DecimalFormat("###,###");
        return String.format("%s", df.format(number));
    }

    public static String numberDecimalFormat(int number){
        DecimalFormat df = new DecimalFormat("###,###");
        return String.format("%s", df.format(number));
    }
}
