package another.planet.clutch.util;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.*;
import retrofit.mime.TypedFile;

import java.util.ArrayList;
import java.util.List;

/**
 * ClutchUtil - 클러치 API 서버와의 통신을 담당한다.
 * 모든 통신은 비동기로 행해지므로 콜백 함수를 작성해야만 한다.
 */
public class ClutchUtil {

    private static boolean DEBUG = true;
    public static String ENDPOINT = "http://" +
            "thedeblur.com:1129";
    private static final RestAdapter mRestAdapter = new RestAdapter.Builder()
            .setEndpoint(ENDPOINT)
            .build();

    public static class Status {
        public Integer code;
        public String message;

        public static final int SUCCEED = 200;
        public static final int FAILED = 400;
        public static final int FAILED_NO_PERMISSION = 401;
        public static final int FAILED_TOKEN_INVALID = 402;
        public static final int FAILED_LESS_PARAMETER = 411;
        public static final int INVALID_OAUTH = 501;
        public static final int ID_NOT_FOUND = 502;
        public static final int RESULT_NOT_FOUND = 503;
        public static final int FAILED_DB_CONNECTION = 1000;
        public static final int FAILED_ACCOUNT_INVALID = 1001;
        public static final int FAILED_ALREADY_EXISTS = 1002;
        public static final int NOT_ENOUGH_BALLON = 1003;
        public static final int LESS_PARAMETER = 1010;


        public static String check(int status) {
            switch (status) {
                case SUCCEED:
                    return "";
                case FAILED:
                    return "서버 내부 오류";
                case FAILED_NO_PERMISSION:
                    return "권한 부족";
                case FAILED_TOKEN_INVALID:
                    return "Auth토큰 불일치";
                case FAILED_LESS_PARAMETER:
                    return "파라미터 부족";
                case INVALID_OAUTH:
                    return "인증 오류";
                case ID_NOT_FOUND:
                    return "해당 항목을 찾을 수 없음";
                case RESULT_NOT_FOUND:
                    return "검색 결과를 찾을 수 없";
                case FAILED_DB_CONNECTION:
                    return "데이터베이스 내부 오류";
                case FAILED_ACCOUNT_INVALID:
                    return "계정 불일치";
                case FAILED_ALREADY_EXISTS:
                    return "아이디가 이미 존재";
                case NOT_ENOUGH_BALLON:
                    return "풍선 부족";
                case LESS_PARAMETER:
                    return "필수 필드 누락";
            }
            return "알수 없는 오류 : " + String.valueOf(status);
        }
    }

    // 모든 API 호출 결과의 슈퍼클래스
    public static class Result {
        public Status status;

        public Integer getStatusCode() {
            return status.code;
        }

        public String getStatusMessage() {
            return status.message;
        }
    }

    // 유저 프로필
    public static class Profile {
        public String id;
        public String name;
        public String status_message;
        public String picture;
        public int followers_count;
        public int followings_count;
        public int liked_count;
        public int clutch_count;
        public int cb_count;
        public ArrayList<String> last_photos;
        public int isFollowing;
    }

    // 클러치 변수
    public static class ClutchArray {
        public String clutchId;
        public String cb_id;
        public String cb_name;
        public String userId;
        public String userName;
        public String userPhoto;
        public String date;
        public int likeCount;
        public int shared_times;
        public int commentCount;
        public String content_body;
        public int content_type;
        public ArrayList<String> content_pictures;
        public String content_link;
        public int content_picture_width;
        public int content_picture_height;
        public boolean isLiking;
    }

    //코멘트 변수
    public static class CommentArray {
        public String commentid;
        public String userId;
        public String userName;
        public String userPhoto;
        public String body;
        public int likeCount;
        public int unlikeCount;
        public String date;
    }

    //랭킹 변수
    public static class RankingArray {
        public String rank;
        public String name;
        public String profile;
        public int star;
        public String artistId;
        public String _id;
    }

    public static class ClutchBookArray {
        public String cb_id;
        public String name;
        public String description;
        public ArrayList<String> last_photos;
        public int clutches_count;
        public int followers_count;
        public int isFollowing;
    }

    public static class ArtistArray {
        public String _id;
        public String name;
        public String profile_picture;
        public int category;
        public int starCount;
        public int weeklyStarCount;
    }

    public static class NotificationArray {
        public String ownerId;
        public String message;
        public String action;
        public String actionBundle;
        public String actionBundleName;
        public String date;
    }

    /**
     * 클러치들을 포함하고 있는 클러치북들이다.
     */
    public static interface ClutchBook {
        @GET("/user/{userid}/cb/{cbid}")
        void getBook(@Path("userid") String userId,
                     @Path("cbid") String cbId,
                     @Query("sort") int sort,
                     @Query("pageNo") int pageNo,
                     @Query("pageCount") int pageCount,
                     @Header("token") String token,
                     Callback<Clutch.ClutchResult> cb);

        @GET("/cb/search")
        void search(@Query("keyword") String keyword,
                    @Query("sort") int sort,
                    @Query("pageNo") int pageNo,
                    @Query("pageCount") int pageCount,
                    Callback<User.ClutchBookResult> cb);

        @POST("/user/{userid}/cb")
        @FormUrlEncoded
        void makeCB(@Path("userid") String userid,
                    @Header("token") String token,
                    @Field("name") String name,
                    @Field("description") String description,
                    Callback<Result> cb);
    }

    /**
     * 클러치 데이터를 담아놓을 클러치북 API 인터페이스이다.
     * 처음에 클래스 이름이 Clutch 그대로 갖다썻는데 문서보니까 Clutch,ClutchBook 둘다있더라 고생좀했다
     */
    public static interface Clutch {

        /**
         * 미안하다 파라미터 설명은 깃허브문서를 보자.
         * 그리고 모든 통신의 헤더에 User Token 넣을것.
         * ClutchUtil 새로운 클러치 쓰기
         */
        @POST("/clutch")
        @FormUrlEncoded
        void post(@Field("tags") String tags,
                  @Field("cb_id") String cb_id,
                  @Field("content_type") int content_type,
                  @Field("content_pictures") String content_pictures,
                  @Field("content_link") String content_link,
                  @Field("content_link_image_url") String content_link_image_url,
                  @Field("content_body") String content_body,
                  @Header("token") String token,
                  Callback<ClutchResult> cb);

        /**
         * Clutch 를 가져온다. 타임라인용
         */
        @GET("/clutch")
        void get(@Query("sort") int sort,
                 @Query("pageNo") int pageNo,
                 @Query("pageCount") int pageCount,
                 @Header("token") String token,
                 Callback<ClutchResult> cb);

        /**
         * Clutch를 지운다
         */
        @DELETE("/clutch/{clutchid}")
        @FormUrlEncoded
        void delete(@Path("clutchid") String clutchid,
                    @Header("token") String token,
                    Callback<ClutchResult> cb);

        /**
         * 특정 클러치 얻어오기
         */
        @GET("/clutch/{clutchid}")
        void getClutch(@Path("clutchid") String clutchid,
                       @Header("token") String token,
                       Callback<SingleClutchResult> cb);

        /**
         * Clutch 에 좋아요, 좋아요 해체하기
         */
        @POST("/clutch/{clutchid}/like")
        @FormUrlEncoded
        void like(@Path("clutchid") String clutchid,
                  @Header("token") String token,
                  @Field("") String blank,
                  Callback<ClutchResult> cb);

        @POST("/clutch/{clutchid}/like/delete")
        @FormUrlEncoded
        void dislike(@Path("clutchid") String clutchid,
                     @Header("token") String token,
                     @Field("") String blank,
                     Callback<ClutchResult> cb);

        @POST("/clutch/{clutchid}/share")
        @FormUrlEncoded
        void shareClutch(@Path("clutchid") String clutchid,
                         @Header("token") String token,
                         @Field("cb_id") String cb_id,
                         Callback<Result> cb);

        /**
         * Link , Video Upload
         */

        @GET("/clutch/thumbnails")
        void getLinkImages(@Query("link") String link,
                           Callback<LinkResult> cb);

        @GET("/video/thumb")
        void getVideo(@Query("url") String link,
                      Callback<VideoResult> cb);

        public static class SingleClutchResult extends Result{
            public ClutchArray clutch;
        }

        public static class ClutchResult extends Result {
            public ArrayList<ClutchArray> clutches;
            public int isFollowing;
        }

        public static class LinkResult extends Result {
            public List<String> image_urls;
        }

        public static class VideoResult extends Result {
            public String thumb_id;
        }

        /**
         * 댓글을 가져온다
         */
        @GET("/clutch/{clutchid}/comment")
        void comments(@Path("clutchid") String clutchid,
                      @Query("pageNo") int pageNo,
                      @Query("pageCount") int pageCount,
                      @Header("token") String token,
                      Callback<CommentResult> cb);

        /**
         * 댓글등록
         */
        @POST("/clutch/{clutchid}/comment")
        @FormUrlEncoded
        void postcomment(@Path("clutchid") String clutchid,
                         @Field("body") String body,
                         @Header("token") String token,
                         Callback<Result> cb);

        /**
         * 댓글을 지운다
         */
        @DELETE("/clutch/{clutchid}/comment/{commentid}")
        @FormUrlEncoded
        void deletec(@Path("clutchid") String clutchid,
                     @Path("commentid") String commentid,
                     @Header("token") String token,
                     Callback<Result> cb);

        /**
         * 클러치 검색
         */

        @GET("/clutch/search")
        void search(@Query("keyword") String keyword,
                    @Query("sort") int sort,
                    @Query("pageNo") int pageNo,
                    @Query("pageCount") int pageCount,
                    Callback<ClutchResult> cb);

        @GET("/user/{id}/clutches/search")
        void searchMine(@Path("id") String userId,
                        @Query("keyword") String keyword,
                        Callback<ClutchResult> cb);

        public static class CommentResult extends Result {
            public ArrayList<CommentArray> comments;
        }
    }

    /**
     * 클러치 회원정보 API 인터페이스이다.
     */
    public static interface User {

        /**
         * Clutch 서비스에 회원가입한다.
         * <p/>
         * Url:
         * POST /user/register
         *
         * @param email         이메일
         * @param pw            패스워드 (MD5로 해시되어야됨. 반드시!)
         * @param userName      이름
         * @param statusMessage 상태 메세지
         * @param profileUrl    프로필 사진 (URL)
         * @param gender        성별 (0: 남성, 1: 여성)
         *                      <p/>
         *                      Response:
         *                      userId : 유저ID
         *                      userToken : 토큰
         */
        @POST("/user/register")
        @FormUrlEncoded
        void register(@Field("email") String email,
                      @Field("pw") String pw,
                      @Field("name") String userName,
                      @Field("status_message") String statusMessage,
                      @Field("picture") String profileUrl,
                      @Field("gender") int gender,
                      Callback<LoginResult> cb);

        /**
         * Clutch 서비스에 로그인한다.
         *
         * @param email 이메일
         * @param pw    패스워드 (MD5로 해시되어야됨. 반드시!)
         *              <p/>
         *              Response:
         *              userId : 유저ID
         *              userToken : 토큰
         */
        @POST("/user/login")
        @FormUrlEncoded
        void login(@Field("email") String email,
                   @Field("pw") String pw,
                   Callback<LoginResult> cb);

        /**
         * 로그인 / 회원가입 결과값을 담은 Result 클래스
         */
        public static class LoginResult extends Result {
            public String userId;
            public String userToken;
            public Profile profile;
        }

        /**
         * 프로필을 수정한다.
         *
         * @param userId        유저 ID
         * @param token         유저 토큰
         * @param userName      이름
         * @param statusMessage 상태 메세지
         * @param profileUrl    프로필 사진 (URL)
         * @param gender        성별 (0: 남성, 1: 여성)
         *                      <p/>
         *                      Response:
         */
        @POST("/user/{id}/patch")
        @FormUrlEncoded
        void edit(@Path("id") String userId,
                  @Header("token") String token,
                  @Field("name") String userName,
                  @Field("status_message") String statusMessage,
                  @Field("picture") String profileUrl,
                  @Field("gender") String gender,
                  @Field("pw") String pw,
                  Callback<Result> cb);

        /**
         * 프로필을 서버에서 가져온다.
         *
         * @param userId 유저 ID
         */
        @GET("/user/{id}")
        void get(@Path("id") String userId,
                 @Header("token") String userToken,
                 Callback<ProfileResult> cb);

        /**
         * 프로필 가져오기 결과값을 담은 Result 클래스
         */
        public static class ProfileResult extends Result {
            public Profile profile;
        }

        /**
         * 유저의 별 갯수를 서버에서 가져온다.
         *
         * @param userId 유저 ID
         * @param token  유저 토큰
         */
        @GET("/user/{id}/star")
        void getStar(@Path("id") String userId, @Header("token") String token, Callback<StarResult> cb);

        public static class StarResult extends Result {
            public Integer stars;
        }

        /**
         * 해당 유저를 삭제한다.
         *
         * @param userId 유저 ID
         * @param token  유저 토큰
         */
        @DELETE("/user/{id}")
        void delete(@Path("id") String userId, @Header("token") String token, Callback<Result> cb);

        /**
         * 다른 사람을 팔로우한다.
         */
        @POST("/user/{id}/followings")
        @FormUrlEncoded
        void follow(@Path("id") String userId, @Header("token") String token,
                    @Field("following_id") String toFollowUserId, Callback<Result> cb);

        /**
         * 다른 사람을 언팔한다.
         */
        @POST("/user/{id}/followings/delete")
        @FormUrlEncoded
        void unfollow(@Path("id") String userId, @Header("token") String token,
                      @Field("following_id") String toUnfollowUserId, Callback<Result> cb);

        /**
         * 팔로어 목록을 구한다.
         */
        @GET("/user/{id}/followers")
        void getFollowers(@Path("id") String userId,
                          @Query("pageCount") Integer pageCount,
                          @Query("pageNo") Integer pageNo,
                          @Header("token") String userToken,
                          Callback<FollowerResult> cb);

        /**
         * 팔로어 가져오기 결과값을 담은 Result 클래스
         */
        public static class FollowerResult extends Result {
            public List<Profile> followers;
        }

        /**
         * 팔로잉 목록을 구한다.
         */
        @GET("/user/{id}/followings")
        void getFollowings(@Path("id") String userId,
                           @Query("pageCount") Integer pageCount,
                           @Query("pageNo") Integer pageNo,
                           @Header("token") String userToken,
                           Callback<FollowingResult> cb);

        /**
         * 유저의 클러치들 보기
         */
        @GET("/user/{id}/clutches")
        void getUserClutch(
                @Path("id") String userId,
                @Query("sort") int sort,
                @Query("pageNo") int pageNo,
                @Query("pageCount") int pageCount,
                @Query("token") String userToken,
                Callback<Clutch.ClutchResult> cb);

        /**
         * 유저의 클러치북들 보기
         */
        @GET("/user/{id}/cb")
        void getUserCB(
                @Path("id") String userId,
                @Header("token") String userToken,
                Callback<ClutchBookResult> cb);

        public static class ClutchBookResult extends Result {
            public List<ClutchBookArray> cbs;
        }


        /**
         * 유저가 좋아한 클러치들 보기
         */
        @GET("/user/{id}/clutches/likes")
        void getUserLikes(
                @Path("id") String userId,
                @Query("sort") int sort,
                @Query("pageNo") int pageNo,
                @Query("pageCount") int pageCount,
                @Header("token") String userToken,
                Callback<Clutch.ClutchResult> cb);

        /**
         * 팔로잉 가져오기 결과값을 담은 Result 클래스
         */
        public static class FollowingResult extends Result {
            public List<Profile> followings;
        }

        @GET("/user/{id}/favorites")
        void getUserArtists(
                @Path("id") String userId,
                @Header("token") String userToken,
                Callback<ArtistResult> cb);

        @POST("/user/{id}/favorites")
        @FormUrlEncoded
        void addUserArtists(
                @Path("id") String userId,
                @Field("artistId") String artistId,
                @Header("token") String userToken,
                Callback<Result> cb);

        @POST("/user/{id}/favorites/delete")
        @FormUrlEncoded
        void deleteUserArtists(
                @Path("id") String userId,
                @Field("artistId") String artistId,
                @Header("token") String userToken,
                Callback<Result> cb);

        public static class ArtistResult extends Result {
            public List<ArtistArray> artists;
        }

        @POST("/user/{id}/gcm/register")
        @FormUrlEncoded
        void reg(
                @Path("id") String userId,
                @Header("token") String userToken,
                @Field("regId") String regId,
                Callback<Result> cb);

        @POST("/user/{id}/gcm/unregister")
        @FormUrlEncoded
        void unreg(
                @Path("id") String userId,
                @Header("token") String userToken,
                @Field("regId") String regId,
                Callback<Result> cb);

        @GET("/user/{id}/cb/{cbid}/is_following")
        void isFollowing(
                @Path("id") String userId,
                @Path("cbid") String cbId,
                @Header("token") String userToken,
                Callback<isFollowingResult> cb);

        @GET("/user/{userid}/cb/{cbid}/follow")
        void getCBFollow(
                @Path("userid") String userId,
                @Path("cbid") String cbId,
                @Header("token") String userToken,
                Callback<Result> cb);

        @GET("/user/{userid}/cb/{cbid}/unfollow")
        void getCBUnFollow(
                @Path("userid") String userId,
                @Path("cbid") String cbId,
                @Header("token") String userToken,
                Callback<Result> cb);

        public static class isFollowingResult extends Result {
            public int isFollowing;
        }

    }

    public static interface Ranking {
        @GET("/ranking/{week}")
        void getRanking(
                @Path("week") int prevWeek,
                @Query("pageNo") int pageNo,
                @Query("pageCount") int pageCount,
                Callback<RankingResult> cb);

        public static class RankingResult extends Result {
            public String year;
            public String month;
            public String week;
            public ArrayList<RankingArray> ranks;
        }
    }

    public static interface Image {
        @Multipart
        @PUT("/images/put")
        void put(@Part("image") TypedFile filePath,
                 @Part("token") String userToken,
                 Callback<ImageResult> cb);

        public static class ImageResult extends Result {
            public String imageId;
        }
    }

    public static interface Notification {
        @GET("/notifications")
        void getNoti(
                @Header("token") String userToken,
                Callback<NotificationResult> cb);

        public static class NotificationResult extends Result {
            public List<NotificationArray> notifications;
        }
    }


    /**
     * 각종 API 인터페이스를 생성한 User 인스턴스를 돌려준다.
     *
     * @return ClutchUtil.User API 인터페이스
     */

    public static ClutchUtil.ClutchBook getClutchBookInstance() {
        return mRestAdapter.create(ClutchBook.class);
    }

    public static ClutchUtil.User getUserInstance() {
        return mRestAdapter.create(User.class);
    }

    public static ClutchUtil.Clutch getClutchInstance() {
        return mRestAdapter.create(Clutch.class);
    }

    public static ClutchUtil.Ranking getRankingInstance() {
        return mRestAdapter.create(Ranking.class);
    }

    public static ClutchUtil.Image getImageInstance() {
        return mRestAdapter.create(Image.class);
    }

    public static ClutchUtil.Notification getNotiInstance() {
        return mRestAdapter.create(Notification.class);
    }


}