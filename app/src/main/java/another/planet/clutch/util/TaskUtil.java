package another.planet.clutch.util;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
import another.planet.clutch.activity.StartClutchActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by toast on 2014. 2. 21..
 */
public class TaskUtil {
    static Context mContext;
    public static String NetworkMessage = "네트워크에 연결되어있지 않거나, 상태를 확인할 수 없습니다.";
    public static String ErrorMessage = "오류가 발생했습니다. 잠시후 다시 시도해주세요.";
    public static String SuccessMessage = "성공적으로 작업했습니다.";
    public static int temp_sort = 1; // 대부분의 게시글 호출은 최신순 , 인기순이 있는데 임시로 최신순으로 바꿔오게 한다.
    public static int clutchPageCount = 20; // 한번에 표시해줄 클러치 갯수
    public static int littlePagecount = 50; // 한번에 표시해줄 댓글 / 프로필 리스트 갯수
    public static int rankListCount = 20;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String SENDER_ID = "370338327974";

    static String endResult = "";
    static int isFollowing;

    public static ProgressDialog getProgressDialog(Context c) {
        mContext = c;
        ProgressDialog progressDialog;

        progressDialog = new ProgressDialog(mContext);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("잠시만 기다려주세요");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        return progressDialog;
    }

    /**
     * TimeLine 용 sort 를 저장하고 불러온다.
     */

    public static void saveSort(Context c, int sort) {
        mContext = c;
        SharedPreferences sp = c.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("sort", sort);
        editor.commit();
    }

    public static int getSort(Context c) {
        mContext = c;
        SharedPreferences sp = c.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
        int sort = sp.getInt("sort", 0);
        return sort;
    }

    public static String getUserToken(Context c) {
        mContext = c;
        SharedPreferences sp = c.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
        String userToken = sp.getString("userToken", "");
        return userToken;
    }

    public static String getUserId(Context c) {
        mContext = c;
        SharedPreferences sp = c.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
        String userId = sp.getString("userId", "");
        return userId;
    }

    public static boolean isNetworkAvailable(Context c) {
        mContext = c;
        boolean isAble;

        ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        try {
            if (!mobile.isConnected() && !wifi.isConnected()) {
                isAble = false;
            } else {
                isAble = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            isAble = false;
        }
        return isAble;
    }

    public static String md5(String unConverted) {
        String converted = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(unConverted.getBytes());
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            converted = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("converted Result(MD5)", converted);
        return converted;
    }

    public static boolean isLogin(Context c) {
        mContext = c;
        SharedPreferences sp = mContext.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
        boolean isLogin = sp.getBoolean("isLogin", false);
        return isLogin;
    }

    public static String getDate(String timeStamp) {
        long timestamp = Long.parseLong(timeStamp);
        String dateString;
        try {
            DateFormat dm = new SimpleDateFormat("yyyy. MM. dd. a hh:mm");
            Date netDate = (new Date(timestamp));
            dateString = dm.format(netDate);
        } catch (Exception e) {
            dateString = "";
        }
        return dateString;
    }

    public static void saveUserStar(Context c) {
        mContext = c;
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        user.getStar(getUserId(mContext), getUserToken(mContext), new Callback<ClutchUtil.User.StarResult>() {
            @Override
            public void success(ClutchUtil.User.StarResult starResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(response.getStatus());
                if (errorMsg.equals("")) {
                    int star = starResult.stars;
                    SharedPreferences sp = mContext.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putInt("star", star);
                    editor.commit();
                } else {
                    Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "TaskUtil/getStar");
                }
            }
        });
    }

    public static int getUserDataInt(Context c, String keyword) {
        SharedPreferences sp = c.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
        int result = sp.getInt(keyword, 0);
        return result;
    }

    public static int getUserStar(Context c) {
        SharedPreferences sp = c.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
        int star = sp.getInt("star", 0);
        return star;
    }

    public static String getUserDataString(Context c, String keyword) {
        SharedPreferences sp = c.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
        String result = sp.getString(keyword, "");
        return result;
    }

    public static boolean getDismissPost(Context c) {
        SharedPreferences sp = c.getSharedPreferences("clutch_data", Context.MODE_PRIVATE);
        boolean isExist = sp.getBoolean("dismiss_post", false);
        return isExist;
    }

    public static String patchUserData(Context c, String name, String status_message, String pictureID, String gender, String pw) {
        mContext = c;
        String endResult = "";
        try {
            HttpClient client = new DefaultHttpClient();
            client.getParams().setParameter("http.protocol.expect-continue", false);
            client.getParams().setParameter("http.connection.timeout", 5000);
            client.getParams().setParameter("http.socket.timeout", 5000);

            String url = ClutchUtil.ENDPOINT + "/user/" + TaskUtil.getUserId(mContext) + "/patch";
            HttpPost post = new HttpPost(url);
            post.setHeader("token", TaskUtil.getUserToken(mContext));
            MultipartEntity entity = new MultipartEntity();
            Charset chars = Charset.forName("UTF-8");

            if (name != null) entity.addPart("name", new StringBody(name, chars));
            if (status_message != null) entity.addPart("status_message", new StringBody(status_message, chars));
            if (pictureID != null) entity.addPart("picture", new StringBody(pictureID));
            if (gender != null) entity.addPart("gender", new StringBody(gender));
            if (pw != null) entity.addPart("pw", new StringBody(pw));

            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            HttpEntity resEntity = response.getEntity();

            endResult = EntityUtils.toString(resEntity);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return endResult;
    }

    public static String uploadImage(Context c, Uri filePath) {
        mContext = c;
        String endResult = "";
        try {
            HttpClient client = new DefaultHttpClient();
            client.getParams().setParameter("http.protocol.expect-continue", false);
            client.getParams().setParameter("http.connection.timeout", 10000);
            client.getParams().setParameter("http.socket.timeout", 10000);

            String url = ClutchUtil.ENDPOINT + "/images/put";
            HttpPost post = new HttpPost(url);
            post.setHeader("token", TaskUtil.getUserToken(mContext));
            MultipartEntity entity = new MultipartEntity();
            Charset chars = Charset.forName("UTF-8");

            entity.addPart("image", new FileBody(new File(filePath.getPath())));

            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            HttpEntity resEntity = response.getEntity();

            endResult = EntityUtils.toString(resEntity);
            Log.e("Picture Meta ID", endResult);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return endResult;
    }

    public static void saveUserDataString(Context c, String keyword, String query) {
        mContext = c;
        SharedPreferences sp = mContext.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(keyword, query);
        editor.commit();
    }

    public static boolean checkPlayServices(final Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context, PLAY_SERVICES_RESOLUTION_REQUEST);
                if (errorDialog.isShowing()) errorDialog.dismiss();
                errorDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        ((Activity) context).finish();
                    }
                });
                errorDialog.show();
            } else {
                Log.i("Logging", "This device is not supported.");
                ((Activity) context).finish();
            }
            return false;
        }
        return true;
    }

    public static String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i("Logging", "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i("Logging", "App version changed.");
            return "";
        }
        return registrationId;
    }

    public static SharedPreferences getGCMPreferences(Context context) {
        return context.getSharedPreferences(StartClutchActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static void sendRegistrationIdToBackend() {
        // Your implementation here.
    }

    public static void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i("Logging", "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    //boolean -> Yes 가입 , No -> 해
    public static String setRegisterGCM(String userId, String userToken, String regId, boolean isRegegister) {

        ClutchUtil.User user = ClutchUtil.getUserInstance();
        if (isRegegister) {
            user.reg(userId, userToken, regId, new Callback<ClutchUtil.Result>() {
                @Override
                public void success(ClutchUtil.Result result, Response response) {
                    String errorMsg = ClutchUtil.Status.check(result.status.code);
                    if (errorMsg.equals("")) {
                        endResult = "";
                        Log.e("endResult", "Success");
                    } else {
                        endResult = errorMsg;
                        Log.e("endResult", result.getStatusMessage());
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {

                }
            });
        } else {
            user.unreg(userId, userToken, regId, new Callback<ClutchUtil.Result>() {
                @Override
                public void success(ClutchUtil.Result result, Response response) {
                    String errorMsg = ClutchUtil.Status.check(result.status.code);
                    if (errorMsg.equals("")) {
                        endResult = "";
                    } else {
                        endResult = errorMsg;
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {

                }
            });
        }
        return endResult;
    }

    public static int getCBFollowing(String userId, String cbId, Context context) {
        mContext = context;
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        user.isFollowing(userId, cbId, getUserToken(context), new Callback<ClutchUtil.User.isFollowingResult>() {
            @Override
            public void success(ClutchUtil.User.isFollowingResult isFollowingResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(isFollowingResult.status.code);
                if (errorMsg.equals("")) {
                    isFollowing = isFollowingResult.isFollowing;
                } else {
                    isFollowing = 2;
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                isFollowing = 2;
            }
        });
        return isFollowing;
    }
}
