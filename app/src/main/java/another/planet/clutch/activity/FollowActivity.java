package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

/**
 * Created by Marutian on 2014. 2. 11..
 */
public class FollowActivity extends Activity implements View.OnClickListener {

    Context context;
    String userId;
    boolean isFollowing;
    FollowListAdapter fAdapter;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_follow);
        //setContentView(R.layout.closed_page_unfinished);
        context = this;
        isFollowing = getIntent().getBooleanExtra("isFollowing", false); // true면 팔로잉 리스트 , false면 팔로워리스트
        userId = getIntent().getStringExtra("userId");

        ImageLoaderUtil.setDefImageConfig(context);

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        LinearLayout btnBallon = (LinearLayout) findViewById(R.id.actionbar_ballon_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        ((TextView) findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(context)));

        titleTextView.setText((isFollowing) ? "팔로잉" : "팔로워");

        btnBack.setOnClickListener(this);
        btnBallon.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        getProfileList(0, TaskUtil.littlePagecount);
        //실제로 백엔드 주소 작업할때는 팔로잉/팔로우 가지고 올 주소만 넣어주면됨.
    }

    private void getProfileList(int pageNo, int pageCount) {
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        if (isFollowing) {
            user.getFollowings(userId, pageCount, pageNo, TaskUtil.getUserToken(context), new Callback<ClutchUtil.User.FollowingResult>() {
                @Override
                public void success(ClutchUtil.User.FollowingResult followingResult, Response response) {
                    String errorMsg = ClutchUtil.Status.check(followingResult.status.code);
                    if (errorMsg.equals("")) {
                        ArrayList<Construct.Profile> array = new ArrayList<Construct.Profile>();
                        for (ClutchUtil.Profile profile : followingResult.followings) {
                            array.add(new Construct.Profile(
                                    profile.id,
                                    profile.name,
                                    profile.status_message,
                                    profile.picture,
                                    profile.followers_count,
                                    profile.followings_count,
                                    profile.liked_count,
                                    profile.clutch_count,
                                    profile.cb_count,
                                    profile.last_photos,
                                    profile.isFollowing
                            ));
                            fAdapter = new FollowListAdapter(context, 0, array);
                            ((ListView) findViewById(R.id.follower_list)).setAdapter(fAdapter);
                        }
                    } else {
                        Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {

                }
            });
        } else {
            user.getFollowers(userId, pageCount, pageNo, TaskUtil.getUserToken(context), new Callback<ClutchUtil.User.FollowerResult>() {
                @Override
                public void success(ClutchUtil.User.FollowerResult followerResult, Response response) {
                    String errorMsg = ClutchUtil.Status.check(followerResult.status.code);
                    if (errorMsg.equals("")) {
                        ArrayList<Construct.Profile> array = new ArrayList<Construct.Profile>();
                        for (ClutchUtil.Profile profile : followerResult.followers) {
                            array.add(new Construct.Profile(
                                    profile.id,
                                    profile.name,
                                    profile.status_message,
                                    profile.picture,
                                    profile.followers_count,
                                    profile.followings_count,
                                    profile.liked_count,
                                    profile.clutch_count,
                                    profile.cb_count,
                                    profile.last_photos,
                                    profile.isFollowing
                            ));
                            fAdapter = new FollowListAdapter(context, 0, array);
                            ((ListView) findViewById(R.id.follower_list)).setAdapter(fAdapter);
                        }
                    } else {
                        Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {

                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_ballon_layout:
                startActivity(new Intent(context, BallonStatusActivity.class));
                break;
            case R.id.actionbar_search:
                startActivity(new Intent(context, SearchActivity.class));
                break;
        }
    }

    public class FollowListAdapter extends ArrayAdapter<Object> {
        ArrayList<Construct.Profile> array;

        public FollowListAdapter(Context context, int resource, ArrayList<Construct.Profile> array) {
            super(context, resource);
            this.array = array;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_follower, null);

            Construct.Profile followData = array.get(position);

            ((TextView) convertView.findViewById(R.id.profile_name)).setText(followData.name);
            if (followData.isFollowing == 0) {
                ((TextView) convertView.findViewById(R.id.txtFollow)).setText("Follow");
            } else if (followData.isFollowing == 1){
                ((TextView) convertView.findViewById(R.id.txtFollow)).setText("UnFollow");
            } else {
                ((LinearLayout) convertView.findViewById(R.id.follow)).setVisibility(View.INVISIBLE);
            }
            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(followData.picture, 200), ((ImageView) convertView.findViewById(R.id.profile_image)), ImageLoaderUtil.imageLoaderOptions());

            final View finalConvertView = convertView;
            ((LinearLayout) convertView.findViewById(R.id.list_item)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, OtherProfileActivity.class);
                    i.putExtra("userId", array.get(position).id);
                    i.putExtra("userName", array.get(position).name);
                    startActivity(i);
                }
            });

            ((LinearLayout) convertView.findViewById(R.id.follow)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if(array.get(position).isFollowing == 0){
                        ClutchUtil.User user = ClutchUtil.getUserInstance();
                        user.follow(TaskUtil.getUserId(context), TaskUtil.getUserToken(context), array.get(position).id, new Callback<ClutchUtil.Result>() {
                            @Override
                            public void success(ClutchUtil.Result result, Response response) {
                                String errorMsg = ClutchUtil.Status.check(result.status.code);
                                if(errorMsg.equals("")){
                                    array.get(position).isFollowing = 1;
                                    ((TextView) finalConvertView.findViewById(R.id.txtFollow)).setText("UnFollow");
                                } else{
                                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError retrofitError) {

                            }
                        });
                    }

                    else if(array.get(position).isFollowing == 1){
                        ClutchUtil.User user = ClutchUtil.getUserInstance();
                        user.unfollow(TaskUtil.getUserId(context), TaskUtil.getUserToken(context), array.get(position).id, new Callback<ClutchUtil.Result>() {
                            @Override
                            public void success(ClutchUtil.Result result, Response response) {
                                String errorMsg = ClutchUtil.Status.check(result.status.code);
                                if(errorMsg.equals("")){
                                    array.get(position).isFollowing = 0;
                                    ((TextView) finalConvertView.findViewById(R.id.txtFollow)).setText("Follow");
                                } else{
                                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError retrofitError) {

                            }
                        });
                    }
                }
            });

            return convertView;
        }

        @Override
        public int getCount() {
            return array.size();
        }
    }
}
