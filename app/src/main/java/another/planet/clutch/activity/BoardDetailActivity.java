package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.util.TaskUtil;

import java.util.ArrayList;

public class BoardDetailActivity extends Activity implements View.OnClickListener {

    Context context = BoardDetailActivity.this;
    ArrayList<CommentContents> array;
    EditText edtComment;
    String postId;

    class CommentContents {
        CommentContents(String mentMessage, BitmapDrawable profileImage, String name, String date) {
            this.mentMessage = mentMessage;
            this.profileImage = profileImage;
            this.name = name;
            this.date = date;
        }

        String mentMessage, name, date;
        BitmapDrawable profileImage;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_freeb_detail);

        String postId = getIntent().getStringExtra("post_id");
        String postType = getIntent().getStringExtra("post_type");
        String postName = getIntent().getStringExtra("post_name");
        String postUploader = getIntent().getStringExtra("post_uploader");
        String postDate = getIntent().getStringExtra("post_date");
        String postMent = getIntent().getStringExtra("post_ment");
        String commentCount = getIntent().getStringExtra("comment_count");
        String likeCount = getIntent().getStringExtra("like_count");

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        LinearLayout btnBallon = (LinearLayout) findViewById(R.id.actionbar_ballon_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        ((TextView) findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(context)));
        titleTextView.setText("게시물 상세보기");


        btnBack.setOnClickListener(this);
        btnBallon.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        //ID로 컨텐츠 정보 가져와야함(컨텐츠에 표시될 사진과 작성자 프로필사진)
        BitmapDrawable postImage = (BitmapDrawable) getResources().getDrawable(R.drawable.temp_image);
        BitmapDrawable uploaderImage = (BitmapDrawable) getResources().getDrawable(R.drawable.temp_image);

        //ID로 댓글 아이템들 가져와야함
        String comment[] = {
                "민아와 함께 새해 첫 인사를 드리네요. 2014년 건강하시고 행복한 한해가 되길 바랄게요. 쇼케이스 때 봐요",
                "아이유와 함께 새해 첫 인사를 드리네요. 2014년 건강하시고 행복한 한해가 되길 바랄게요. 쇼케이스 때 봐요",
                "제시카와 함께 새해 첫 인사를 드리네요. 2014년 건강하시고 행복한 한해가 되길 바랄게요. 쇼케이스 때 봐요"};

        BitmapDrawable profileImage[] = {(BitmapDrawable) getResources().getDrawable(R.drawable.temp_image), (BitmapDrawable) getResources().getDrawable(R.drawable.temp_image2), (BitmapDrawable) getResources().getDrawable(R.drawable.temp_image3)};
        String name[] = {"Sean Jung", "augite", "ABCDE"};
        String date[] = {"2014.01.12", "2014.01.24", "2014.01.25"};

        array = new ArrayList<CommentContents>();

        for (int i = 0; i < comment.length; i++) {
            array.add(new CommentContents(comment[i], profileImage[i], name[i], date[i]));
        }

        ListView lv = (ListView) findViewById(R.id.freeb_detail_list);
        View header = getLayoutInflater().inflate(R.layout.item_freeboard_header, null);

        ((TextView) header.findViewById(R.id.free_post_type)).setText(postType);
        ((TextView) header.findViewById(R.id.free_post_name)).setText(postName);
        ((TextView) header.findViewById(R.id.free_uploader)).setText(postUploader);
        ((TextView) header.findViewById(R.id.free_post_date)).setText(postDate);
        ((TextView) header.findViewById(R.id.free_post_ment)).setText(postMent);
        ((TextView) header.findViewById(R.id.free_post_like_count)).setText(likeCount);
        ((TextView) header.findViewById(R.id.free_post_comment_count)).setText(commentCount);
        ((ImageView) header.findViewById(R.id.post_image)).setImageDrawable((Drawable) postImage);
        ((ImageView) header.findViewById(R.id.profile_uploader_image)).setImageDrawable((Drawable) uploaderImage);

        lv.addHeaderView(header, -1, true);
        lv.setAdapter(new DetailListAdapter(context, R.layout.item_freeb_comment_row));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_ballon_layout:
                startActivity(new Intent(context, BallonStatusActivity.class));
                break;
            case R.id.actionbar_search:
                startActivity(new Intent(context, SearchActivity.class));
                break;
        }
    }

    public class DetailListAdapter extends ArrayAdapter<String> {

        public DetailListAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_freeb_comment_row, null);

            CommentContents contents = array.get(position);
            ((TextView) v.findViewById(R.id.ment)).setText(contents.mentMessage);
            ((TextView) v.findViewById(R.id.name)).setText(contents.name);
            ((TextView) v.findViewById(R.id.date)).setText(contents.date);
            ((ImageView) v.findViewById(R.id.profile_image)).setImageDrawable((Drawable) contents.profileImage);

            return v;
        }

        @Override
        public int getCount() {
            return array.size();
        }
    }
}