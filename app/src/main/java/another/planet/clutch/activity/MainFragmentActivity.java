package another.planet.clutch.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import another.planet.clutch.R;
import another.planet.clutch.arcmenu.ArcLayout;
import another.planet.clutch.arcmenu.ArcMenu;
import another.planet.clutch.dialog.LinkUrlDialog;
import another.planet.clutch.fragment.main.BoardFragment;
import another.planet.clutch.fragment.main.FavoriteFragment;
import another.planet.clutch.fragment.main.MainFragment;
import another.planet.clutch.fragment.main.ProfileFragment;
import another.planet.clutch.fragment.main.RankFragment;
import another.planet.clutch.upload.PhotoUploadActivity;
import another.planet.clutch.upload.VideoUploadActivity;
import another.planet.clutch.util.TaskUtil;

public class MainFragmentActivity extends FragmentActivity implements OnClickListener {

    static Context context;
    ViewPager mPager;
    public static View darkmask;
    LinearLayout tabMain, tabFavorite, tabRank, tabBoard, tabProfile;
    public static ArcMenu arcMenu;
    private static final int[] ITEM_DRAWABLES = {R.drawable.upload_photo, R.drawable.upload_video, R.drawable.upload_link};

    // The fragment which needs to show on the viewpager.
    private Fragment[] FRAGMENTS;

    public interface OnBackPressedListener {
        public void onBackPressed();
    }

    public OnBackPressedListener listener;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        setContentView(R.layout.activity_main);

        ((ImageView) findViewById(R.id.actionbar_back)).setVisibility(View.GONE);  //Not needed back button on main page.
        ((ImageView) findViewById(R.id.actionbar_logo)).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.actionbar_text)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(context)));

        MainAdapter mainAdapter = new MainAdapter(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.main_pager);
        mPager.setAdapter(mainAdapter);
        mPager.setOffscreenPageLimit(4);
        mPager.setPageMargin(16);

        tabMain = (LinearLayout) findViewById(R.id.tab_home);
        tabFavorite = (LinearLayout) findViewById(R.id.tab_favorite);
        tabRank = (LinearLayout) findViewById(R.id.tab_rank);
        tabBoard = (LinearLayout) findViewById(R.id.tab_board);
        tabProfile = (LinearLayout) findViewById(R.id.tab_profile);

        darkmask = (View) findViewById(R.id.darkmask);

        LinearLayout btnBallon = (LinearLayout) findViewById(R.id.actionbar_ballon_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);

        tabMain.setOnClickListener(this);
        tabFavorite.setOnClickListener(this);
        tabRank.setOnClickListener(this);
        tabBoard.setOnClickListener(this);
        tabProfile.setOnClickListener(this);

        btnBallon.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        tabMain.setSelected(true);

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                selectTab(arg0);
            }     //페이지가 바뀔때 마다 탭의 상태를 바꿈

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        arcMenu = (ArcMenu) findViewById(R.id.arc_menu);
        //아크메뉴의 해상도 대응 (저해상도 폰에서 너무 위로 올라오는 문제때문에..)
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) arcMenu.getLayoutParams();
        int density = metrics.densityDpi;
        Log.e("density", Float.toString(metrics.ydpi));
        switch (density) {
            case 160:
                arcMenu.setY(metrics.ydpi - 96);
                ArcLayout.mChildPadding = 64;
                break;
            case 240:
                arcMenu.setY(metrics.ydpi - 140);
                ArcLayout.mChildPadding = 72;
                break;
            case 320:
                arcMenu.setY(metrics.ydpi - 180);
                ArcLayout.mChildPadding = 96;
                break;
            case 480:
                arcMenu.setY(metrics.ydpi - 320);
                ArcLayout.mChildPadding = 128;
                break;
            default:
                Toast.makeText(context, "지원하는 해상도가 아닙니다. 앱 사용이 불편할 수 있습니다.", Toast.LENGTH_SHORT).show();
                break;
        }

        final int itemCount = ITEM_DRAWABLES.length;
        for (int i = 0; i < itemCount; i++) {
            ImageView item = new ImageView(context);
            item.setImageResource(ITEM_DRAWABLES[i]);

            final int position = i;
            arcMenu.addItem(item, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (position) {
                        case 0:
                            startActivity(new Intent(context, PhotoUploadActivity.class));
                            break;
                        case 1:
                            startActivity(new Intent(context, VideoUploadActivity.class));
                            break;
                        case 2:
                            LinkUrlDialog linkDialog = new LinkUrlDialog(context);
                            linkDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                public void onDismiss(DialogInterface dialog) {
                                }
                            });
                            linkDialog.show();
                            break;
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab_home:
                mPager.setCurrentItem(0);
                break;
            case R.id.tab_favorite:
                mPager.setCurrentItem(1);
                break;
            case R.id.tab_rank:
                mPager.setCurrentItem(2);
                break;
            case R.id.tab_board:
                mPager.setCurrentItem(3);
                break;
            case R.id.tab_profile:
                mPager.setCurrentItem(4);
                break;
            case R.id.actionbar_ballon_layout:
                startActivity(new Intent(context, BallonStatusActivity.class));
                break;
            case R.id.actionbar_search:
                startActivity(new Intent(context, SearchActivity.class));
                break;
        }
    }

    // 현재 보이는 탭을 포커스처리함
    public void selectTab(int position) {
        switch (position) {
            case 0:
                initTabImage();
                tabMain.setSelected(true);
                arcMenu.setVisibility(View.VISIBLE);
                break;
            case 1:
                initTabImage();
                tabFavorite.setSelected(true);
                arcMenu.setVisibility(View.VISIBLE);
                break;
            case 2:
                initTabImage();
                tabRank.setSelected(true);
                arcMenu.setVisibility(View.GONE);
                break;
            case 3:
                initTabImage();
                tabBoard.setSelected(true);
                arcMenu.setVisibility(View.GONE);
                break;
            case 4:
                initTabImage();
                tabProfile.setSelected(true);
                arcMenu.setVisibility(View.GONE);
                break;
        }
    }

    public void initTabImage() {
        //탭의 이미지를 모두 초기화한다.
        tabMain.setSelected(false);
        tabFavorite.setSelected(false);
        tabRank.setSelected(false);
        tabBoard.setSelected(false);
        tabProfile.setSelected(false);
    }

    private class MainAdapter extends FragmentStatePagerAdapter {

        public MainAdapter(FragmentManager fm) {
            super(fm);
            FRAGMENTS = new Fragment[5];
            FRAGMENTS[0] = new MainFragment(context);
            FRAGMENTS[1] = new FavoriteFragment(context);
            FRAGMENTS[2] = new RankFragment(context);
            FRAGMENTS[3] = new BoardFragment(context);
            FRAGMENTS[4] = new ProfileFragment(context);
        }

        @Override
        public Fragment getItem(int position) {
            return FRAGMENTS[position];
        }

        @Override
        public int getCount() {
            return FRAGMENTS.length;
        }

    }

    public void onBackPressed() {
        if (FRAGMENTS[1] != null &&
                FRAGMENTS[1] instanceof FavoriteFragment &&
                ((FavoriteFragment) FRAGMENTS[1]).spinnerDropdownView.isShown()) {
            ((FavoriteFragment) FRAGMENTS[1]).spinnerSwitchHandler.sendEmptyMessage(0);
        } else {
            finish();
        }
    }
}