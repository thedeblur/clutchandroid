package another.planet.clutch.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.fragment.profile.ClutchBookFragment;
import another.planet.clutch.fragment.profile.other.ClutchFragment;
import another.planet.clutch.fragment.profile.other.LikeFragment;
import another.planet.clutch.fragment.profile.other.MoreFragment;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Marutian on 2014. 2. 21..
 */
public class OtherProfileActivity extends FragmentActivity implements View.OnClickListener {

    Context context;
    ViewPager mPager;
    LinearLayout tabLike, tabClutch, tabClutchBook, tabMore;
    String profileName, userId, followingLength, followerLength;
    ImageView profileImage;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_profile);

        context = this;
        ImageLoaderUtil.setDefImageConfig(context);
        profileName = getIntent().getStringExtra("userName");
        userId = getIntent().getStringExtra("userId");

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        LinearLayout btnBallon = (LinearLayout) findViewById(R.id.actionbar_ballon_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        ((TextView) findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(context)));
        profileImage = (ImageView) findViewById(R.id.profile_image);
        tabLike = (LinearLayout) findViewById(R.id.tab_profile_like);
        tabClutch = (LinearLayout) findViewById(R.id.tab_profile_clutch);
        tabClutchBook = (LinearLayout) findViewById(R.id.tab_profile_clutchbook);
        tabMore = (LinearLayout) findViewById(R.id.tab_profile_more);

        btnBack.setOnClickListener(this);
        btnBallon.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        tabLike.setOnClickListener(this);
        tabClutch.setOnClickListener(this);
        tabClutchBook.setOnClickListener(this);
        tabMore.setOnClickListener(this);

        titleTextView.setText(profileName + "님의 프로필");
        tabLike.setSelected(true);

        if (TaskUtil.isNetworkAvailable(context)) {
            ClutchUtil.User user = ClutchUtil.getUserInstance();
            user.get(userId, TaskUtil.getUserToken(context), new Callback<ClutchUtil.User.ProfileResult>() {
                @Override
                public void success(final ClutchUtil.User.ProfileResult profileResult, Response response) {
                    String errorMsg = ClutchUtil.Status.check(profileResult.status.code);
                    if (errorMsg.equals("")) {
                        ((TextView) findViewById(R.id.profile_name)).setText(profileResult.profile.name);
                        ((TextView) findViewById(R.id.profile_statusmsg)).setText(profileResult.profile.status_message);

                        Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                super.handleMessage(msg);
                                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(profileResult.profile.picture, 200), profileImage, ImageLoaderUtil.imageLoaderOptions());
                            }
                        };
                        handler.sendEmptyMessageDelayed(1, 0);

                        followerLength = Integer.toString(profileResult.profile.followers_count);
                        followingLength = Integer.toString(profileResult.profile.followings_count);
                        setAdapter();
                    } else {
                        Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    String errorMsg = ClutchUtil.Status.check(5000);
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    try {
                        Log.e("Error Reason", retrofitError.getCause().toString());
                    } catch (Exception e) {
                        Log.e("UnKnownError", "OtherProfileActivity / getUserData");
                    }
                }
            });
        } else {
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
        }
    }

    private void setAdapter() {
        ProfileMenuAdapter adapter = new ProfileMenuAdapter(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.menu_pager);
        mPager.setAdapter(adapter);
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                selectTab(arg0);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_ballon_layout:
                startActivity(new Intent(context, BallonStatusActivity.class));
                break;
            case R.id.actionbar_search:
                startActivity(new Intent(context, SearchActivity.class));
                break;
            case R.id.tab_profile_like:
                mPager.setCurrentItem(0);
                break;
            case R.id.tab_profile_clutch:
                mPager.setCurrentItem(1);
                break;
            case R.id.tab_profile_clutchbook:
                mPager.setCurrentItem(2);
                break;
            case R.id.tab_profile_more:
                mPager.setCurrentItem(3);
                break;
        }
    }

    // 현재 보이는 탭을 포커스처리함
    public void selectTab(int position) {
        switch (position) {
            case 0:
                initTabImage();
                tabLike.setSelected(true);
                break;
            case 1:
                initTabImage();
                tabClutch.setSelected(true);
                break;
            case 2:
                initTabImage();
                tabClutchBook.setSelected(true);
                break;
            case 3:
                initTabImage();
                tabMore.setSelected(true);
                break;
        }
    }

    public void initTabImage() {
        //탭의 배경을 초기화한다.
        tabLike.setSelected(false);
        tabClutch.setSelected(false);
        tabClutchBook.setSelected(false);
        tabMore.setSelected(false);
    }

    private class ProfileMenuAdapter extends FragmentPagerAdapter {

        // The fragment which needs to show on the viewpager.
        private Fragment[] FRAGMENTS = {
                new LikeFragment(context, userId),
                new ClutchFragment(context, userId),
                new ClutchBookFragment(context, userId),
                new MoreFragment(context, userId, followerLength, followingLength)
        };

        public ProfileMenuAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return FRAGMENTS[position];
        }

        @Override
        public int getCount() {
            return FRAGMENTS.length;
        }

    }
}