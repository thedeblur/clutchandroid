package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.TaskUtil;
import com.facebook.Request;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Marutian on 2014. 2. 19..
 */
public class JoinActivity extends Activity implements View.OnClickListener {


    Context context;
    LinearLayout joinFb, joinTwt, joinId, btnExit;
    TextView textProvision;
    EditText edtEmail, edtNick, edtPassword, edtPasswordConfirm;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);
        context = this;

        btnExit = (LinearLayout) findViewById(R.id.btn_exit);
        textProvision = (TextView) findViewById(R.id.text_provision);

        joinFb = (LinearLayout) findViewById(R.id.btn_fb_join);
        joinTwt = (LinearLayout) findViewById(R.id.btn_twt_join);
        joinId = (LinearLayout) findViewById(R.id.btn_join);

        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtNick = (EditText) findViewById(R.id.edt_nick);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtPasswordConfirm = (EditText) findViewById(R.id.edt_password_confirm);

        textProvision.setText(Html.fromHtml("<font color=\"#a3a3a3\">가입과 함께 </font><font color=\"#5e5e5e\"><b>약관</b></font><font color=\"#a3a3a3\">에 동의하시는거임!</font>"));

        btnExit.setOnClickListener(this);
        textProvision.setOnClickListener(this);
        joinFb.setOnClickListener(this);
        joinTwt.setOnClickListener(this);
        joinId.setOnClickListener(this);


    }

    public void setjoin() {
        if (!edtEmail.getText().toString().contains("@")) {
            Toast.makeText(context, "올바른 이메일 형식이 아닙니다.", Toast.LENGTH_SHORT).show();
        } else if (!edtPassword.getText().toString().equals(edtPasswordConfirm.getText().toString())) {
            Toast.makeText(context, "비밀번호를 올바르게 입력해주세요.", Toast.LENGTH_SHORT).show();
        } else {
            TaskUtil.getProgressDialog(context).show();
            //    ProgressDialog progressDialog = TaskUtil.getProgressDialog(context);
            //    progressDialog.show();
            if (edtNick.getText().toString().equals("")) {
                edtNick.setText(edtEmail.getText().toString().split("@")[0]);
            }
            ClutchUtil.User user = ClutchUtil.getUserInstance();
            user.register(edtEmail.getText().toString(), TaskUtil.md5(edtPassword.getText().toString()), edtNick.getText().toString(), "", "", 0, new Callback<ClutchUtil.User.LoginResult>() {
                @Override
                public void success(ClutchUtil.User.LoginResult loginResult, Response response) {
                    String errorMsg = ClutchUtil.Status.check(loginResult.status.code);
                    if (errorMsg.equals("")) {

                        SharedPreferences sp = getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("status_message", "");
                        editor.putString("picture", "");
                        editor.putString("name", edtNick.getText().toString());
                        editor.putInt("followers_count", 0);
                        editor.putInt("followings_count", 0);
                        editor.putString("userId", loginResult.userId);
                        editor.putString("userToken", loginResult.userToken);
                        editor.putBoolean("isLogin", true);
                        editor.putString("userEmail", edtEmail.getText().toString());
                        editor.commit();

                        //TaskUtil.setRegisterGCM(loginResult.userId, loginResult.userToken, getIntent().getStringExtra("regId"), true);

                        String endResult = TaskUtil.setRegisterGCM(loginResult.userId, loginResult.userToken, getIntent().getStringExtra("regId"), true);
                        if (!endResult.equals("")) Toast.makeText(context, endResult, Toast.LENGTH_SHORT).show();
                        else startActivity(new Intent(context, MainFragmentActivity.class));
                        finish();


                    } else {
                        Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    String errorMsg = ClutchUtil.Status.check(5000);
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    TaskUtil.getProgressDialog(context).dismiss();
                }
            });
            //progressDialog.dismiss();
            TaskUtil.getProgressDialog(context).dismiss();
        }
    }

    public void setJoinFb() {

        Session.openActiveSession(this, true, new Session.StatusCallback() {

            // callback when session changes state
            @Override
            public void call(Session session, SessionState state,
                             Exception exception) {
                if (session.isOpened()) {
                    Request.executeMeRequestAsync(session,
                            new Request.GraphUserCallback() {
                                @Override
                                public void onCompleted(GraphUser user, com.facebook.Response response) {
                                    if (user != null) {
                                        Session session = Session.getActiveSession();
                                        Log.e("sessionToken", session.getAccessToken());
                                        Log.e("sessionTokenDueDate", session.getExpirationDate().toLocaleString());
                                        Log.e("userId" , user.getName());
                                    }
                                }
                     });
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_exit:
                finish();
                break;
            case R.id.btn_join:
                if (TaskUtil.isNetworkAvailable(context)) {
                    setjoin();
                } else {
                    Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_fb_join:
                if (TaskUtil.isNetworkAvailable(context)) {

                    setJoinFb();
                    Log.e("sdfsf", "sdf");
                } else {
                    Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_twt_join:
                Toast.makeText(context, "트위터로 가입", Toast.LENGTH_SHORT).show();
                break;
            case R.id.text_provision:
                startActivity(new Intent(context, JoinProvisionActivity.class));
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }
}