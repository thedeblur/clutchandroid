package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import another.planet.clutch.R;
import another.planet.clutch.util.TaskUtil;

public class SearchActivity extends Activity implements View.OnClickListener {

    Context context;
    EditText edtSearch;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        context = this;
        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        LinearLayout btnBallon = (LinearLayout) findViewById(R.id.actionbar_ballon_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);
        edtSearch = (EditText) findViewById(R.id.edt_search);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        ((TextView) findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(context)));
        ImageButton btnEnter = (ImageButton) findViewById(R.id.btn_search);

        btnSearch.setVisibility(View.GONE); //Not needed search button on search window.
        titleTextView.setText("검색");

        btnBack.setOnClickListener(this);
        btnBallon.setOnClickListener(this);
        btnEnter.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_ballon_layout:
                Intent intent = new Intent(context, BallonStatusActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
            case R.id.btn_search:
                if (!edtSearch.getText().toString().trim().equals("")) {
                    Intent searchIntent = new Intent(context, SearchResultActivity.class);
                    searchIntent.putExtra("keyword", edtSearch.getText().toString());
                    startActivity(searchIntent);
                }
        }
    }
}