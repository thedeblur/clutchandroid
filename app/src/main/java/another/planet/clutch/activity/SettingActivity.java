package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.dialog.SetNotification;
import another.planet.clutch.dialog.SetPassword;
import another.planet.clutch.dialog.SetPrivacy;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import com.android.camera.CropImageIntentBuilder;

/**
 * Created by Marutian on 14. 2. 4.
 */
public class SettingActivity extends Activity implements View.OnClickListener, DialogInterface.OnDismissListener {

    Context context;
    private static final int SELECT_PICTURE = 1;
    private static final int CROP_PICTURE = 2;
    private static final String IMAGE_RESULT_PATH = Environment.getExternalStorageDirectory() + "/FakeGifty";
    private static final String CROP_IMAGE_PATH = "/.tempPic.jpg";
    private Uri mTempOutputUri;
    private boolean isCustomProfile = false;

    ImageView profileImage;
    public static Bitmap profileImageBitmap;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        context = this;
        ImageLoaderUtil.setDefImageConfig(context);

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        LinearLayout btnBallon = (LinearLayout) findViewById(R.id.actionbar_ballon_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        ((TextView) findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(context)));
        profileImage = (ImageView) findViewById(R.id.profile_image);
        titleTextView.setText("설정");

        btnBack.setOnClickListener(this);
        btnBallon.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        FrameLayout emailLayout = (FrameLayout) findViewById(R.id.layout_profile_email);
        FrameLayout nickLayout = (FrameLayout) findViewById(R.id.layout_profile_nick);
        FrameLayout passwdLayout = (FrameLayout) findViewById(R.id.layout_profile_passwd);
        FrameLayout notiLayout = (FrameLayout) findViewById(R.id.layout_profile_noti);
        FrameLayout privacyLayout = (FrameLayout) findViewById(R.id.layout_profile_privacy);
        FrameLayout noticeLayout = (FrameLayout) findViewById(R.id.layout_profile_notice);
        FrameLayout qnaLayout = (FrameLayout) findViewById(R.id.layout_profile_qna);
        FrameLayout contactLayout = (FrameLayout) findViewById(R.id.layout_profile_contact);
        LinearLayout logoutLayout = (LinearLayout) findViewById(R.id.btn_logout);

        emailLayout.setOnClickListener(this);
        nickLayout.setOnClickListener(this);
        passwdLayout.setOnClickListener(this);
        notiLayout.setOnClickListener(this);
        privacyLayout.setOnClickListener(this);
        noticeLayout.setOnClickListener(this);
        qnaLayout.setOnClickListener(this);
        contactLayout.setOnClickListener(this);
        logoutLayout.setOnClickListener(this);

        ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(TaskUtil.getUserDataString(context, "picture"), 200), profileImage, ImageLoaderUtil.imageLoaderOptions());

        //profileImage.setImageUrl(TaskUtil.getCustomImageURL(TaskUtil.getUserDataString(context, "picture"), 200),imageLoader);

        setUserData();
    }

    private void setUserData() {
        ((TextView) findViewById(R.id.profile_name)).setText(TaskUtil.getUserDataString(context, "name"));
        ((TextView) findViewById(R.id.profile_ment)).setText(TaskUtil.getUserDataString(context, "status_message"));
        ((TextView) findViewById(R.id.profile_email)).setText(TaskUtil.getUserDataString(context, "userEmail"));
        ((TextView) findViewById(R.id.profile_nick)).setText(TaskUtil.getUserDataString(context, "name"));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_ballon_layout:
                startActivity(new Intent(context, BallonStatusActivity.class));
                break;
            case R.id.actionbar_search:
                startActivity(new Intent(context, SearchActivity.class));
                break;
            case R.id.layout_profile_email:
                Toast.makeText(context, "이메일", Toast.LENGTH_SHORT).show();
                break;
            case R.id.layout_profile_nick:
                startActivity(new Intent(SettingActivity.this, ProfileActivity.class));
                break;
            case R.id.layout_profile_passwd:
                SetPassword pDialog = new SetPassword(context);
                pDialog.setOnDismissListener(this);
                pDialog.show();
                break;
            case R.id.layout_profile_noti:
                SetNotification nDialog = new SetNotification(context);
                nDialog.setOnDismissListener(this);
                nDialog.show();
                break;
            case R.id.layout_profile_privacy:
                SetPrivacy prDialog = new SetPrivacy(context);
                prDialog.setOnDismissListener(this);
                prDialog.show();
                break;
            case R.id.layout_profile_notice:
                Toast.makeText(context, "공지사항", Toast.LENGTH_SHORT).show();
                break;
            case R.id.layout_profile_qna:
                Toast.makeText(context, "QNA", Toast.LENGTH_SHORT).show();
                break;
            case R.id.layout_profile_contact:
                Toast.makeText(context, "문의메일", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_logout:
                SharedPreferences sp = context.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putBoolean("isLogin", false);
                editor.commit();
                this.finish();
                ((Activity) MainFragmentActivity.context).finish();

                //String endResult = TaskUtil.setRegisterGCM(TaskUtil.getUserId(context), TaskUtil.getUserToken(context), , true);
                //if(!endResult.equals("")) Toast.makeText(context, endResult, Toast.LENGTH_SHORT).show();

                startActivity(new Intent(context, StartClutchActivity.class));
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUserData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) return;

        if (requestCode == SELECT_PICTURE) {
            Uri selectedImage = data.getData();
            CropImageIntentBuilder intentBuilder = new CropImageIntentBuilder(480, 230, 480, 230, mTempOutputUri);
            intentBuilder.setSourceImage(selectedImage);
            intentBuilder.setScale(true);
            startActivityForResult(intentBuilder.getIntent(context), CROP_PICTURE);
        }

        if (requestCode == CROP_PICTURE) {
            profileImageBitmap = BitmapFactory.decodeFile(IMAGE_RESULT_PATH + CROP_IMAGE_PATH);
            profileImage.setImageBitmap(profileImageBitmap);
            isCustomProfile = true;
        }
    }

}