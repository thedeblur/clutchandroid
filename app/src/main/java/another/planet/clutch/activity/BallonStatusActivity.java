package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import another.planet.clutch.R;
import another.planet.clutch.util.TaskUtil;

public class BallonStatusActivity extends Activity implements View.OnClickListener {

    Context context;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ballon_status);

        context = this;

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        ((TextView) findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(context)));
        titleTextView.setText("풍선");

        btnBack.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        String youMessage[] = {"3위 EXO에게 113개 풍선을 기부하셨습니다."};
        String myMessage[] = {"아이유 사진이 풍선 4개를 받았습니다."};

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_search:
                Intent intent = new Intent(context, SearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
        }
    }

    public class MessageListAdapter extends ArrayAdapter<String> {

        public MessageListAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return convertView;
        }

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public int getItemViewType(int position) {
            return super.getItemViewType(position);
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }
    }
}