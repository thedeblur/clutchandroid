package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.dialog.ClutchDialog;
import another.planet.clutch.dialog.ClutchShareDialog;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import another.planet.clutch.widget.RoundedImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

public class MainDetailActivity extends Activity implements View.OnClickListener, DialogInterface.OnDismissListener {

    static Context context;

    ArrayList<CommentContents> array;
    DetailListAdapter dlAdapter;
    Construct.MainCard mc;

    String link = "";
    TextView txtClutchCount, txtLikeCount, txtCommentCount, linkText, txtCommentCountBar;
    LinearLayout likeClutch, cropClutch, commentClutch, commentCountBar;
    ImageView playVideo;
    boolean isLike = false;
    static Handler mDialogHandler;
    View header;

    class CommentContents {
        CommentContents(String commentId, String userId, String userName, String profileImage, String body, int likeCount, int unlikeCount, String date) {
            this.commentId = commentId;
            this.userId = userId;
            this.userName = userName;
            this.profileImage = profileImage;
            this.body = body;
            this.likeCount = likeCount;
            this.unlikeCount = unlikeCount;
            this.date = date;
        }

        String body, profileImage, userName, commentId, userId, date;
        int likeCount, unlikeCount;
    }

    public void onCreate(Bundle b) {
        context = this;
        super.onCreate(b);
        setContentView(R.layout.activity_detail);

        ImageLoaderUtil.setDefImageConfig(context);

        header = getLayoutInflater().inflate(R.layout.item_detail_contents, null);
        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        ImageButton btnMore = (ImageButton) findViewById(R.id.actionbar_more);
        ImageButton shareTwt = (ImageButton) findViewById(R.id.actionbar_twitter);
        ImageButton shareFb = (ImageButton) findViewById(R.id.actionbar_fb);
        cropClutch = (LinearLayout)header.findViewById(R.id.btn_detail_clutch);
        likeClutch = (LinearLayout)header.findViewById(R.id.btn_detail_like);
        commentClutch = (LinearLayout)header.findViewById(R.id.btn_detail_comment);
        commentCountBar = (LinearLayout)header.findViewById(R.id.comment_count_bar);
         linkText = (TextView) header.findViewById(R.id.text_detail_content_link);
        txtClutchCount = (TextView) header.findViewById(R.id.text_detail_clutch_count);
        txtLikeCount = (TextView) header.findViewById(R.id.text_detail_like_count);
        txtCommentCount = (TextView) header.findViewById(R.id.text_detail_comment_count);
        txtCommentCountBar = (TextView) header.findViewById(R.id.text_detail_bar_comment_count);
        playVideo = (ImageView) header.findViewById(R.id.play_video);

        btnBack.setOnClickListener(this);
        btnMore.setOnClickListener(this);
        shareTwt.setOnClickListener(this);
        shareFb.setOnClickListener(this);
        cropClutch.setOnClickListener(this);
        likeClutch.setOnClickListener(this);
        commentClutch.setOnClickListener(this);
        linkText.setOnClickListener(MainDetailActivity.this);
        playVideo.setOnClickListener(MainDetailActivity.this);

        if(TaskUtil.isNetworkAvailable(context))
            getClutch(getIntent().getStringExtra("postId"));
         else
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();

        mDialogHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                String bookId = msg.getData().getString("cb_id");
                //shareClutch(bookId);
            }
        };
    }
/*
    private void shareClutch(String bookId) {
        Log.e("clutchId", mc.postId);
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        clutch.shareClutch(mc.postId, TaskUtil.getUserToken(context), bookId, new Callback<ClutchUtil.Result>() {
            @Override
            public void success(ClutchUtil.Result result, Response response) {
                String errorMsg = ClutchUtil.Status.check(result.status.code);
                if(errorMsg.equals("")){
                    Toast.makeText(context, TaskUtil.SuccessMessage, Toast.LENGTH_SHORT).show();
                } else{
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }*/

    private void getClutch(String postId) {
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        clutch.getClutch(postId, TaskUtil.getUserToken(context), new Callback<ClutchUtil.Clutch.SingleClutchResult>() {
            @Override
            public void success(ClutchUtil.Clutch.SingleClutchResult singleClutchResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(singleClutchResult.status.code);
                Log.e("result", errorMsg);
                if (errorMsg.equals("")) {
                    String content_link = "";
                    if (singleClutchResult.clutch.content_type >= 1) {
                        content_link = singleClutchResult.clutch.content_link;
                    }

                    mc = new Construct.MainCard(singleClutchResult.clutch.clutchId,
                            singleClutchResult.clutch.content_body,
                            singleClutchResult.clutch.likeCount,
                            singleClutchResult.clutch.shared_times,
                            singleClutchResult.clutch.commentCount,
                            singleClutchResult.clutch.userName,
                            singleClutchResult.clutch.cb_name,
                            singleClutchResult.clutch.userPhoto,
                            singleClutchResult.clutch.content_pictures,
                            singleClutchResult.clutch.userId,
                            singleClutchResult.clutch.content_type,
                            content_link,
                            singleClutchResult.clutch.cb_id,
                            singleClutchResult.clutch.date,
                            singleClutchResult.clutch.content_picture_width,
                            singleClutchResult.clutch.content_picture_height);

                    isLike = singleClutchResult.clutch.isLiking;
                    if(isLike) likeClutch.setSelected(true);

                    ListView lv = (ListView) findViewById(R.id.detail_list);
                    lv.addHeaderView(header, -1, true);

                     if (mc.contentType != 0) {
                        link = mc.content_link;
                        linkText.setText(mc.content_link);
                        if (mc.contentType == 1)
                            playVideo.setVisibility(View.VISIBLE);
                    } else{
                        linkText.setVisibility(View.GONE);
                    }

                    ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(mc.content_pictures.get(0)), ((ImageView) header.findViewById(R.id.detail_image)), ImageLoaderUtil.imageLoaderOptions());
                    ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(mc.profileImage, 100), ((RoundedImageView) header.findViewById(R.id.post_writer_image)), ImageLoaderUtil.imageLoaderOptions());

                    ((TextView) header.findViewById(R.id.text_detail_content_writer)).setText(mc.profileName);
                    ((TextView) header.findViewById(R.id.text_detail_content_date)).setText(TaskUtil.getDate(mc.date));
                    ((TextView) header.findViewById(R.id.text_detail_content_ment)).setText(mc.mentMessage);
                    txtClutchCount.setText(Integer.toString(mc.clutchCount));
                    txtLikeCount.setText(Integer.toString(mc.likeCount));
                    txtCommentCount.setText(Integer.toString(mc.commentCount));
                    txtCommentCountBar.setText(Integer.toString(mc.commentCount));

                    getComments(0, TaskUtil.littlePagecount, true);
                    try {
                        if (array.size() != 0) lv.setAdapter(dlAdapter);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        lv.setEmptyView(findViewById(android.R.id.empty));
                    }

                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    private void getComments(int pageNo, int pageCount, final boolean isNew) { // isNew : 배열을 초기화하고 다시 불러와야하는가! 궁덕마쇼!
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        clutch.comments(getIntent().getStringExtra("postId"), pageNo, pageCount, TaskUtil.getUserToken(context), new Callback<ClutchUtil.Clutch.CommentResult>() {
            @Override
            public void success(ClutchUtil.Clutch.CommentResult commentResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(commentResult.status.code);

                if (errorMsg.equals("")) {
                    if (isNew) array = new ArrayList<CommentContents>();
                    for (ClutchUtil.CommentArray clutch : commentResult.comments) {
                        array.add(new CommentContents(
                                clutch.commentid,
                                clutch.userId,
                                clutch.userName,
                                clutch.userPhoto,
                                clutch.body,
                                clutch.likeCount,
                                clutch.unlikeCount,
                                clutch.date));
                    }
                    dlAdapter = new DetailListAdapter(context, 0);
                    if (isNew) ((ListView) findViewById(R.id.detail_list)).setAdapter(dlAdapter);
                    else dlAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    Log.e("ResultMessage", ClutchUtil.Status.check(response.getStatus()));
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "MainDetailActivity / getComments");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_more:
                ClutchShareDialog csdialog = new ClutchShareDialog(context);
                csdialog.show();
                break;
            /*case R.id.write:
                if (TaskUtil.isNetworkAvailable(context)) {
                    postComment(((EditText) findViewById(R.id.edt_comments)).getText().toString());
                } else {
                    Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
                }
                break;*/
            case R.id.text_detail_content_link:
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra("url", link);
                startActivity(intent);
                break;
            case R.id.btn_detail_clutch:
                ClutchDialog cDialog = new ClutchDialog(context);
                cDialog.show();
                break;
            case R.id.btn_detail_like:
                if (TaskUtil.isNetworkAvailable(context)) {
                    if (!isLike)
                        likeClutch();
                    else
                        unlikeClutch();
                } else {
                    Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.actionbar_twitter:
                Toast.makeText(context, "트위터로 공유", Toast.LENGTH_SHORT).show();
                break;
            case R.id.actionbar_fb:
                Toast.makeText(context, "페이스북으로 공유", Toast.LENGTH_SHORT).show();
                break;
            case R.id.play_video:
                Uri uri = Uri.parse(mc.content_link);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(i);
        }
    }


    private void likeClutch() {
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        clutch.like(getIntent().getStringExtra("postId"), TaskUtil.getUserToken(context), "", new Callback<ClutchUtil.Clutch.ClutchResult>() {
            @Override
            public void success(ClutchUtil.Clutch.ClutchResult clutchResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(response.getStatus());

                if (errorMsg.equals("")) {
                    likeClutch.setSelected(true);
                    int likecount = Integer.parseInt(txtLikeCount.getText().toString());
                    txtLikeCount.setText(Integer.toString(likecount + 1));
                    isLike = true;
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "MainDetailActivity / Like");
                }
            }
        });
    }

    private void unlikeClutch() {
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        clutch.dislike(getIntent().getStringExtra("postId"), TaskUtil.getUserToken(context), "", new Callback<ClutchUtil.Clutch.ClutchResult>() {
            @Override
            public void success(ClutchUtil.Clutch.ClutchResult clutchResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(response.getStatus());

                if (errorMsg.equals("")) {
                    likeClutch.setSelected(false);
                    int likecount = Integer.parseInt(txtLikeCount.getText().toString());
                    Log.e("aa", Integer.toString(likecount));
                    txtLikeCount.setText(Integer.toString(likecount - 1));
                    isLike = false;
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "MainDetailActivity / Like");
                }
            }
        });
    }
/*
    private void postComment(String comment) {
        if (!comment.equals("")) {
            ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
            clutch.postcomment(getIntent().getStringExtra("postId"), comment, TaskUtil.getUserToken(context), new Callback<ClutchUtil.Result>() {
                @Override
                public void success(ClutchUtil.Result result, Response response) {
                    String errorMsg = ClutchUtil.Status.check(response.getStatus());
                    if (errorMsg.equals("")) {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(((EditText) findViewById(R.id.edt_comments)).getWindowToken(), 0);
                        ((EditText) findViewById(R.id.edt_comments)).setText("");
                        getComments(0, 0, true);
                    } else {
                        Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    String errorMsg = ClutchUtil.Status.check(5000);
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    Log.e("Error Reason", retrofitError.getCause().toString());
                }
            });
        }
    }*/


    public class DetailListAdapter extends ArrayAdapter<String> {

        public DetailListAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_detail_comment_row, null);

            CommentContents contents = array.get(position);
            ((TextView) v.findViewById(R.id.text_comment_ment)).setText(contents.body);
            ((TextView) v.findViewById(R.id.text_comment_writer)).setText(contents.userName);
            ((TextView) v.findViewById(R.id.text_comment_date)).setText(TaskUtil.getDate(contents.date));
            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(contents.profileImage, 100), ((ImageView) v.findViewById(R.id.profile_image)), ImageLoaderUtil.imageLoaderOptions());
            //((NetworkImageView) v.findViewById(R.id.profile_image)).setImageUrl(TaskUtil.getCustomImageURL(contents.profileImage, 100), imageLoader);

            return v;
        }

        @Override
        public int getCount() {
            return array.size();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {

    }

}