package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.*;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import another.planet.clutch.R;

/**
 * Created by Marutian on 2014. 2. 15..
 */
public class WebViewActivity extends Activity {

    WebView mWebView;
    Context context;
    ProgressBar progressBar;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_webview);

        context = this;

        final String url = getIntent().getStringExtra("url");
        ((TextView) findViewById(R.id.webview_url)).setText(url);
        mWebView = (WebView) findViewById(R.id.webview);

        ((ImageView) findViewById(R.id.browser)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });


        progressBar = (ProgressBar) this.findViewById(R.id.progressbar);
        // mWeb.setWebViewClient(new WebViewClient());
        WebSettings set = mWebView.getSettings();
        set.setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        mWebView.setWebViewClient(new WebViewClient() { //KAKAOLINK OVERRIDE
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String overrideUrl) {
                view.stopLoading();
                if (overrideUrl.contains("close")) {
                    finish();
                } else {
                    if (overrideUrl.startsWith("kakaolink://")) {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(overrideUrl));
                        startActivity(i);

                        return true;
                    } else {
                        view.loadUrl(overrideUrl);

                    }
                }
                return false;
            }

            ;


            //SSL overriding
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }

            ;


            @Override
            public void onPageStarted(WebView view, String url,
                                      android.graphics.Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
            }

            ;

            @Override

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }

            ;

  /*          @Override
            public void onReceivedError(WebView view, int errorCode, //IF PAGE LOADING FAIL
                                        String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                Toast.makeText(context, "다음 오류로 로드하지 못함 : " + description,
                        Toast.LENGTH_LONG).show();
            };     */


        });

        mWebView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progressBar.setProgress(newProgress);
            }
        });


        mWebView.loadUrl(url);

    }
}
