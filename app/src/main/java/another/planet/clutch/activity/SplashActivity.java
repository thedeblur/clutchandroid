package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.TaskUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.io.File;

public class SplashActivity extends Activity {

    Context context;
    String TEMP_FILE_DIRECTORY = Environment.getExternalStorageDirectory() + "/Clutch";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        context = this;

        //이미지 혹은 임시파일 저장을 위한 파일폴더 생성 : 위치는 sdcard의 Clutch폴더
        if (!new File(TEMP_FILE_DIRECTORY).exists()) {
            new File(TEMP_FILE_DIRECTORY).mkdir();
        }

        Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (TaskUtil.isNetworkAvailable(context)) {
                    updateUserData();
                } else {
                    startActivity(new Intent(SplashActivity.this, MainFragmentActivity.class));
                    finish();
                }
            }
        };
        mHandler.sendEmptyMessageDelayed(0, 1000);

    }

    private void updateUserData() {
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        user.get(TaskUtil.getUserId(context), TaskUtil.getUserToken(context), new Callback<ClutchUtil.User.ProfileResult>() {
            @Override
            public void success(ClutchUtil.User.ProfileResult profileResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(response.getStatus());
                if (errorMsg.equals("")) {
                    SharedPreferences sp = getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("status_message", profileResult.profile.status_message);
                    editor.putString("picture", profileResult.profile.picture);
                    editor.putString("name", profileResult.profile.name);
                    editor.putInt("followers_count", profileResult.profile.followers_count);
                    editor.putInt("followings_count", profileResult.profile.followings_count);
                    editor.commit();
                    startActivity(new Intent(context, MainFragmentActivity.class));
                    finish();
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(SplashActivity.this, MainFragmentActivity.class));
                    finish();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "SplashActivity/UpdateUserData");
                }
                startActivity(new Intent(SplashActivity.this, MainFragmentActivity.class));
                finish();
            }
        });
    }
}