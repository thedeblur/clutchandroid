package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import another.planet.clutch.R;

public class BoardWriteActivity extends Activity implements View.OnClickListener {

    Context context;
    EditText edtTag, edtTitle, edtContent;
    LinearLayout btnShowTemp, btnPost;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freeb_post);

        context = this;

        btnShowTemp = (LinearLayout) findViewById(R.id.btn_temp_post);
        btnPost = (LinearLayout) findViewById(R.id.btn_save);
        edtTag = (EditText) findViewById(R.id.edt_tag);
        edtTitle = (EditText) findViewById(R.id.edt_title);
        edtContent = (EditText) findViewById(R.id.edt_contents);
        Button btnTemp = (Button) findViewById(R.id.btn_temp_save);
        ImageButton uploadPhoto = (ImageButton) findViewById(R.id.upload_photo);
        ImageButton uploadLink = (ImageButton) findViewById(R.id.upload_link);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_temp_post:
                Toast.makeText(context, "임시저장 보기", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_save:
                Toast.makeText(context, "저장", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_temp_save:
                Toast.makeText(context, "임시저장", Toast.LENGTH_SHORT).show();
                break;
            case R.id.upload_photo:
                Toast.makeText(context, "사진업로드", Toast.LENGTH_SHORT).show();
                break;
            case R.id.upload_link:
                Toast.makeText(context, "링크업로드", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}