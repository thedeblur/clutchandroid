package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import com.origamilabs.library.views.StaggeredGridView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

/**
 * Created by Marutian on 2014. 2. 15..
 */
public class OtherClutchActivity extends Activity implements View.OnClickListener {

    Context context;
    StaggeredGridView gridView;
    String name;
    int isFollowing;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_clutch);

        context = this;
        ImageLoaderUtil.setDefImageConfig(context);

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        LinearLayout btnBallon = (LinearLayout) findViewById(R.id.actionbar_ballon_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        ((TextView) findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(context)));
        TextView nameText = (TextView) findViewById(R.id.profile_name);
        TextView clutchText = (TextView) findViewById(R.id.cb_name);
        ImageView profileImageView = (ImageView) findViewById(R.id.profile_image);
        LinearLayout btnProfile = (LinearLayout) findViewById(R.id.profile_layout);
        LinearLayout btnFollow = (LinearLayout) findViewById(R.id.follow);

        titleTextView.setText(getIntent().getStringExtra("cbName"));
        //nameText.setText(getIntent().getStringExtra("userName"));
        //clutchText.setText(getIntent().getStringExtra("cbName"));

        //ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(getIntent().getStringExtra("userImage"), 100), profileImageView, ImageLoaderUtil.imageLoaderOptions());


        btnBack.setOnClickListener(this);
        btnBallon.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnProfile.setOnClickListener(this);
        btnFollow.setOnClickListener(this);

        if (TaskUtil.isNetworkAvailable(context)) {
            gridView = (StaggeredGridView) findViewById(R.id.grid);
            gridView.setPadding(16, 0, 16, 0);
            gridView.setItemMargin(16);


            /*if(isFollowing){
                ((TextView)findViewById(R.id.txtFollow)).setText("UnFollow");
            }
            else if(getIntent().getStringExtra("userId").equals(TaskUtil.getUserId(context))){
                ((LinearLayout) findViewById(R.id.follow)).setVisibility(View.INVISIBLE);
            }
            else{
                ((TextView)findViewById(R.id.txtFollow)).setText("Follow");
            }*/
            getClutch(TaskUtil.temp_sort, 0, TaskUtil.clutchPageCount);
        } else {
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
        }

        btnFollow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(context, "USER ID: "+userId+" 팔로잉", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getClutch(int sort, int pageNo, int pageCount) {
        ClutchUtil.ClutchBook cb = ClutchUtil.getClutchBookInstance();
        cb.getBook(getIntent().getStringExtra("userId"), getIntent().getStringExtra("cbId"), sort, pageNo, pageCount, TaskUtil.getUserToken(context), new Callback<ClutchUtil.Clutch.ClutchResult>() {
            @Override
            public void success(ClutchUtil.Clutch.ClutchResult clutchResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(clutchResult.status.code);
                if (errorMsg.equals("")) {
                    ArrayList<Construct.MainCard> array = new ArrayList<Construct.MainCard>();
                    for (ClutchUtil.ClutchArray clutch : clutchResult.clutches) {
                        String content_link = "";
                        if (clutch.content_type >= 1) {
                            content_link = clutch.content_link;
                        }

                        array.add(new Construct.MainCard(clutch.clutchId,
                                clutch.content_body,
                                clutch.likeCount,
                                clutch.shared_times,
                                clutch.commentCount,
                                clutch.userName,
                                clutch.cb_name,
                                clutch.userPhoto,
                                clutch.content_pictures,
                                clutch.userId,
                                clutch.content_type,
                                content_link,
                                clutch.cb_id,
                                clutch.date,
                                clutch.content_picture_width,
                                clutch.content_picture_height));
                        isFollowing = clutchResult.isFollowing;
                    }
                    ((TextView)findViewById(R.id.profile_name)).setText(array.get(0).profileName);
                    ((TextView)findViewById(R.id.cb_name)).setText(array.get(0).cb_name);
                    ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(array.get(0).profileImage, 100), (ImageView) findViewById(R.id.profile_image), ImageLoaderUtil.imageLoaderOptions());
                    name = array.get(0).profileName;

                    isFollowing = TaskUtil.getCBFollowing(getIntent().getStringExtra("userId"), getIntent().getStringExtra("cbId"), context);
                    if(isFollowing == 0)
                        ((TextView)findViewById(R.id.txtFollow)).setText("Follow");
                    else if(isFollowing == 1)
                        ((TextView)findViewById(R.id.txtFollow)).setText("UnFollow");
                    else
                        ((LinearLayout) findViewById(R.id.follow)).setVisibility(View.INVISIBLE);

                    ClutchAdapter cAdapter = new ClutchAdapter(context, R.layout.item_main, array);
                    gridView.setAdapter(cAdapter);
                    cAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "OtherClutchActivity / getClutches");
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_ballon_layout:
                startActivity(new Intent(context, BallonStatusActivity.class));
                break;
            case R.id.actionbar_search:
                startActivity(new Intent(context, SearchActivity.class));
                break;
            case R.id.profile_layout:
                Intent i = new Intent(context, OtherProfileActivity.class);
                i.putExtra("userName", name);
                i.putExtra("userId", getIntent().getStringExtra("userId"));
                startActivity(i);
                break;
            case R.id.follow:
                if(isFollowing == 1){
                    followCB(false);
                } else{
                    followCB(true);
                }
        }
    }

    private void followCB(boolean getFollow) {
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        if(getFollow){
            user.getCBFollow(getIntent().getStringExtra("userId"), getIntent().getStringExtra("cbId"), TaskUtil.getUserToken(context), new Callback<ClutchUtil.Result>() {
                @Override
                public void success(ClutchUtil.Result result, Response response) {
                    String errorMsg = ClutchUtil.Status.check(result.status.code);
                    if(errorMsg.equals("")){
                        changeFollow(1);
                        Toast.makeText(context, TaskUtil.SuccessMessage, Toast.LENGTH_SHORT).show();
                    } else{
                        Toast.makeText(context, TaskUtil.ErrorMessage, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {

                }
            });
        } else{
            user.getCBUnFollow(getIntent().getStringExtra("userId"), getIntent().getStringExtra("cbId"), TaskUtil.getUserToken(context), new Callback<ClutchUtil.Result>() {
                @Override
                public void success(ClutchUtil.Result result, Response response) {
                    String errorMsg = ClutchUtil.Status.check(result.status.code);
                    if(errorMsg.equals("")){
                        changeFollow(2);
                        Toast.makeText(context, TaskUtil.SuccessMessage, Toast.LENGTH_SHORT).show();
                    } else{
                        Toast.makeText(context, TaskUtil.ErrorMessage, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {

                }
            });
        }
    }

    private class ClutchAdapter extends ArrayAdapter<Object> {

        ArrayList<Construct.MainCard> array;

        public ClutchAdapter(Context context, int textViewResourceId,
                             ArrayList<Construct.MainCard> array) {
            super(context, textViewResourceId);

            this.array = array;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflate = LayoutInflater.from(getContext());
            convertView = inflate.inflate(R.layout.item_main, null);

            ((View) convertView.findViewById(R.id.writer_info)).setVisibility(View.GONE); // Hide Writer Information

            ImageView cardImageView = (ImageView) convertView.findViewById(R.id.card_image);

            final Construct.MainCard item = array.get(position);
            ((View) convertView.findViewById(R.id.filter_card)).setVisibility(View.GONE);
            ((View) convertView.findViewById(R.id.content_card)).setVisibility(View.VISIBLE);
            ((TextView) convertView.findViewById(R.id.ment)).setText(item.mentMessage);
            //cardImageView.setImageUrl(TaskUtil.getCustomImageURL(item.imageURL, 300) , imageLoader);
            ((TextView) convertView.findViewById(R.id.text_card_like_count)).setText(Integer.toString(item.likeCount));
            ((TextView) convertView.findViewById(R.id.text_card_clutch_count)).setText(Integer.toString(item.clutchCount));
            ((TextView) convertView.findViewById(R.id.text_card_comment_count)).setText(Integer.toString(item.commentCount));
            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(item.content_pictures.get(0), 300), cardImageView, ImageLoaderUtil.imageLoaderOptions());
            ImageView playVideo = (ImageView) convertView.findViewById(R.id.play_video);
            if(item.contentType == 1){
                playVideo.setVisibility(View.VISIBLE);
            }
            playVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri uri = Uri.parse(item.content_link);
                    Intent i = new Intent(Intent.ACTION_VIEW,uri);
                    startActivity(i);
                }
            });
            cardImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Construct.MainCard mc = array.get(position);
                    Intent intent = new Intent(context, MainDetailActivity.class);
                    intent.putExtra("postId", mc.postId);
                    startActivity(intent);
                }
            });

            cardImageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(context, "길게눌렀엉", Toast.LENGTH_SHORT).show();
                    Vibrator vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(50);
                    return true;
                }
            });

            return convertView;
        }

        @Override
        public int getCount() {
            return array.size();
        }
    }
    public void changeFollow(int newFollowValue){
        if(newFollowValue == 1){
            ((TextView)findViewById(R.id.txtFollow)).setText("Follow");
            isFollowing = 2;
        } else{
            ((TextView)findViewById(R.id.txtFollow)).setText("UnFollow");
            isFollowing = 1;
        }
    }
}