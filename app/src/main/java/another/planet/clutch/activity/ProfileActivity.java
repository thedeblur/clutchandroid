package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.util.TaskUtil;

/**
 * Created by toast on 2014. 3. 17..
 */
public class ProfileActivity extends Activity implements View.OnClickListener{
    Context context;
    EditText edtStatusMessage, edtGender, edtNick;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_status);
        context = this;

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        LinearLayout btnBallon = (LinearLayout) findViewById(R.id.actionbar_ballon_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        ((TextView) findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(context)));
        edtStatusMessage = (EditText) findViewById(R.id.edt_status);
        edtGender = (EditText) findViewById(R.id.edt_gender);
        edtNick = (EditText) findViewById(R.id.edt_nick);

        titleTextView.setText("프로필 설정");

        btnBack.setOnClickListener(this);
        btnBallon.setOnClickListener(this);

         findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 if (TaskUtil.isNetworkAvailable(context)) {
                     if (!edtStatusMessage.getText().toString().equals("") && !edtGender.getText().toString().equals(""))
                         edit();
                 } else {
                     Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
                 }
             }
         });
    }

    public void edit() {

        new AsyncTask<String, String, String>() {

            @Override
            protected void onPostExecute(String s) {
                if (!s.contains("error") && !s.equals("")) {
                    Toast.makeText(context, "성공", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                String endResult = TaskUtil.patchUserData(context,
                        edtNick.getText().toString(),
                        edtStatusMessage.getText().toString(),
                        null,
                        edtGender.getText().toString(), null);
                Log.e("endResult", endResult);
                return endResult;
            }
        }.execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_ballon_layout:
                Intent intent = new Intent(context, BallonStatusActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
        }
    }
}
