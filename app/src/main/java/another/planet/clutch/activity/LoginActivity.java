package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import another.planet.clutch.ClutchActionBar;
import another.planet.clutch.R;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.TaskUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends Activity implements View.OnClickListener {

    Context context;
    LinearLayout btnExit, loginFb, loginTwt, loginId, btnResetPw;
    EditText edtEmail, edtPassword;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ClutchActionBar cab = new ClutchActionBar(this);
        addContentView(cab.getMainBar(), cab.getLinearLayoutParams());

        context = this;

        btnExit = (LinearLayout) findViewById(R.id.btn_exit);
        loginFb = (LinearLayout) findViewById(R.id.btn_fb_login);
        loginTwt = (LinearLayout) findViewById(R.id.btn_twt_login);
        loginId = (LinearLayout) findViewById(R.id.btn_login);
        btnResetPw = (LinearLayout) findViewById(R.id.btn_reset_pw);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPassword = (EditText) findViewById(R.id.edt_password);

        btnExit.setOnClickListener(this);
        loginFb.setOnClickListener(this);
        loginTwt.setOnClickListener(this);
        loginId.setOnClickListener(this);
        btnResetPw.setOnClickListener(this);

        if (edtEmail.getText().length() > 2) {
            loginId.setBackgroundResource(R.drawable.button_style2);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_exit:
                finish();
                startActivity(new Intent(context, JoinActivity.class));
                break;
            case R.id.btn_fb_login:
                Toast.makeText(context, "페이스북으로 로그인", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_twt_login:
                Toast.makeText(context, "트위터로 로그인", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_login:
                if (TaskUtil.isNetworkAvailable(context)) {
                    login();
                } else {
                    Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_reset_pw:
                startActivity(new Intent(context, ResetPasswordActivity.class));
                break;
        }
    }

    public void login() {
        if (!edtEmail.getText().toString().equals("") && !edtPassword.getText().toString().equals("")) {
            ClutchUtil.User user = ClutchUtil.getUserInstance();
            user.login(edtEmail.getText().toString(), TaskUtil.md5(edtPassword.getText().toString()), new Callback<ClutchUtil.User.LoginResult>() {
                @Override
                public void success(ClutchUtil.User.LoginResult loginResult, Response response) {
                    String resultMsg = ClutchUtil.Status.check(loginResult.status.code);

                    if (resultMsg.equals("")) {
                        SharedPreferences sp = getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("userEmail", edtEmail.getText().toString());
                        editor.putString("status_message", loginResult.profile.status_message);
                        editor.putString("picture", loginResult.profile.picture);
                        editor.putString("name", loginResult.profile.name);
                        //editor.putInt("gender", loginResult.profile.gender);
                        editor.putInt("followers_count", loginResult.profile.followers_count);
                        editor.putInt("followings_count", loginResult.profile.followings_count);
                        editor.putString("userId", loginResult.userId);
                        editor.putString("userToken", loginResult.userToken);
                        editor.putBoolean("isLogin", true);
                        editor.commit();

                        String endResult = TaskUtil.setRegisterGCM(loginResult.userId, loginResult.userToken, getIntent().getStringExtra("regId"), true);

                        if (!endResult.equals("")) Toast.makeText(context, endResult, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(context, MainFragmentActivity.class));
                        finish();
                    } else {
                        Toast.makeText(context, resultMsg, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    String errorMsg = ClutchUtil.Status.check(5000);
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, "빈 칸을 기입해주세요!", Toast.LENGTH_SHORT).show();
        }
    }
}

