package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.CompoundButton;
import android.widget.TextView;
import another.planet.clutch.R;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import another.planet.clutch.util.UnitSizeUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.File;
import java.io.IOException;

public class StartClutchActivity extends Activity {

    Context context;
    WebView mWebView;
    TextView textLogin;
    View startLayout;
    String TEMP_FILE_DIRECTORY = Environment.getExternalStorageDirectory() + "/Clutch";

    GoogleCloudMessaging gcm;
    String regid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startclutch);

        context = this;
        ImageLoaderUtil.setStaticImageLoader(context);

        if(TaskUtil.checkPlayServices(getBaseContext())){
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = TaskUtil.getRegistrationId(context);

            if(regid.isEmpty()){
                registerInBackground();
            }
        }else{
            Log.i("Logging", "No valid Google Play Services APK found.");
        }

        if(TaskUtil.isLogin(context)){
            TaskUtil.saveUserStar(context);
            startActivity(new Intent(this, SplashActivity.class));
            finish();
        }

        //이미지 혹은 임시파일 저장을 위한 파일폴더 생성 : 위치는 sdcard의 Clutch폴더
        if(!new File(TEMP_FILE_DIRECTORY).exists()){
            new File(TEMP_FILE_DIRECTORY).mkdir();
        }

        starterPage();

    }


    public void starterPage() { //가입 전 보여주는 화면

        startLayout = (View) findViewById(R.id.start);
        textLogin = (TextView) findViewById(R.id.text_login);
        textLogin.setText(Html.fromHtml("<font color=\"#ffffff\">이미 클러치 회원이라면, </font><font color=\"#ffe23d\"><b>로그인</b> </font><font color=\"#ffffff\">궈궈</font>"));
        startLayout.setVisibility(View.VISIBLE);
        startLayout.startAnimation(AnimationUtils.loadAnimation(context, R.anim.dialog_up));

       final SwitchCompat switchCompat = (SwitchCompat)findViewById(R.id.start_switch);
        switchCompat.setChecked(false);
        switchCompat.setSwitchMinWidth(UnitSizeUtil.dp2px(context, 100));
        switchCompat.setThumbResource(R.drawable.switch_thumb_arrow);
        switchCompat.setTrackResource(R.drawable.switch_track);
        switchCompat.setOnClickListener(null);
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Intent i = new Intent(context, JoinActivity.class);
                    i.putExtra("regId", regid);
                    startActivity(i);
                    switchCompat.setChecked(false);
                }
            }
        });

        textLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, LoginActivity.class);
                i.putExtra("regId", regid);
                startActivity(i);
            }
        });

    }

    private void registerInBackground() {
        new AsyncTask() {
            @Override
            protected String doInBackground(Object[] objects) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(TaskUtil.SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    TaskUtil.sendRegistrationIdToBackend();

                    TaskUtil.storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(Object msg) {
            }
        }.execute(null, null, null);
    }
}