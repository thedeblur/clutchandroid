package another.planet.clutch.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import another.planet.clutch.R;

/**
 * Created by Marutian on 2014. 2. 19..
 */
public class JoinProvisionActivity extends Activity {

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_join_provision);

        ((WebView) findViewById(R.id.service_webview)).loadUrl("file:///android_asset/policies.html");
        ((WebView) findViewById(R.id.post_webview)).loadUrl("file:///android_asset/post_maintenace.html");
        ((WebView) findViewById(R.id.control_webview)).loadUrl("file:///android_asset/post_restriction.html");
        ((LinearLayout) findViewById(R.id.btn_exit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
