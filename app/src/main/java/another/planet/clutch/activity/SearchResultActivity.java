package another.planet.clutch.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.fragment.search.ResultClutchBookFragment;
import another.planet.clutch.fragment.search.ResultClutchFragment;
import another.planet.clutch.util.TaskUtil;


public class SearchResultActivity extends FragmentActivity implements View.OnClickListener {

    LinearLayout tabClutch, tabClutchBook;
    Context context;
    ViewPager mPager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        context = this;

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        LinearLayout btnBallon = (LinearLayout) findViewById(R.id.actionbar_ballon_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        ((TextView) findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(context)));
        ((EditText) findViewById(R.id.edt_search)).setText(getIntent().getStringExtra("keyword"));
        tabClutch = (LinearLayout) findViewById(R.id.tab_search_clutch);
        tabClutchBook = (LinearLayout) findViewById(R.id.tab_search_clutchbook);
        ImageView btnDelete = (ImageButton) findViewById(R.id.btn_delete);

        titleTextView.setText("검색");

        btnBack.setOnClickListener(this);
        btnBallon.setOnClickListener(this);
        tabClutch.setOnClickListener(this);
        tabClutchBook.setOnClickListener(this);

        selectTab(0);
        ResultAdapter adapter = new ResultAdapter(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.search_resultpager);
        mPager.setAdapter(adapter);
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int arg0) {
                selectTab(arg0);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_ballon_layout:
                Intent intent = new Intent(context, BallonStatusActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
            case R.id.btn_delete:
                finish();
                break;
            case R.id.tab_search_clutch:
                mPager.setCurrentItem(0);
                break;
            case R.id.tab_search_clutchbook:
                mPager.setCurrentItem(1);
                break;
        }
    }

    // 현재 보이는 탭을 포커스처리함
    public void selectTab(int position) {
        switch (position) {
            case 0:
                initTabImage();
                tabClutch.setSelected(true);
                break;
            case 1:
                initTabImage();
                tabClutchBook.setSelected(true);
                break;
        }
    }

    public void initTabImage() {
        //탭의 배경을 초기화한다.
        tabClutch.setSelected(false);
        tabClutchBook.setSelected(false);
    }


    public class ResultAdapter extends FragmentPagerAdapter {

        private Fragment[] FRAGMENTS;

        public ResultAdapter(FragmentManager fm) {
            super(fm);
            FRAGMENTS = new Fragment[2];
            FRAGMENTS[0] = new ResultClutchFragment(context, getIntent().getStringExtra("keyword"));
            FRAGMENTS[1] = new ResultClutchBookFragment(context, getIntent().getStringExtra("keyword"));
        }

        @Override
        public Fragment getItem(int position) {
            return FRAGMENTS[position];
        }

        @Override
        public int getCount() {
            return FRAGMENTS.length;
        }

        @Override
        public void finishUpdate(View arg0) {
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
        }

    }
}