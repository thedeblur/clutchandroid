package another.planet.clutch.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import another.planet.clutch.R;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import another.planet.clutch.widget.RemovableImageLayout;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class FavoriteSettingActivity extends Activity implements View.OnClickListener {

    Context context = FavoriteSettingActivity.this;
    ArrayList<Construct.RankList> array;
    ArrayList<Construct.ArtistList> myFavoriteArray;
    RankListAdapter rAdapter;
    View myList;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_setting);

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        titleTextView.setText("관심인물 설정");

        btnBack.setOnClickListener(this);

        if (TaskUtil.isNetworkAvailable(context)) {
            ImageLoaderUtil.setDefImageConfig(context);

            //getMyFavoriteListLocal(); // 미리 저장해둔 관심인물을 재빨리 파싱한다.
            getMyFavoriteList(true); // 다시 갱신하기 위해 가져온다.
            getRank(0, 0, TaskUtil.rankListCount, true); // 랭킹을 불러온다.
        } else {
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
        }
        //FavoriteSettingAdapter adapter = new FavoriteSettingAdapter(context, R.layout.item_favorite_rank);
        //lv.setAdapter(adapter);

    }

    private void getMyFavoriteList(final boolean getFirst) {
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        user.getUserArtists(TaskUtil.getUserId(context), TaskUtil.getUserToken(context), new Callback<ClutchUtil.User.ArtistResult>() {
            @Override
            public void success(ClutchUtil.User.ArtistResult artistResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(artistResult.status.code);
                if (errorMsg.equals("")) {

                    if (getFirst) {
                        myList = getLayoutInflater().inflate(R.layout.item_favorite_setting_header, null);
                    } else {
                        //       ((ListView) findViewById(R.id.favorite_setting_list)).removeFooterView(myList);
                    }
                    myFavoriteArray = new ArrayList<Construct.ArtistList>();
                    for (int i = 0; i < artistResult.artists.size(); i++) { // foreach 식으로 쓰고 싶었는데 addView를 위해 바꾸자
                        myFavoriteArray.add(new Construct.ArtistList(
                                artistResult.artists.get(i)._id,
                                artistResult.artists.get(i).name,
                                artistResult.artists.get(i).profile_picture,
                                artistResult.artists.get(i).category,
                                artistResult.artists.get(i).starCount,
                                artistResult.artists.get(i).weeklyStarCount));
                        addMyFavoriteList(i, myFavoriteArray, myList);
                    }
                    ((ListView) findViewById(R.id.favorite_setting_list)).removeHeaderView(myList);
                    ((ListView) findViewById(R.id.favorite_setting_list)).addHeaderView(myList, -1, true);
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "FavoriteSettingActivity / getUserArtists");
                }
            }
        });
    }

    private void getRank(int prevweek, int pageNo, int pageCount, final boolean getFirst) {
        ClutchUtil.Ranking rank = ClutchUtil.getRankingInstance();
        rank.getRanking(prevweek, pageNo, pageCount, new Callback<ClutchUtil.Ranking.RankingResult>() {
            @Override
            public void success(ClutchUtil.Ranking.RankingResult rankingResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(rankingResult.status.code);
                if (errorMsg.equals("")) {
                    if (getFirst) array = new ArrayList<Construct.RankList>();
                    for (ClutchUtil.RankingArray rankList : rankingResult.ranks) {
                        array.add(new Construct.RankList(
                                rankList.rank,
                                rankList.name,
                                rankList.profile,
                                rankList.star,
                                rankList.artistId));
                    }

                    if (getFirst) {
                        rAdapter = new RankListAdapter(context, 0, array);
                        ((ListView) findViewById(R.id.favorite_setting_list)).setAdapter(rAdapter);
                    } else rAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "FavoriteSettingActivity / getRanking");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
        }
    }

    public class RankListAdapter extends ArrayAdapter<Object> {
        ArrayList<Construct.RankList> array;

        public RankListAdapter(Context context, int resource, ArrayList<Construct.RankList> array) {
            super(context, resource);
            this.array = array;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_favorite_rank, null);

            View item = (View) convertView.findViewById(R.id.list_item);
            final Construct.RankList rankList = array.get(position);
            final TextView number = (TextView) convertView.findViewById(R.id.numberic);
            ((TextView) convertView.findViewById(R.id.artist_name)).setText(rankList.name);
            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(rankList.profile, 150), ((ImageView) convertView.findViewById(R.id.artist_image)), ImageLoaderUtil.imageLoaderOptions());
            final ImageButton btnAdd = (ImageButton) convertView.findViewById(R.id.favorite_add);
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    throwAwayMyLIst(myList);
                    addMyArtist(rankList.artistId);
                }
            });

            if (myFavoriteArray.contains(rankList.artistId)) {
                btnAdd.setVisibility(View.INVISIBLE);
            } else {
                btnAdd.setVisibility(View.VISIBLE);
            }


            if (position == 0) {
                number.setBackgroundResource(R.drawable.rank_1st_number);
                number.setText(" ");
            } else {
                number.setText(Integer.toString(position + 1));
            }

            if (position == array.size() - 1) {
                item.setBackgroundResource(R.drawable.list_style2_bottom);
            }
            return convertView;
        }

        @Override
        public int getCount() {
            return array.size();
        }

    }

    private void addMyArtist(String artistId) {
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        user.addUserArtists(TaskUtil.getUserId(context), artistId, TaskUtil.getUserToken(context), new Callback<ClutchUtil.Result>() {
            @Override
            public void success(ClutchUtil.Result result, Response response) {
                String errorMsg = ClutchUtil.Status.check(result.status.code);
                if (errorMsg.equals("")) {
                    Toast.makeText(context, TaskUtil.SuccessMessage, Toast.LENGTH_SHORT).show();
                    getMyFavoriteList(false);
                    getRank(0, 0, TaskUtil.rankListCount, true);
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "FavoriteSettingActivity / addArtist");
                }
            }
        });
    }

    public void addMyFavoriteList(final int position, final ArrayList<Construct.ArtistList> myFavoriteArray, View mylist) {

        final View mylistItem = getLayoutInflater().inflate(R.layout.item_favorite, null);
        final View item = mylistItem.findViewById(R.id.list_item);
        final LinearLayout root = (LinearLayout) mylist.findViewById(R.id.favorite_person_layout);
        final TextView name = (TextView) mylistItem.findViewById(R.id.name);

        name.setText(myFavoriteArray.get(position).name);
        mylistItem.findViewById(R.id.favorite_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClutchUtil.User user = ClutchUtil.getUserInstance();
                user.deleteUserArtists(TaskUtil.getUserId(context), myFavoriteArray.get(position).artistId, TaskUtil.getUserToken(context), new Callback<ClutchUtil.Result>() {
                    @Override
                    public void success(ClutchUtil.Result result, Response response) {
                        String errorMsg = ClutchUtil.Status.check(result.status.code);
                        if (errorMsg.equals("")) {
                            Toast.makeText(context, TaskUtil.SuccessMessage, Toast.LENGTH_SHORT).show();
                            try {
                                root.removeViewAt(position);
                            } catch (Exception e) {
                            }
                        } else {
                            Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        String errorMsg = ClutchUtil.Status.check(5000);
                        Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                        try {
                            Log.e("Error Reason", retrofitError.getCause().toString());
                        } catch (Exception e) {
                            Log.e("UnKnownError", "FavoriteSettingActivity / deleteArtist");
                        }
                    }
                });

            }


        });
        if (position == myFavoriteArray.size() - 1) {
            item.setBackgroundResource(R.drawable.list_style2_bottom);
        } else {
            item.setBackgroundResource(R.drawable.list_style2_center);
        }

        RemovableImageLayout tempItem = new RemovableImageLayout(this);
        tempItem.setImageResource(R.drawable.temp_image);
        tempItem.setRemoveButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "삭쪠", Toast.LENGTH_SHORT).show();
            }
        });
        ((LinearLayout) mylist.findViewById(R.id.favorite_person_layout)).addView(tempItem, position);

    }

    public void throwAwayMyLIst(View mylist) {
        ((LinearLayout) mylist.findViewById(R.id.favorite_person_layout)).removeAllViewsInLayout();
    }
}