package another.planet.clutch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Copyright 2014 Deblur. All rights reserved.
 *
 * @author Shin Gwangsu (maruroid@gmail.com)
 * @since 14. 11. 25.
 */
public class ClutchActionBar extends View  {

    private Context mContext;
    View actionbar;

    public ClutchActionBar(Context context) {
        super(context);
        this.mContext = context;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        actionbar = inflater.inflate(R.layout.test_kitkat, null);
    }

    public View getMainBar() {
/*
        ImageView btnBack = (ImageView)actionbar.findViewById(R.id.actionbar_back);
        TextView titleText = (TextView)actionbar.findViewById(R.id.actionbar_text);

        btnBack.setVisibility(View.GONE);  //Not needed back button on main page.
        titleText.setVisibility(View.GONE);

        ((ImageView)actionbar.findViewById(R.id.actionbar_logo)).setVisibility(View.VISIBLE);
        ((TextView)actionbar.findViewById(R.id.actionbar_text)).setVisibility(View.GONE);
        ((TextView)actionbar.findViewById(R.id.actionbar_ballon_count)).setText(Integer.toString(TaskUtil.getUserStar(mContext)));
*/
        return actionbar;
    }

    
    public LinearLayout.LayoutParams getLinearLayoutParams(){
        return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }
    
    public FrameLayout.LayoutParams getFrameLayoutParams(){
        return new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
    }

    public RelativeLayout.LayoutParams getRelativeLayoutParams(){
        return new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    }
/*
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.actionbar_back:
                ((Activity)mContext).finish();

            case R.id.actionbar_ballon_layout:
                mContext.startActivity(new Intent(mContext, BallonStatusActivity.class));
                break;
            case R.id.actionbar_search:
                mContext.startActivity(new Intent(mContext, SearchActivity.class));
                break;
        }
        }*/
}

