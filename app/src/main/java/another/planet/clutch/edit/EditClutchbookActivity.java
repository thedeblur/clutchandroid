package another.planet.clutch.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import another.planet.clutch.R;
import another.planet.clutch.activity.BallonStatusActivity;
import another.planet.clutch.activity.SearchActivity;

/**
 * Created by Marutian on 2014. 3. 18..
 */
public class EditClutchbookActivity extends Activity implements View.OnClickListener {

    Context context;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_cb);

        context = this;

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.actionbar_back_layout);
        LinearLayout btnBallon = (LinearLayout) findViewById(R.id.actionbar_ballon_layout);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.actionbar_search);
        TextView titleTextView = (TextView) findViewById(R.id.actionbar_text);
        FrameLayout btnBookCover = (FrameLayout) findViewById(R.id.layout_bookcover);
        FrameLayout btnTag = (FrameLayout) findViewById(R.id.layout_tag);
        LinearLayout btnSave = (LinearLayout) findViewById(R.id.btn_save);
        LinearLayout btnDelete = (LinearLayout) findViewById(R.id.btn_delete);

        titleTextView.setText("클러치북 수정");

        btnBack.setOnClickListener(this);
        btnBallon.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnBookCover.setOnClickListener(this);
        btnTag.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnDelete.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.actionbar_back_layout:
                finish();
                break;
            case R.id.actionbar_ballon_layout:
                startActivity(new Intent(context, BallonStatusActivity.class));
                break;
            case R.id.actionbar_search:
                startActivity(new Intent(context, SearchActivity.class));
                break;
            case R.id.layout_bookcover:
                startActivity(new Intent(context, EditBookCoverActivity.class));
                break;
            case R.id.layout_tag:
                startActivity(new Intent(context, EditTagActivity.class));
                break;
            case R.id.btn_save:
                break;
            case R.id.btn_delete:
                break;
        }
    }
}