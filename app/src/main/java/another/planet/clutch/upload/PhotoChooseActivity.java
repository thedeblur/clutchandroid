package another.planet.clutch.upload;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import another.planet.clutch.util.UnitSizeUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageLoadingProgressListener;

import java.util.ArrayList;

public class PhotoChooseActivity extends Activity implements View.OnClickListener {

    Context context;
    String[] imagePath;
    ArrayList<String> selectedImage;
    boolean[] isSelected;


    @Override
    public void onCreate(Bundle savedInstantState) {
        super.onCreate(savedInstantState);
        setContentView(R.layout.activity_upload_picture_pick);

        context = this;

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.btn_back);
        LinearLayout btnAlbum = (LinearLayout) findViewById(R.id.btn_album);
        Button btnDone = (Button) findViewById(R.id.btn_done);

        btnBack.setOnClickListener(this);
        btnAlbum.setOnClickListener(this);

        ImageLoaderUtil.setPickImageConfig(context);

        //GET Images URI via MediaStore
        String[] projection = {MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA};
        Cursor imageCursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Images.Media._ID + " DESC");

        int count = imageCursor.getCount();
        int image_column_index = imageCursor.getColumnIndex(MediaStore.Images.Media._ID);
        int image_path_index = imageCursor.getColumnIndex(MediaStore.Images.Media.DATA);

        imagePath = new String[count];
        isSelected = new boolean[count];
        final Bitmap[] bm = new Bitmap[count];
        selectedImage = new ArrayList<String>();

        for (int i = 0; i < count; i++) {
            imageCursor.moveToPosition(i);
            imagePath[i] = "file:///" + imageCursor.getString(image_path_index);
            //bm[i] = MediaStore.Images.Thumbnails.getThumbnail(getApplicationContext().getContentResolver(), id, MediaStore.Images.Thumbnails.MICRO_KIND, null);
        }
        //   imageCursor.close();


        GridView grid = (GridView) findViewById(R.id.grid);
        grid.setAdapter(new ImageAdapter(context, imagePath));
        btnDone.setVisibility(View.GONE); // 여기선 한장의 이미지만 선택해야하므로 완료버튼이 필요없다.
 /*   한장의 사진을 선택할땐, 주석처리한다. btnDone 도 역시 View.GONE 처리 해주어야함.
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(selectedImage.size() > 0){
                    TaskUtil.cleanImageCache(TaskUtil.MEMORY_CLEAR);
                    Message msg = new Message();
                    Bundle data = new Bundle();
                    data.putStringArrayList("fileList" , selectedImage);
                    msg.setData(data);
                    PhotoUploadActivity.PictureHandler.sendMessage(msg);
                    finish();
                }else{
                    Toast.makeText(context, "한개 이상의 사진을 선택하세요.", Toast.LENGTH_SHORT).show();
                }

            }
        });                         */

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                ImageLoaderUtil.cleanImageCache(ImageLoaderUtil.ALL_CACHE_CLEAR);
                finish();
                break;
            case R.id.btn_album:
                Toast.makeText(context, "앨범", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public class ImageAdapter extends BaseAdapter {

        Context context;
        String[] imagePath;
        int cellSize;

        public ImageAdapter(Context context, String[] imagePath) {
            this.context = context;
            this.imagePath = imagePath;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            cellSize = UnitSizeUtil.dp2px(context, 108);
        }

        @Override
        public int getCount() {
            return imagePath.length + 1;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


        @Override
        public View getView(final int index, View v, ViewGroup parent) {

            View convertView;
            if (index == 0) {
                convertView = getLayoutInflater().inflate(R.layout.item_list_choose_add, null);
                ((ImageView) convertView.findViewById(R.id.icon)).setImageResource(R.drawable.gallery_ic_capture);
                convertView.findViewById(R.id.frame).setLayoutParams(new LinearLayout.LayoutParams(cellSize, cellSize));

                convertView.findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "사진찍기", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                final int position = index - 1; //0번 아이템은 특정 동작을 위해 사용함
                convertView = getLayoutInflater().inflate(R.layout.item_list_choose_image, null);

                final FrameLayout frameLayout = (FrameLayout) convertView.findViewById(R.id.frame);
                final LinearLayout loadLayout = (LinearLayout) convertView.findViewById(R.id.load);
                final ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
                final LinearLayout checkLayout = (LinearLayout) convertView.findViewById(R.id.select);
                final Button btnSelect = (Button) convertView.findViewById(R.id.btn_select);

                frameLayout.setLayoutParams(new LinearLayout.LayoutParams(cellSize, cellSize));

                btnSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //아래의 문장들은 한장의 사진을 선택할때만 사용합니다.
                        selectedImage.add(imagePath[position]);
                        ImageLoaderUtil.cleanImageCache(ImageLoaderUtil.MEMORY_CLEAR);
                        Message msg = new Message();
                        Bundle data = new Bundle();
                        data.putStringArrayList("fileList", selectedImage);
                        msg.setData(data);
                        PhotoUploadActivity.PictureHandler.sendMessage(msg);
                        finish();

                        /*  사진을 5장 선택해야 하는 경우에 이것들을 사용
                        if (selectedImage.contains(imagePath[position])) {
                            checkLayout.setVisibility(View.GONE);
                            int savedIndex = selectedImage.indexOf(imagePath[position]); //삭제할 아이템의 내용과 비교해서 인덱스찾기
                            selectedImage.remove(savedIndex);   //그리고 어레이에서 떨궈내기
                        } else {
                            if(selectedImage.size() < 2){
                                checkLayout.setVisibility(View.VISIBLE);
                                selectedImage.add(imagePath[position]);
                            }else{
                                Toast.makeText(context, "최대 5장까지 선택할 수 있습니다.", Toast.LENGTH_SHORT).show();
                            }
                        }  */
                    }
                });

                if (selectedImage.contains(imagePath[position])) {
                    checkLayout.setVisibility(View.VISIBLE);
                } else {
                    checkLayout.setVisibility(View.GONE);
                }


                ImageLoaderUtil.imageLoader.displayImage(imagePath[position], imageView, ImageLoaderUtil.imageLoaderOptions(), new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                loadLayout.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view,
                                                        FailReason failReason) {
                                loadLayout.setVisibility(View.GONE);
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                loadLayout.setVisibility(View.GONE);
                            }

                            @Override
                            public void onLoadingCancelled(String s, View view) {
                                loadLayout.setVisibility(View.VISIBLE);
                            }

                        }, new ImageLoadingProgressListener() {
                            @Override
                            public void onProgressUpdate(String imageUri, View view, int current,
                                                         int total) {
                                loadLayout.setVisibility(View.VISIBLE);
                            }
                        }
                );

            }
            return convertView;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ImageLoaderUtil.cleanImageCache(ImageLoaderUtil.ALL_CACHE_CLEAR);
    }
}