package another.planet.clutch.upload;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.adapter.ImagesAdapter;
import another.planet.clutch.dialog.ChooseBookDialog;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.TaskUtil;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

/**
 * Created by Marutian on 2014. 4. 5..
 */
public class PhotoUploadActivity extends Activity implements View.OnClickListener, DialogInterface.OnDismissListener {

    Context context;
    int AFTER_PICK = 77;
    public static Handler mDialogHandler, PictureHandler;

    ArrayList<String> fileList = new ArrayList<String>();
    ArrayList<String> selectedList = new ArrayList<String>();

    boolean isExistMissing; // 지난번에 못쓴 게시물이 있느냐
    ImageView[] thumbImage = new ImageView[2];
    TextView nameText;
    String bookId, bookName;
    String MetaId = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_picture);

        context = this;

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.btn_back);
        ImageButton btnAdd = (ImageButton) findViewById(R.id.btn_add);
        LinearLayout btnTag = (LinearLayout) findViewById(R.id.layout_tag);
        LinearLayout btnCb = (LinearLayout) findViewById(R.id.layout_cb);
        Button btnSave = (Button) findViewById(R.id.btn_save);
        nameText = (TextView) findViewById(R.id.text_cb);

        btnBack.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnTag.setOnClickListener(this);
        btnCb.setOnClickListener(this);

        final ViewPager pager = (ViewPager) findViewById(R.id.image_pager);
        pager.setBackgroundColor(getResources().getColor(R.color.White));
        pager.setPageMargin(12);

        /*isExistMissing = TaskUtil.getDismissPost(context);
        if(isExistMissing){
            SharedPreferences sp = getSharedPreferences("clutch_data" , Context.MODE_PRIVATE);
            try {
                JSONArray jArray = new JSONArray(sp.getString("pictures" , ""));
                for(int i=0; i < jArray.length(); i++){
                    fileList.add(jArray.optString(i));
                    TaskUtil.imageLoader.displayImage(jArray.optString(i), thumbImage[i]);
                    thumbImage[i].setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }*/

        mDialogHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                bookId = msg.getData().getString("cb_id");
                bookName = msg.getData().getString("cb_name");
                nameText.setText(bookName);
            }
        };
        PictureHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                fileList = msg.getData().getStringArrayList("fileList");
                ImagesAdapter pagerAdapter = new ImagesAdapter(context, fileList);
                pager.setBackgroundColor(getResources().getColor(R.color.transparent));
                pager.setAdapter(pagerAdapter);
            }
        };


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PhotoChooseActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_save:
                if (TaskUtil.isNetworkAvailable(context)) {
                    if (bookId != null && !bookId.equals("") && !fileList.get(0).equals("") && fileList.get(0) != null) {
                        post();
                    }
                } else {
                    Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.layout_tag:
                Intent intent = new Intent(context, UploadTagActivity.class);
                startActivity(intent);
                break;
            case R.id.layout_cb:
                ChooseBookDialog chooseDialog = new ChooseBookDialog(context, mDialogHandler);
                chooseDialog.setOnDismissListener(this);
                chooseDialog.show();
                break;
        }
    }

    private void post() {

        new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                //TaskUtil.getProgressDialog(context).show();
            }

            @Override
            protected void onPostExecute(String s) {
                ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
                clutch.post("", bookId, 0, MetaId, "", "", ((EditText) findViewById(R.id.edt_contents)).getText().toString(), TaskUtil.getUserToken(context), new Callback<ClutchUtil.Clutch.ClutchResult>() {
                    @Override
                    public void success(ClutchUtil.Clutch.ClutchResult clutchResult, Response response) {
                        String errorMsg = ClutchUtil.Status.check(clutchResult.status.code);
                        if (errorMsg.equals("")) {
                            TaskUtil.getProgressDialog(context).dismiss();
                            Toast.makeText(context, TaskUtil.SuccessMessage, Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {

                    }
                });
            }

            @Override
            protected String doInBackground(String... strings) {
                for (int i = 0; i < fileList.size(); i++) {
                    String endResult = TaskUtil.uploadImage(context, Uri.parse(fileList.get(i)));
                    if (!endResult.contains("error") && !endResult.equals("")) {
                        try {
                            JSONObject jObject = new JSONObject(endResult);
                            MetaId = MetaId + jObject.getString("imageId") + " ";
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            JSONObject jObject = new JSONObject(endResult);
                            Log.e("ErrorMessage", jObject.getJSONObject("status").getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
                MetaId = MetaId.trim();
                return null;
            }
        }.execute();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {

    }
}