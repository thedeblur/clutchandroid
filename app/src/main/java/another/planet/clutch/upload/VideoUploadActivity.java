package another.planet.clutch.upload;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.dialog.ChooseBookDialog;
import another.planet.clutch.dialog.VideoUrlDialog;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Marutian on 2014. 4. 5..
 */
public class VideoUploadActivity extends Activity implements View.OnClickListener, DialogInterface.OnDismissListener {

    Context context;
    public static Handler videoHandler, mDialogHandler;
    String thumbId;
    TextView nameText;
    String bookId, bookName, videoUrl;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_video);

        context = this;

        ImageLoaderUtil.setDefImageConfig(context);
        LinearLayout btnBack = (LinearLayout) findViewById(R.id.btn_back);
        ImageView btnAdd = (ImageView) findViewById(R.id.thumb_video);
        LinearLayout btnTag = (LinearLayout) findViewById(R.id.layout_tag);
        LinearLayout btnCb = (LinearLayout) findViewById(R.id.layout_cb);
        Button btnSave = (Button) findViewById(R.id.btn_save);
        nameText = (TextView) findViewById(R.id.text_cb);

        btnBack.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnTag.setOnClickListener(this);
        btnCb.setOnClickListener(this);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoUrlDialog videoDialog = new VideoUrlDialog(context);
                videoDialog.setOnDismissListener(VideoUploadActivity.this);
                videoDialog.show();
            }
        });

        videoHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                videoUrl = msg.getData().getString("videoUrl");
                if (TaskUtil.isNetworkAvailable(context)) {
                    getThumb(videoUrl);
                } else {
                    Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
                }
            }
        };
        mDialogHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                bookId = msg.getData().getString("cb_id");
                bookName = msg.getData().getString("cb_name");
                nameText.setText(bookName);
            }
        };
    }

    private void getThumb(String videoUrl) {
        if(videoUrl.contains("https://")) videoUrl = videoUrl.replace("https://" , "http://");

        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        clutch.getVideo(videoUrl, new Callback<ClutchUtil.Clutch.VideoResult>() {
            @Override
            public void success(ClutchUtil.Clutch.VideoResult videoResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(videoResult.status.code);
                if (errorMsg.equals("")) {
                    thumbId = videoResult.thumb_id;
                    ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(thumbId, 500), ((ImageView) findViewById(R.id.thumb_video)), ImageLoaderUtil.imageLoaderOptions());
                } else {
                    Toast.makeText(context, videoResult.status.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_save:
                if (TaskUtil.isNetworkAvailable(context)) {
                    if (bookId != null && !bookId.equals("") && !thumbId.equals("") && thumbId != null) {
                        post();
                    }
                } else {
                    Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.layout_tag:
                Intent intent = new Intent(context, UploadTagActivity.class);
                startActivity(intent);
                break;
            case R.id.layout_cb:
                ChooseBookDialog chooseDialog = new ChooseBookDialog(context, mDialogHandler);
                chooseDialog.setOnDismissListener(this);
                chooseDialog.show();
                break;
        }
    }

    private void post() {
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        clutch.post("", bookId, 1, thumbId, videoUrl, "", ((EditText) findViewById(R.id.edt_contents)).getText().toString(), TaskUtil.getUserToken(context), new Callback<ClutchUtil.Clutch.ClutchResult>() {
            @Override
            public void success(ClutchUtil.Clutch.ClutchResult clutchResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(clutchResult.status.code);
                if (errorMsg.equals("")) {
                    Toast.makeText(context, TaskUtil.SuccessMessage, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {

    }
}