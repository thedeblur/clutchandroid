package another.planet.clutch.upload;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.dialog.ChooseBookDialog;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import another.planet.clutch.util.UnitSizeUtil;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

/**
 * Created by Marutian on 2014. 4. 5..
 */
public class LinkUploadActivity extends Activity implements View.OnClickListener, DialogInterface.OnDismissListener{

    Context context;
    ArrayList<String> imageArray;
    ArrayList<String> selectedImage;
    boolean[] isSelected;

    TextView nameText;
    EditText edtContent;
    public static Handler mDialogHandler;
    String bookId,bookName;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_link);

        context = this;

        if(TaskUtil.isNetworkAvailable(context)){
            ImageLoaderUtil.setDefImageConfig(context);
            getImages(getIntent().getStringExtra("url"));
        } else{
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
        }

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        mDialogHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                bookId = msg.getData().getString("cb_id");
                bookName = msg.getData().getString("cb_name");
                nameText.setText(bookName);
            }
        };
    }


    private void getImages(String url) {
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        clutch.getLinkImages(url, new Callback<ClutchUtil.Clutch.LinkResult>() {
            @Override
            public void success(ClutchUtil.Clutch.LinkResult linkResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(linkResult.status.code);
                if(errorMsg.equals("")){
                    imageArray = new ArrayList<String>();
                    selectedImage = new ArrayList<String>();
                    for(int i=0; i < linkResult.image_urls.size(); i++){
                        imageArray.add(linkResult.image_urls.get(i));
                    }

                    CollectImageAdapter adapter = new CollectImageAdapter();

                    StickyGridHeadersGridView grid = (StickyGridHeadersGridView)findViewById(R.id.grid);
                    int dp8 = (int) UnitSizeUtil.dp2px(context, 8);
                    grid.setPadding(dp8, dp8, dp8, dp8);
                    grid.setNumColumns(3);
                    grid.setHorizontalSpacing(dp8);
                    grid.setVerticalSpacing(dp8);
                    grid.setAdapter(adapter);
                } else{
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                finish();
                break;
            case R.id.layout_tag:
                Intent intent = new Intent(context, UploadTagActivity.class);
                startActivity(intent);
                break;
            case R.id.layout_cb:
                ChooseBookDialog chooseDialog = new ChooseBookDialog(context, mDialogHandler);
                chooseDialog.setOnDismissListener(this);
                chooseDialog.show();
                break;
            case R.id.btn_save:
                if (TaskUtil.isNetworkAvailable(context)) {
                    if (bookId != null && !bookId.equals("") && !selectedImage.get(0).equals("")) {
                        post();
                    } else{
                        Toast.makeText(context, "필드 누락", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void post() {
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        clutch.post("", bookId, 2, "", getIntent().getStringExtra("url"), selectedImage.get(0), edtContent.getText().toString(), TaskUtil.getUserToken(context), new Callback<ClutchUtil.Clutch.ClutchResult>() {
            @Override
            public void success(ClutchUtil.Clutch.ClutchResult clutchResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(clutchResult.status.code);
                Log.e("result", ClutchUtil.Status.check(clutchResult.status.code));
                if (errorMsg.equals("")) {
                    Toast.makeText(context, TaskUtil.SuccessMessage, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {

    }


    public class CollectImageAdapter implements StickyGridHeadersBaseAdapter {


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v;
            isSelected = new boolean[imageArray.size()];
            v = getLayoutInflater().inflate(R.layout.item_list_choose_image, null);

            final FrameLayout frameLayout = (FrameLayout)v.findViewById(R.id.frame);
            final LinearLayout loadLayout = (LinearLayout)v.findViewById(R.id.load);
            loadLayout.setVisibility(View.GONE); //TODO:: TEMPORARY SETTING
            final ImageView imageView = (ImageView)v.findViewById(R.id.image);
            final LinearLayout checkLayout = (LinearLayout)v.findViewById(R.id.select);
            final Button btnSelect = (Button)v.findViewById(R.id.btn_select);
            int cellSize = UnitSizeUtil.dp2px(context, 108);
            frameLayout.setLayoutParams(new LinearLayout.LayoutParams(cellSize,cellSize));
            final String imageUrl = imageArray.get(position);
            ImageLoaderUtil.imageLoader.displayImage(imageUrl, imageView, ImageLoaderUtil.imageLoaderOptions());

            btnSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (selectedImage.contains(imageUrl)) {
                        checkLayout.setVisibility(View.GONE);
                        int savedIndex = selectedImage.indexOf(imageUrl); //삭제할 아이템의 내용과 비교해서 인덱스찾기
                        selectedImage.remove(savedIndex);   //그리고 어레이에서 떨궈내기
                    } else {
                        if(selectedImage.size() < 1){  //1장이면 1, 5장이면 5
                            checkLayout.setVisibility(View.VISIBLE);
                            selectedImage.add(imageUrl);
                        }else{
                            Toast.makeText(context, "한 장의 사진만 선택하세요..", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            if(selectedImage.contains(imageUrl)){
                checkLayout.setVisibility(View.VISIBLE);
            }else{
                checkLayout.setVisibility(View.GONE);
            }

            return v;
        }

        @Override
        public int getCount() { return 0; }

        @Override public Object getItem(int position) {return null;}

        @Override public long getItemId(int position) {return position;}

        @Override public int getItemViewType(int position) {return 0;}

        @Override public int getViewTypeCount() {return 2;}

        @Override public boolean areAllItemsEnabled() {return false;}

        @Override public boolean isEnabled(int position) { return false; }

        @Override public void registerDataSetObserver(DataSetObserver observer) {}

        @Override public void unregisterDataSetObserver(DataSetObserver observer) {}

        @Override public boolean hasStableIds() { return false; }

        @Override public boolean isEmpty() { return false; }


        @Override
        public int getCountForHeader(int header) {
            int size = 0;
            if(header == 0){
                size = imageArray.size();
            }else if(header == 1){
                size = 0;
            }
            return size;
        }

        @Override
        public int getNumHeaders() {
            return 2;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {

            View info;
            if(position == 1){
                info = getLayoutInflater().inflate(R.layout.item_upload_link_footer, null);
                LinearLayout btnTag = (LinearLayout)info.findViewById(R.id.layout_tag);
                LinearLayout btnCb = (LinearLayout)info.findViewById(R.id.layout_cb);
                edtContent = (EditText)info.findViewById(R.id.edt_contents);
                Button btnSave = (Button)info.findViewById(R.id.btn_save);
                nameText = (TextView)info.findViewById(R.id.text_cb);

                btnSave.setOnClickListener(LinkUploadActivity.this);
                btnTag.setOnClickListener(LinkUploadActivity.this);
                btnCb.setOnClickListener(LinkUploadActivity.this);

            }else{
                info = getLayoutInflater().inflate(R.layout.item_empty_layout, null);

            }
            return info;
        }
    }
}