package another.planet.clutch.upload;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.UnitSizeUtil;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;

import java.util.ArrayList;

/**
 * Created by Marutian on 2014. 4. 5..
 */
public class UploadTagActivity extends Activity implements View.OnClickListener {

    Context context;
    ArrayList<Construct.RecentItems> recentArray;
    ArrayList<Construct.TagItems> itemArray;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_upload_picture_tag);

        context = this;

        LinearLayout btnBack = (LinearLayout) findViewById(R.id.btn_back);
        LinearLayout btnSave = (LinearLayout) findViewById(R.id.btn_save);
        Button btnDone = (Button) findViewById(R.id.btn_done);

        btnBack.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        //DUMMY DATA
        String[] recentid = {"1", "2", "3", "4", "5"};
        String[] recentname = {"가", "나", "다", "라", "마"};
        String[] id = {"11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22"};
        String[] name = {"거", "너", "더", "러", "머", "버", "서", "어", "저", "처", "터", "퍼"};
        BitmapDrawable thumb = (BitmapDrawable) getResources().getDrawable(R.drawable.temp_image);

        recentArray = new ArrayList<Construct.RecentItems>();
        itemArray = new ArrayList<Construct.TagItems>();

        for (int i = 0; i < recentid.length; i++) {
            recentArray.add(new Construct.RecentItems(recentid[i], recentname[i], thumb));
        }

        for (int i = 0; i < id.length; i++) {
            //itemArray.add(new Construct.TagItems(id[i], name[i], thumb));
        }

        GridAdapter adapter = new GridAdapter();
        StickyGridHeadersGridView grid = (StickyGridHeadersGridView) findViewById(R.id.tag_grid);

        int dp8 = (int) UnitSizeUtil.dp2px(context, 8);
        grid.setNumColumns(3);
        grid.setVerticalSpacing(dp8);
        grid.setHorizontalSpacing(dp8);
        grid.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_save:
                Toast.makeText(context, "저장", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public class GridAdapter implements StickyGridHeadersBaseAdapter {

        @Override
        public int getCountForHeader(int header) {
            int size = 0;
            switch (header) {
                case 0:
                    size = recentArray.size();
                    break;
                case 1:
                    size = itemArray.size();
                    break;
                default:
                    size = 0;
                    break;
            }
            return size;
        }

        @Override
        public int getNumHeaders() {
            return 2;
        }

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {

            convertView = getLayoutInflater().inflate(R.layout.item_upload_separator, null);
            TextView headerText = (TextView) convertView.findViewById(R.id.text_header);
            switch (position) {
                case 0:
                    headerText.setText("즐겨찾는 연예인");
                    break;
                case 1:
                    headerText.setText("전체");
                    break;
            }
            return convertView;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = getLayoutInflater().inflate(R.layout.item_list_choose_name, null);

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int cellSize = UnitSizeUtil.dp2px(context, 108);
            int layoutType = getItemViewType(position);
            final FrameLayout frameLayout = (FrameLayout) v.findViewById(R.id.frame);
            final LinearLayout loadLayout = (LinearLayout) v.findViewById(R.id.load);
            final ImageView imageView = (ImageView) v.findViewById(R.id.image);
            final TextView nameText = (TextView) v.findViewById(R.id.name);
            final LinearLayout checkLayout = (LinearLayout) v.findViewById(R.id.select);
            final Button btnSelect = (Button) v.findViewById(R.id.btn_select);
            frameLayout.setLayoutParams(new LinearLayout.LayoutParams(cellSize, cellSize));
            loadLayout.setVisibility(View.GONE);
            checkLayout.setVisibility(View.GONE);

            if (position < recentArray.size()) {
                //즐겨찾는 연예인
                String id = recentArray.get(position).bookId;
                nameText.setText(recentArray.get(position).bookName);
                imageView.setImageDrawable(recentArray.get(position).bookCover);
                btnSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            } else {
                //전체 목록
                int index = position - recentArray.size();
                String id = itemArray.get(index).bookId;
                nameText.setText(itemArray.get(index).bookName);
                //imageView.setImageDrawable(itemArray.get(index).bookCover);
                btnSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }

            return v;
        }

        @Override
        public int getItemViewType(int position) {

            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }

}