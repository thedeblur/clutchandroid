package another.planet.clutch.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import another.planet.clutch.R;

public class MakeBookDialog extends Dialog {

    Context mContext;
    String bookName;

    public MakeBookDialog(final Context context) {
        super(context, R.style.Clutch_Dialog);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.7f;
        getWindow().setAttributes(lpWindow);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        mContext = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_make_book);

        final EditText edtName = (EditText) findViewById(R.id.edt_cb_name);

        (findViewById(R.id.btn_create)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtName.getText().toString().equals("")){
                    bookName = edtName.getText().toString();
                    Toast.makeText(mContext, "클러치북 이름 : " + bookName , Toast.LENGTH_SHORT).show();
                    BookDiscriptionDialog discriptionDialog = new BookDiscriptionDialog(mContext, bookName);
                    discriptionDialog.show();
                    dismiss();
                }

            }
        });

        (findViewById(R.id.btn_dialog_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


    }
}