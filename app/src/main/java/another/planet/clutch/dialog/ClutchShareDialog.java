package another.planet.clutch.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import another.planet.clutch.R;

/**
 * Created by Marutian on 14. 1. 28.
 */
public class ClutchShareDialog extends Dialog implements View.OnClickListener {

    Context mContext;

    public ClutchShareDialog(Context context) {
        super(context, R.style.Clutch_UpStyleDialog);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.7f;
        getWindow().setAttributes(lpWindow);

        mContext = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    public void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.dialog_clutch_share);

        findViewById(R.id.btn_share_kakao).setOnClickListener(this);
        findViewById(R.id.btn_share_email).setOnClickListener(this);
        findViewById(R.id.btn_share_link).setOnClickListener(this);
        findViewById(R.id.btn_share_img_save).setOnClickListener(this);
        findViewById(R.id.btn_share_close).setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_share_kakao :
                Toast.makeText(mContext, "Share via KakaoTalk", 0).show();
                dismiss();
                break;
            case R.id.btn_share_email :
                Toast.makeText(mContext, "Share via Email", 0).show();
                dismiss();
                break;
            case R.id.btn_share_link :
                Toast.makeText(mContext, "Copy link to clipboard", 0).show();
                dismiss();
                break;
            case R.id.btn_share_img_save :
                Toast.makeText(mContext, "Save image to storage", 0).show();
                dismiss();
                break;
            case R.id.btn_share_close :
                dismiss();
                break;
        }
    }
}