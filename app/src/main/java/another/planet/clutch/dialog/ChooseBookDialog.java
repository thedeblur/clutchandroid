package another.planet.clutch.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.util.*;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

public class ChooseBookDialog extends Dialog {

    Context context;
    private OnDismissListener listener;
    ArrayList<Construct.RecentItems> recentArray;
    StickyGridHeadersGridView grid;
    int cellSize;
    Handler mDialogHandler;
    static Handler refreshHandler;

    public ChooseBookDialog(Context context, Handler handler) {
        super(context, R.style.Clutch_Dialog);

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        cellSize = UnitSizeUtil.px2dp(context, 310);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;

        lpWindow.dimAmount = 0.7f;
        getWindow().setAttributes(lpWindow);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        this.context = context;
        this.mDialogHandler = handler;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choose_book);

        recentArray = new ArrayList<Construct.RecentItems>();


        if (TaskUtil.isNetworkAvailable(context)) {
            grid = (StickyGridHeadersGridView) findViewById(R.id.clutchbook_grid);

            int dp8 = (int) UnitSizeUtil.dp2px(context, 8);
            grid.setNumColumns(3);
            grid.setVerticalSpacing(dp8);
            grid.setHorizontalSpacing(dp8);

            getCB();
        } else {
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
            dismiss();
        }

        refreshHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(msg.getData().getBoolean("refresh", false)){
                    getCB();
                }
            }
        };
    }

    private void getCB() {
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        user.getUserCB(TaskUtil.getUserId(context), TaskUtil.getUserToken(context), new Callback<ClutchUtil.User.ClutchBookResult>() {
            @Override
            public void success(ClutchUtil.User.ClutchBookResult clutchBookResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(clutchBookResult.status.code);
                if (errorMsg.equals("")) {
                    ArrayList<Construct.TagItems> itemArray = new ArrayList<Construct.TagItems>();
                    for (ClutchUtil.ClutchBookArray cArray : clutchBookResult.cbs) {
                        if (clutchBookResult.cbs.size() == 0) {
                            break;
                        } else {
                            String last_photo;
                            try {
                                last_photo = cArray.last_photos.get(0);
                            } catch (IndexOutOfBoundsException e) {
                                last_photo = "";
                            }
                            itemArray.add(new Construct.TagItems(
                                    cArray.cb_id,
                                    cArray.name,
                                    last_photo));
                        }
                    }
                    GridAdapter adapter = new GridAdapter(0, itemArray);
                    grid.setAdapter(adapter);

                } else {

                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    public class GridAdapter extends ArrayList<Object> implements StickyGridHeadersBaseAdapter {
        public ArrayList<Construct.TagItems> itemArray;

        public GridAdapter(int textViewResourceId,
                           ArrayList<Construct.TagItems> array) {
            super(textViewResourceId);

            this.itemArray = array;
        }

        @Override
        public int getCountForHeader(int header) {
            int size = 0;
            switch (header) {
                case 0:
                    size = recentArray.size();
                    break;
                case 1:
                    size = itemArray.size() + 1;
                    break;
                default:
                    size = 0;
                    break;
            }
            return size;
        }

        @Override
        public int getNumHeaders() {
            return 2;
        }

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {

            convertView = getLayoutInflater().inflate(R.layout.item_upload_separator, null);
            TextView headerText = (TextView) convertView.findViewById(R.id.text_header);
            switch (position) {
                case 0:
                    headerText.setText("최근 사용한 클러치북");
                    break;
                case 1:
                    headerText.setText("전체");
                    break;
            }
            return convertView;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(final int index, View convertView, ViewGroup parent) {

            View v;

            v = getLayoutInflater().inflate(R.layout.item_list_choose_name, null);

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();

            final FrameLayout frameLayout = (FrameLayout) v.findViewById(R.id.frame);
            final LinearLayout loadLayout = (LinearLayout) v.findViewById(R.id.load);
            final ImageView imageView = (ImageView) v.findViewById(R.id.image);
            final TextView nameText = (TextView) v.findViewById(R.id.name);
            final LinearLayout checkLayout = (LinearLayout) v.findViewById(R.id.select);
            final Button btnSelect = (Button) v.findViewById(R.id.btn_select);
            cellSize = UnitSizeUtil.dp2px(context, 100);
            frameLayout.setLayoutParams(new LinearLayout.LayoutParams(cellSize, cellSize));

            loadLayout.setVisibility(View.GONE);
            checkLayout.setVisibility(View.GONE);

            if (index < recentArray.size()) {
                //최근 사용한 클러치북
                nameText.setText(itemArray.get(index).bookName);
                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(itemArray.get(index).bookCover, 200), imageView, ImageLoaderUtil.imageLoaderOptions());
                //imageView.setImageDrawable(itemArray.get(index).bookCover);
                btnSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Message msg = new Message();
                        Bundle data = new Bundle();
                        data.putString("cb_id", itemArray.get(index).bookId);
                        data.putString("cb_name", itemArray.get(index).bookName);
                        msg.setData(data);
                        mDialogHandler.sendMessage(msg);
                        dismiss();
                    }
                });
            } else {
                int pos = index - recentArray.size();
                if (pos == 0) {
                    //새로운 클러치북 만들기
                    v = getLayoutInflater().inflate(R.layout.item_list_choose_add, null);
                    v.findViewById(R.id.name).setVisibility(View.VISIBLE);
                    v.findViewById(R.id.frame).setLayoutParams(new LinearLayout.LayoutParams(cellSize, cellSize));
                    v.findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MakeBookDialog makeDialog = new MakeBookDialog(context);
                            makeDialog.setOnDismissListener(new OnDismissListener() {
                                public void onDismiss(DialogInterface dialog) {
                                }
                            });
                            makeDialog.show();
                        }
                    });
                } else {
                    //전체 목록들
                    final int position = pos - 1;
                    nameText.setText(itemArray.get(position).bookName);
                    ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(itemArray.get(position).bookCover, 200), imageView, ImageLoaderUtil.imageLoaderOptions());
                    btnSelect.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Message msg = new Message();
                            Bundle data = new Bundle();
                            data.putString("cb_id", itemArray.get(position).bookId);
                            data.putString("cb_name", itemArray.get(position).bookName);
                            msg.setData(data);
                            mDialogHandler.sendMessage(msg);
                            dismiss();
                        }
                    });
                }
            }
            return v;
        }

        @Override
        public int getItemViewType(int position) {

            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }
}