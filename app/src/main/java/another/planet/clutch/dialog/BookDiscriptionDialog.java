package another.planet.clutch.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.TaskUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class BookDiscriptionDialog extends Dialog {

    Context mContext;
    String bookName;

    public BookDiscriptionDialog(Context context, String name) {
        super(context, R.style.Clutch_Dialog);


        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.7f;
        getWindow().setAttributes(lpWindow);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        mContext = context;
        this.bookName = name;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_book_discription);

        final EditText edtDiscription = (EditText) findViewById(R.id.edt_discription);

        (findViewById(R.id.btn_done)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtDiscription.getText().toString().equals("")){
                    final String description = edtDiscription.getText().toString();

                    ClutchUtil.ClutchBook cb = ClutchUtil.getClutchBookInstance();
                    cb.makeCB(TaskUtil.getUserId(mContext), TaskUtil.getUserToken(mContext), bookName, description, new Callback<ClutchUtil.Result>() {
                        @Override
                        public void success(ClutchUtil.Result result, Response response) {
                            String errorMsg = ClutchUtil.Status.check(result.status.code);
                            if(errorMsg.equals("")){
                                Toast.makeText(mContext, TaskUtil.SuccessMessage, Toast.LENGTH_SHORT).show();
                                Message msg = new Message();
                                Bundle data = new Bundle();
                                data.putBoolean("refresh", true);
                                msg.setData(data);
                                ChooseBookDialog.refreshHandler.sendMessage(msg);
                                dismiss();
                            } else{
                                Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {

                        }
                    });

                }

            }
        });

        (findViewById(R.id.btn_dialog_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


    }
}