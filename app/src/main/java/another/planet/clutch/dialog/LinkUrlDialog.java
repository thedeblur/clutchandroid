package another.planet.clutch.dialog;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.upload.LinkUploadActivity;

public class LinkUrlDialog extends Dialog {

    Context mContext;

    public LinkUrlDialog(Context context) {
        super(context, R.style.Clutch_Dialog);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.7f;
        getWindow().setAttributes(lpWindow);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        mContext = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_upload_linkurl);

        final EditText edtUrl = (EditText) findViewById(R.id.edt_url);

        findViewById(R.id.dialog_copy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager cm = (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);

                if(cm.hasPrimaryClip()){
                    ClipData.Item item = cm.getPrimaryClip().getItemAt(0);
                    String pastedStr = item.getText().toString();
                    edtUrl.setText(pastedStr);

                }else{
                    Toast.makeText(mContext, "클립보드가 비어있습니다.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        findViewById(R.id.dialog_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = edtUrl.getText().toString();
                if(!url.contains("http://")){
                    url = "http://" + url;
                }

                Intent intent = new Intent(mContext, LinkUploadActivity.class);
                intent.putExtra("url" ,url);
                mContext.startActivity(intent);
                dismiss();

            }
        });


    }
}