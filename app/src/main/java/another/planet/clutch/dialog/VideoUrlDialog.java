package another.planet.clutch.dialog;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.upload.VideoUploadActivity;

public class VideoUrlDialog extends Dialog {

    Context mContext;

    public VideoUrlDialog(Context context) {
        super(context, R.style.Clutch_Dialog);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.7f;
        getWindow().setAttributes(lpWindow);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        mContext = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_upload_videourl);

        final EditText edtUrl = (EditText) findViewById(R.id.edt_url);

        findViewById(R.id.dialog_copy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager cm = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                if (cm.hasPrimaryClip()) {
                    ClipData.Item item = cm.getPrimaryClip().getItemAt(0);
                    String pastedStr = item.getText().toString();
                    edtUrl.setText(pastedStr);

                } else {
                    Toast.makeText(mContext, "클립보드가 비어있습니다.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.dialog_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Message msg = new Message();
                Bundle data = new Bundle();
                data.putString("videoUrl", edtUrl.getText().toString());
                msg.setData(data);
                VideoUploadActivity.videoHandler.sendMessage(msg);
                dismiss();

            }
        });


    }
}