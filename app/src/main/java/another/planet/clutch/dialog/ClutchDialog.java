package another.planet.clutch.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.*;
import another.planet.clutch.R;

import java.util.ArrayList;
import java.util.TreeSet;

public class ClutchDialog extends Dialog implements View.OnClickListener{

    Context mContext;
    String strClutchText, strBookName;

    class BookList {public BookList(String cbId, String bookName, String thumbUrl, boolean isPrivate) {
            this.cbId = cbId;
            this.bookName = bookName;
            this.thumbUrl = thumbUrl;
            this.isPrivate = isPrivate;
        }

        String cbId, bookName, thumbUrl;
        boolean isPrivate;
    }

    public ClutchDialog(Context context) {
        super(context, R.style.Clutch_Dialog);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.7f;
        getWindow().setAttributes(lpWindow);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        mContext = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);

    }

    public void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.dialog_clutch_crop);

        //dummy
        String data1[] = {"1", "2", "3", "4", "5", "6", "7"};
        String data2[] = {"11", "22", "33", "44", "55", "66", "77", "88", "99", "100", "110", "120"};
        boolean data3[] = {false, false, false, true, false, false, false}; //4
        boolean data4[] = {true,  false, false, true, false, false, true, false, false, true, false, false}; //11 44 77 100

        ArrayList<BookList> recentArray = new ArrayList<BookList>();
        ArrayList<BookList> allArray = new ArrayList<BookList>();

        findViewById(R.id.btn_dialog_add_clutch).setOnClickListener(this);
        findViewById(R.id.btn_dialog_close).setOnClickListener(this);
        findViewById(R.id.btn_edit).setOnClickListener(this);

        ImageView shareFb = (ImageView)findViewById(R.id.dialog_add_cb_fb);
        ImageView shareTw = (ImageView)findViewById(R.id.dialog_add_cb_twitter);

        final BookListAdapter adapter = new BookListAdapter();

        for(int i=0; i<data1.length; i++) {
            if(i==0){
                adapter.addSeparatorItem("최근에 수집한 클러치북");
            }
            recentArray.add(new BookList("", data1[i], "", data3[i]));
            adapter.addItem(recentArray.get(i));
        }

        for(int i=0; i<data2.length; i++) {
            if(i==0){
                adapter.addSeparatorItem("모든 클러치북");
            }

            allArray.add(new BookList("", data2[i], "", data4[i]));
            adapter.addItem(allArray.get(i));
        }

        ListView listView = (ListView)findViewById(R.id.list_dialog_add_cb);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(mContext, adapter.mData.get(position).bookName, Toast.LENGTH_LONG).show();
            }
        });

    }
    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.btn_dialog_add_clutch:
                break;
            case R.id.btn_dialog_close:
                dismiss();
                break;
            case R.id.btn_edit:
                break;

        }
    }

    private class BookListAdapter extends BaseAdapter {
        private static final int TYPE_ITEM = 0;
        private static final int TYPE_SEPARATOR = 1;

        private ArrayList<BookList> mData = new ArrayList<BookList>();
        private TreeSet mSeparatorsSet = new TreeSet();

        public void addItem(BookList item){
            mData.add(item);
            notifyDataSetChanged();
        }

        public void addSeparatorItem(String item){
            mData.add(new BookList("", item, "", false));
            mSeparatorsSet.add(mData.size() - 1);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int position) {
            return mData.get(position);
        }

        @Override
        public int getItemViewType(int position) {
            return mSeparatorsSet.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
        }

        @Override
        public int getViewTypeCount() {
            return TYPE_SEPARATOR+1;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            Log.e("asdf", mData.get(position).bookName);
            int type = getItemViewType(position);
            System.out.println("getView " + position + " " + convertView + " type = " + type);
            if (convertView == null) {
                holder = new ViewHolder();
                switch (type) {
                    case TYPE_ITEM:
                        convertView = getLayoutInflater().inflate(R.layout.item_dialog_cb_row, null);
                        holder.thumbImageView = (ImageView)convertView.findViewById(R.id.img_cb_list_thumb);
                        holder.textView = (TextView)convertView.findViewById(R.id.text_cb_list);

                        if(mData.get(position).isPrivate)
                            convertView.findViewById(R.id.icon_cb_private).setVisibility(View.VISIBLE); //Default (value == false) -> View.GONE

                        break;
                    case TYPE_SEPARATOR:

                        if(mData.get(position).bookName.equals("최근에 수집한 클러치북")){
                            convertView = getLayoutInflater().inflate(R.layout.item_separator_recent_cb, null);

                            holder.textView = (TextView) convertView.findViewById(R.id.text_separator_title);
                            holder.thumbImageView = (ImageView) convertView.findViewById(R.id.img_cb_list_thumb);
                        }else if(mData.get(position).bookName.equals("모든 클러치북")){
                            convertView = getLayoutInflater().inflate(R.layout.item_separator_all_cb, null);

                            holder.textView = (TextView) convertView.findViewById(R.id.text_separator_title);
                            holder.thumbImageView = (ImageView) convertView.findViewById(R.id.img_cb_list_thumb);
                        }


                        break;
                }
                if(convertView != null){
                    convertView.setTag(holder);
                }
            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.thumbImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.temp_image));
            holder.textView.setText(mData.get(position).bookName);

            return convertView;
        }

    }

    public static class ViewHolder {
        public TextView textView;
        public ImageView thumbImageView;
    }

}