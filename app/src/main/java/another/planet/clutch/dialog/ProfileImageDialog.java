package another.planet.clutch.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import another.planet.clutch.R;
import another.planet.clutch.fragment.main.ProfileFragment;

public class ProfileImageDialog extends Dialog implements View.OnClickListener {

    LinearLayout takePicture, cropPicture, fromFacebook;

    public ProfileImageDialog(Context context) {
        super(context, R.style.Clutch_Dialog);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.7f;
        getWindow().setAttributes(lpWindow);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        setContentView(R.layout.dialog_profile_image);

        takePicture = (LinearLayout) findViewById(R.id.dialog_pimage_item1);
        cropPicture = (LinearLayout) findViewById(R.id.dialog_pimage_item2);
        fromFacebook = (LinearLayout) findViewById(R.id.dialog_pimage_item3);

        takePicture.setOnClickListener(this);
        cropPicture.setOnClickListener(this);
        fromFacebook.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Message msg = new Message();
        msg.what = ProfileFragment.DIALOG_PROFILE_IMAGE;
        if (view == takePicture) {
            msg.obj = ProfileFragment.PROFILE_TAKE_PICTURE;
        } else if (view == cropPicture) {
            msg.obj = ProfileFragment.PROFILE_CROP_IMAGE;
        } else if (view == fromFacebook) {
            msg.obj = ProfileFragment.PROFILE_FROM_FACEBOOK;
        }
        ProfileFragment.mDialogHandler.sendMessage(msg);
        dismiss();
    }


}
