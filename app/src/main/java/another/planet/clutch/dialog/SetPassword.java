package another.planet.clutch.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import another.planet.clutch.R;

/**
 * Created by Marutian on 14. 1. 28.
 */
public class SetPassword extends Dialog {

    Context mContext;

    public SetPassword(Context context) {
        super(context, R.style.Clutch_Dialog);


        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.7f;
        getWindow().setAttributes(lpWindow);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        mContext = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_setting_password);

        final EditText passwd = (EditText) findViewById(R.id.edt_newpasswd);
        final EditText cpasswd = (EditText) findViewById(R.id.edt_newpasswd_confirm);

        ((LinearLayout) findViewById(R.id.dialog_done)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String pw = passwd.getText().toString();
                String pwConfirm = cpasswd.getText().toString();

                if (pwConfirm.equals(pw)) {
                    Toast.makeText(mContext, pw, Toast.LENGTH_SHORT).show();
                    dismiss();
                } else {
                    Toast.makeText(mContext, "비밀번호 확인이 맞지 않습니다.", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }
}