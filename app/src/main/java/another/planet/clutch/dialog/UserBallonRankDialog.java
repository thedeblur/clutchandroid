package another.planet.clutch.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.adapter.UserRankAdapter;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.UnitSizeUtil;
import another.planet.clutch.widget.NumbericImageView;
import another.planet.clutch.widget.RoundedImageView;

import java.util.ArrayList;

/**
 * Created by Marutian on 14. 1. 28.
 */
public class UserBallonRankDialog extends Dialog {

    Context context;
    String artistId;
    String dummy = "9ddebdb011d7e77a3d1c17a9b1cf160c52d4";

    public UserBallonRankDialog(Context context, String artistId) {
        super(context, R.style.Clutch_Dialog);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.7f;
        getWindow().setAttributes(lpWindow);

        this.context = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    public void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.dialog_rank_user);

        String d1[] = {"가", "나", "다", "라"};
        int[] a = new int[]{324, 231, 13, 1};
        String d2[] = {dummy, dummy, dummy, dummy};

        final ArrayList<Construct.UserBallonRank> array = new ArrayList<Construct.UserBallonRank>();
        for(int i=0; i<d1.length; i++){
            array.add(new Construct.UserBallonRank(d1[i], d1[i], d2[i], d1[i], a[i]));
        }

        UserRankAdapter adapter = new UserRankAdapter(context, 0, array);
        ListView lvRank = (ListView)findViewById(R.id.list_dialog_user_rank);
        lvRank.setAdapter(adapter);

        findViewById(R.id.btn_dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        lvRank.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                ImageLoaderUtil.imageLoader.displayImage(
                        ImageLoaderUtil.getImageURL(array.get(position).profileURL, 300),
                        ((RoundedImageView)findViewById(R.id.img_user_profile_select)),
                        ImageLoaderUtil.imageLoaderOptions());

                findViewById(R.id.layout_rank_user_select).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.text_user_name_select)).setText(array.get(position).name);
                ((TextView)findViewById(R.id.text_user_ballon_count_select)).setText(UnitSizeUtil.numberDecimalFormat(array.get(position).star));

                findViewById(R.id.btn_go_profile).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, array.get(position).userId, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


    }
}