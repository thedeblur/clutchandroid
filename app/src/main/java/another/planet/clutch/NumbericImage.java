package another.planet.clutch;

import android.app.Activity;
import android.os.Bundle;
import another.planet.clutch.widget.NumbericImageView;


public class NumbericImage extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numberic_image);

        NumbericImageView niv = (NumbericImageView)findViewById(R.id.num1111);
        niv.setNumberText("5151.");
        niv.setNumberTextStyle(NumbericImageView.RANK_NUMBER_TOP);

    }
}
