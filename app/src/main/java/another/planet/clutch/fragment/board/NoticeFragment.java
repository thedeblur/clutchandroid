package another.planet.clutch.fragment.board;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import another.planet.clutch.R;

import java.util.ArrayList;

public class NoticeFragment extends Fragment {

    int layoutid;
    Context context;
    ArrayList<String> contentNameArray, contentDateArray;

    public NoticeFragment(Context context) {
        super();
        this.context = context;
        this.layoutid = R.layout.board_notice;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.closed_page_unfinished, null);

        /**String contentName[] = {"클러치 1월 공지 - 걸스데이 소진 혜리 스타킹 녹화 -", "클러치 12월 공지 - 걸스데이 소진 혜리 스타킹 녹화 -", "클러치 11월 공지 - 걸스데이 소진 혜리 스타킹 녹화 -"};
         String contentDate[] = {"2014.01.11", "2013.12.23", "2013.11.14"};

         contentNameArray = new ArrayList<String>();
         contentDateArray = new ArrayList<String>();

         for (int i = 0; i < contentName.length; i++) {
         contentNameArray.add(contentName[i]);
         contentDateArray.add(contentDate[i]);
         }

         ((ListView) v.findViewById(R.id.notice_list)).setAdapter(new NoticeListAdapter(context, R.layout.item_board_notice));
         */
        return v;
    }

    public class NoticeListAdapter extends ArrayAdapter<String> {

        public NoticeListAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_board_notice, null);

            ((TextView) convertView.findViewById(R.id.board_name)).setText(contentNameArray.get(position));
            ((TextView) convertView.findViewById(R.id.board_date)).setText(contentDateArray.get(position));

            ((LinearLayout) convertView.findViewById(R.id.notice_item_layout)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, contentNameArray.get(position), Toast.LENGTH_SHORT).show();
                }
            });

            return convertView;
        }

        @Override
        public int getCount() {
            return contentNameArray.size();
        }
    }

}