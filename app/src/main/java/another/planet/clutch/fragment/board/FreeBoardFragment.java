package another.planet.clutch.fragment.board;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.activity.BoardDetailActivity;

import java.util.ArrayList;

public class FreeBoardFragment extends Fragment {

    int layoutid;
    Context context;
    ArrayList<PostItems> postItemsArray;

    public FreeBoardFragment(Context context) {
        super();
        this.context = context;
        this.layoutid = R.layout.board_freeboard;
    }

    class PostItems {
        PostItems(
                String postId, String postType, String postName, String postUploader, String postDate, String postMent,
                String likeCount, String commentCount, BitmapDrawable postImage, BitmapDrawable profileImage) {
            this.postId = postId;
            this.postType = postType;
            this.postName = postName;
            this.postUploader = postUploader;
            this.postDate = postDate;
            this.postMent = postMent;
            this.likeCount = likeCount;
            this.commentCount = commentCount;
            this.postImage = postImage;
            this.profileImage = profileImage;
        }

        String postId, postType, postName, postUploader, postDate, postMent, likeCount, commentCount;
        BitmapDrawable postImage, profileImage;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.closed_page_unfinished, null);

        /**String postId[] = {"0023413", "002412", "002411"};
         String postType[] = {"[아이유]", "[걸스데이]", "[손나은]"};
         String postName[] = {"아이유 언니 봤어!", "걸스데이 봤어!", "손나은 언니 봤어!"};
         String postUploader[] = {"Sean Jung", "augite", "ABCDE"};
         String postDate[] = {"2014. 1. 31 오후 1:00", "2014. 2. 1 오전 10:11", "2014. 2. 4 오후 8:12"};
         String postMent[] = {
         "진짜 머리도 작고 몸도 작고 너무너무 이쁘요 저런사람이 되어야하는데 나는 진짜 머리도 작고 몸도 작고",
         "진짜 머리도 작고, 몸도 작고 너무너무 이쁘요 저런사람이 되어야하는데 나는 진짜 머리도 작고 몸도 작고",
         "5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트5줄테스트"};
         String likeCount[] = {"22341", "5523", "1223"};
         String commentCount[] = {"66248", "9953", "18908"};
         BitmapDrawable postImage[] = {(BitmapDrawable) getResources().getDrawable(R.drawable.temp_image), (BitmapDrawable) getResources().getDrawable(R.drawable.temp_image2), (BitmapDrawable) getResources().getDrawable(R.drawable.temp_image3)};
         BitmapDrawable profileImage[] = {(BitmapDrawable) getResources().getDrawable(R.drawable.temp_image), (BitmapDrawable) getResources().getDrawable(R.drawable.temp_image2), (BitmapDrawable) getResources().getDrawable(R.drawable.temp_image3)};

         postItemsArray = new ArrayList<PostItems>();

         for (int i = 0; i < postId.length; i++) {
         postItemsArray.add(new PostItems(
         postId[i], postType[i], postName[i], postUploader[i], postDate[i],
         postMent[i], likeCount[i], commentCount[i], postImage[i], profileImage[i]));
         }

         ((ListView) v.findViewById(R.id.freeboard_list)).setAdapter(new BoardPostAdapter(context, R.layout.item_freeboard));
         (v.findViewById(R.id.btn_add)).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        startActivity(new Intent(context, BoardWriteActivity.class));
        }
        });*/
        return v;
    }

    public class BoardPostAdapter extends ArrayAdapter<String> {

        public BoardPostAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_freeboard, null);

            final TextView mentTextView = (TextView) convertView.findViewById(R.id.free_post_ment);
            final TextView continueRead = (TextView) convertView.findViewById(R.id.free_continue_read);

            final PostItems items = postItemsArray.get(position);
            ((TextView) convertView.findViewById(R.id.free_post_type)).setText(items.postType);
            ((TextView) convertView.findViewById(R.id.free_post_name)).setText(items.postName);
            ((TextView) convertView.findViewById(R.id.free_uploader)).setText(items.postUploader);
            ((TextView) convertView.findViewById(R.id.free_post_date)).setText(items.postDate);
            mentTextView.setText(items.postMent);
            ((TextView) convertView.findViewById(R.id.free_post_like_count)).setText(items.likeCount);
            ((TextView) convertView.findViewById(R.id.free_post_comment_count)).setText(items.commentCount);
            ((ImageView) convertView.findViewById(R.id.post_image)).setImageDrawable((Drawable) items.postImage);
            ((ImageView) convertView.findViewById(R.id.profile_uploader_image)).setImageDrawable((Drawable) items.profileImage);

            mentTextView.post(new Runnable() {                 //내용을 표시하는 TextView가 5줄이 넘어가면 계속읽기 보이기
                @Override
                public void run() {
                    if (mentTextView.getLineCount() > 4) {
                        continueRead.setVisibility(View.VISIBLE);
                        continueRead.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(context, "ID : [" + postItemsArray.get(position).postId + "] 번 포스트 계속 읽기", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, BoardDetailActivity.class);
                                intent.putExtra("post_id", postItemsArray.get(position).postId);
                                intent.putExtra("post_type", postItemsArray.get(position).postType);
                                intent.putExtra("post_name", postItemsArray.get(position).postName);
                                intent.putExtra("post_uploader", postItemsArray.get(position).postUploader);
                                intent.putExtra("post_date", postItemsArray.get(position).postDate);
                                intent.putExtra("post_ment", postItemsArray.get(position).postMent);
                                intent.putExtra("like_count", postItemsArray.get(position).likeCount);
                                intent.putExtra("comment_count", postItemsArray.get(position).commentCount);
                                startActivity(intent);
                            }
                        });
                    }
                }
            });

            ((LinearLayout) convertView.findViewById(R.id.free_share_layout)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "공유", Toast.LENGTH_SHORT).show();
                }
            });

            ((LinearLayout) convertView.findViewById(R.id.free_like_layout)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "좋아요", Toast.LENGTH_SHORT).show();
                }
            });

            ((LinearLayout) convertView.findViewById(R.id.free_comment_layout)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "댓글", Toast.LENGTH_SHORT).show();
                }
            });

            return convertView;
        }

        @Override
        public int getCount() {
            return postItemsArray.size();
        }
    }

}