package another.planet.clutch.fragment.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.activity.FavoriteSettingActivity;
import another.planet.clutch.activity.MainFragmentActivity;
import another.planet.clutch.adapter.FavoriteClutchAdapter;
import another.planet.clutch.adapter.FavoriteSpinnerAdapter;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshStaggeredGridView;
import com.origamilabs.library.views.StaggeredGridView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

public class FavoriteFragment extends Fragment {

    private static final int STATE_ONSCREEN = 0;
    private static final int STATE_OFFSCREEN = 1;
    private static final int STATE_RETURNING = 2;
    private int mState = STATE_ONSCREEN;

    private Context context;
    private int layoutid;
    private int pageNo;

    public static Handler spinnerSwitchHandler;
    public static View spinnerDropdownView;

    private View v, spinner;
    private GridView spinnerList;
    private PullToRefreshStaggeredGridView prGridView;
    private StaggeredGridView gridView;
    private FavoriteClutchAdapter adapter;

    public FavoriteFragment(final Context context) {
        super();
        this.context = context;
        this.layoutid = R.layout.main_favorite;

        ImageLoaderUtil.setDefImageConfig(context);
        adapter = new FavoriteClutchAdapter(context);

        spinnerSwitchHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switchSpinner();
            }
        };
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (TaskUtil.isNetworkAvailable(context)) {
            v = inflater.inflate(layoutid, null);

            prGridView = (PullToRefreshStaggeredGridView) v.findViewById(R.id.staggered_grid_favorite);
            gridView = prGridView.getRefreshableView();
            prGridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<StaggeredGridView>() {
                @Override
                public void onPullDownToRefresh(PullToRefreshBase<StaggeredGridView> refreshView) {
                    pageNo = 0;
                    getClutch(TaskUtil.getSort(context), pageNo, TaskUtil.clutchPageCount, true);
                }

                @Override
                public void onPullUpToRefresh(PullToRefreshBase<StaggeredGridView> refreshView) {

                }
            });
            gridView.setPadding(16, 0, 16, 0);
            gridView.setItemMargin(16);
            gridView.setOnLoadMoreListener(new StaggeredGridView.OnLoadMoreListener() {
                @Override
                public boolean onLoadMore() {
                    if (!gridView.isLoading()) {
                        getClutch(TaskUtil.getSort(context), pageNo + 1, TaskUtil.clutchPageCount, false);
                        return false;
                    } else {
                        return true;
                    }
                }
            });



            gridView.setOnScrollMotionListener(new StaggeredGridView.OnScrollMotionListener() {
                @Override
                public void onScrollMotion(ViewGroup view, int motionState) {
                    switch (motionState) {
                        case TOUCH_MOTION_UP:
                            spinner.startAnimation(AnimationUtils.loadAnimation(context, R.anim.spinner_down));
                            spinner.setVisibility(View.VISIBLE);
                            break;
                        case TOUCH_MOTION_DOWN:
                            spinner.startAnimation(AnimationUtils.loadAnimation(context, R.anim.spinner_up));
                            spinner.setVisibility(View.GONE);
                            break;
                    }
                }
            });

            spinner = (View) v.findViewById(R.id.favorite_spinner);
            spinnerDropdownView = v.findViewById(R.id.layout_spinner_dropdown);
            spinnerList = (GridView) v.findViewById(R.id.favorite_spinner_list);

            spinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!spinnerList.isShown()) {
                        if (TaskUtil.isNetworkAvailable(context)) {
                            getArtistList();
                        }
                    } else {
                        switchSpinner();
                    }
                }
            });

            v.findViewById(R.id.btn_favorite_setting).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, FavoriteSettingActivity.class);
                    startActivity(intent);
                    switchSpinner();
                }
            });


            pageNo = 0;
            getClutch(TaskUtil.getSort(context), pageNo, TaskUtil.clutchPageCount, true);
        } else {
            //Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
            v = inflater.inflate(R.layout.closed_page_unsustainable, null);
        }
        return v;
    }

    public void getClutch(int sort, int pageNo, int clutchPageCount, final boolean getFirst) {
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        clutch.get(sort, pageNo, clutchPageCount, TaskUtil.getUserToken(context), new Callback<ClutchUtil.Clutch.ClutchResult>() {
            @Override
            public void success(ClutchUtil.Clutch.ClutchResult result, Response response) {
                String errorMsg = ClutchUtil.Status.check(result.status.code);
                if(errorMsg.equals("")){
                    if(result.clutches.size() == 0){
                        Toast.makeText(context, "현재 로딩할 데이터가 없습니다.", Toast.LENGTH_SHORT).show();
                    }else{
                        if(getFirst){
                            adapter.clear();
                        }
                        for(ClutchUtil.ClutchArray clutch : result.clutches){

                            String content_link = "";
                            if(clutch.content_type >= 1){
                                content_link = clutch.content_link;
                            }

                            adapter.add(new Construct.MainCard(clutch.clutchId,
                                    clutch.content_body,
                                    clutch.likeCount,
                                    clutch.shared_times,
                                    clutch.commentCount,
                                    clutch.userName,
                                    clutch.cb_name,
                                    clutch.userPhoto,
                                    clutch.content_pictures,
                                    clutch.userId,
                                    clutch.content_type,
                                    content_link,
                                    clutch.cb_id,
                                    clutch.date,
                                    clutch.content_picture_width,
                                    clutch.content_picture_height));
                        }

                        if(getFirst){
                            gridView.setAdapter(adapter);
                            if(prGridView.isRefreshing()) prGridView.onRefreshComplete();
                        }else{
                            adapter.notifyDataSetChanged();
                            FavoriteFragment.this.pageNo++;
                        }
                    }
                }else{
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
                gridView.loadMoreCompleated();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try{
                    Log.e("Error Reason", retrofitError.getCause().toString());
                }catch(Exception e){
                    Log.e("UnKnownError", "MainFragment/Timeline");
                }
                gridView.loadMoreCompleated();
            }
        });
    }

    private void getArtistList() {
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        user.getUserArtists(TaskUtil.getUserId(context), TaskUtil.getUserToken(context), new Callback<ClutchUtil.User.ArtistResult>() {
            @Override
            public void success(ClutchUtil.User.ArtistResult artistResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(artistResult.status.code);
                if(errorMsg.equals("")){
                    ArrayList<Construct.ArtistList> favoriteList = new ArrayList<Construct.ArtistList>();
                    for(ClutchUtil.ArtistArray artistArray : artistResult.artists){
                        favoriteList.add(new Construct.ArtistList(
                                artistArray._id,
                                artistArray.name,
                                artistArray.profile_picture,
                                artistArray.category,
                                artistArray.starCount,
                                artistArray.weeklyStarCount));
                    }

                    Handler fSpinnerHandler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            switchSpinner();
                            ((TextView) v.findViewById(R.id.spinner_prompt_text)).setText(msg.getData().getString("selected_name"));
                        }
                    };

                    FavoriteSpinnerAdapter adapter = new FavoriteSpinnerAdapter(context, fSpinnerHandler, favoriteList);
                    switchSpinner();
                    spinnerList.setAdapter(adapter);

                }else{
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    public void switchSpinner() {
        boolean isClosed = !spinnerList.isShown();

        View darkmask = v.findViewById(R.id.spinner_darkmask);
        MainFragmentActivity.arcMenu.setVisibility(isClosed ? View.GONE : View.VISIBLE);
        spinnerDropdownView.setVisibility(isClosed ? View.VISIBLE : View.GONE);
        darkmask.setVisibility(isClosed ? View.VISIBLE : View.GONE);
        spinner.setBackgroundResource(isClosed ? R.drawable.spinner_dropdown_open_normal : R.drawable.spinner_dropdown_close);
        spinnerDropdownView.startAnimation(isClosed? AnimationUtils.loadAnimation(context, R.anim.spinner_down) : AnimationUtils.loadAnimation(context, R.anim.spinner_up));
        darkmask.setAnimation(isClosed? AnimationUtils.loadAnimation(context, android.R.anim.fade_in) : AnimationUtils.loadAnimation(context, android.R.anim.fade_out));

        if(darkmask.isShown()) {
            darkmask.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchSpinner();
                }
            });
        }
    }
}