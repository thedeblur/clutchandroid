package another.planet.clutch.fragment.profile.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.edit.EditClutchbookActivity;
import another.planet.clutch.util.*;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

public class ClutchBookFragment extends Fragment {

    int layoutid;
    Context context;

    CBAdapter cbAdapter;
    GridView gridView;
    String userId;

    public ClutchBookFragment(Context context, String userId) {
        super();
        this.context = context;
        this.layoutid = R.layout.profile_clutchbook;
        this.userId = userId;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v;

        if (TaskUtil.isNetworkAvailable(context)) {
            v = inflater.inflate(layoutid, null);
            ImageLoaderUtil.setDefImageConfig(context);
            gridView = (GridView) v.findViewById(R.id.grid);
            getcbs();
        } else {
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
            v = inflater.inflate(R.layout.closed_page_unsustainable, null);
        }
        return v;
    }

    private void getcbs() {
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        user.getUserCB(userId, TaskUtil.getUserToken(context), new Callback<ClutchUtil.User.ClutchBookResult>() {
            @Override
            public void success(ClutchUtil.User.ClutchBookResult clutchBookResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(clutchBookResult.status.code);
                if (errorMsg.equals("")) {
                    ArrayList<Construct.ClutchBook> array = new ArrayList<Construct.ClutchBook>();
                    for (ClutchUtil.ClutchBookArray cbs : clutchBookResult.cbs) {
                        array.add(new Construct.ClutchBook(
                                cbs.cb_id,
                                cbs.name,
                                cbs.description,
                                cbs.last_photos,
                                cbs.clutches_count,
                                cbs.followers_count));
                    }
                    cbAdapter = new CBAdapter(context, array);
                    gridView.setAdapter(cbAdapter);
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "ClutchBookFragment / getClutcheBook");
                }
            }
        });
    }

    class CBAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        public ArrayList<Construct.ClutchBook> cbs;

        CBAdapter(Context context, ArrayList<Construct.ClutchBook> array) {
            mInflater = LayoutInflater.from(context);
            this.cbs = array;
        }

        @Override
        public int getCount() {
            return cbs.size();
        }

        @Override
        public Object getItem(int i) {
            return cbs.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View v, ViewGroup viewGroup) {
            if (v == null) {
                v = mInflater.inflate(R.layout.item_clutchbook, null, false);
            }

            Construct.ClutchBook cb = cbs.get(i);
            ImageView thumbBig = (ImageView) v.findViewById(R.id.thumb_image_big);
            ImageView thumb1 = (ImageView) v.findViewById(R.id.thumb_image_1);
            ImageView thumb2 = (ImageView) v.findViewById(R.id.thumb_image_2);
            ImageView thumb3 = (ImageView) v.findViewById(R.id.thumb_image_3);
            ((TextView) v.findViewById(R.id.book_name)).setText(cb.name);

            int thumbCellSize = UnitSizeUtil.dp2px(context, 48);
            thumb1.setLayoutParams(new LinearLayout.LayoutParams(thumbCellSize, thumbCellSize));
            thumb2.setLayoutParams(new LinearLayout.LayoutParams(thumbCellSize, thumbCellSize));
            thumb3.setLayoutParams(new LinearLayout.LayoutParams(thumbCellSize, thumbCellSize));

            try {
                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(cb.last_photos.get(0), 200), thumbBig, ImageLoaderUtil.imageLoaderOptions());
                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(cb.last_photos.get(1), 100), thumb1, ImageLoaderUtil.imageLoaderOptions());
                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(cb.last_photos.get(2), 100), thumb2, ImageLoaderUtil.imageLoaderOptions());
                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(cb.last_photos.get(3), 100), thumb3, ImageLoaderUtil.imageLoaderOptions());
            } catch (NullPointerException e) {

            } catch (ArrayIndexOutOfBoundsException e) {

            } catch (IndexOutOfBoundsException e) {

            }

            ((Button) v.findViewById(R.id.btn_follow)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EditClutchbookActivity.class);
                    //  intent.putExtra("cb_id", cb.clutchBookId);
                    startActivity(intent);
                }
            });
            return v;
        }
    }


}