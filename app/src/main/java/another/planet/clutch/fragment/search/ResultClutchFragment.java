package another.planet.clutch.fragment.search;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.activity.MainDetailActivity;
import another.planet.clutch.activity.OtherClutchActivity;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import com.origamilabs.library.views.StaggeredGridView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

public class ResultClutchFragment extends Fragment implements View.OnClickListener {

    int layoutid;
    Context context;
    LinearLayout tabAll, tabMine;
    String keyword;
    StaggeredGridView grid;


    public ResultClutchFragment(Context context, String keyword) {
        this.context = context;
        this.layoutid = R.layout.fragment_result_clutch;
        this.keyword = keyword;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v;

        if (TaskUtil.isNetworkAvailable(context)) {
            v = inflater.inflate(layoutid, null);
            ImageLoaderUtil.setDefImageConfig(context);
            tabAll = (LinearLayout) v.findViewById(R.id.tab_all_clutch);
            tabMine = (LinearLayout) v.findViewById(R.id.tab_my_clutch);

            tabAll.setSelected(true);

            tabAll.setOnClickListener(this);
            tabMine.setOnClickListener(this);

            grid = (StaggeredGridView) v.findViewById(R.id.grid);
            grid.setPadding(16, 0, 16, 0);
            grid.setItemMargin(16);
            getClutches(TaskUtil.temp_sort, 0, TaskUtil.clutchPageCount, false);
        } else {
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
            v = inflater.inflate(R.layout.closed_page_unsustainable, null);
        }


        return v;
    }

    private void getClutches(int sort, int pageNumber, int pageCount, boolean isMine) {
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();
        if (!isMine) {
            clutch.search(keyword, sort, pageNumber, pageCount, new Callback<ClutchUtil.Clutch.ClutchResult>() {
                @Override
                public void success(ClutchUtil.Clutch.ClutchResult clutchResult, Response response) {
                    String errorMsg = ClutchUtil.Status.check(clutchResult.status.code);
                    if (errorMsg.equals("")) {
                        ArrayList<Construct.MainCard> array = new ArrayList<Construct.MainCard>();
                        for (ClutchUtil.ClutchArray clutch : clutchResult.clutches) {
                            String content_link = "";
                            if (clutch.content_type >= 1) {
                                content_link = clutch.content_link;
                            }

                            array.add(new Construct.MainCard(clutch.clutchId,
                                    clutch.content_body,
                                    clutch.shared_times,
                                    clutch.shared_times,
                                    clutch.commentCount,
                                    clutch.userName,
                                    clutch.cb_name,
                                    clutch.userPhoto,
                                    clutch.content_pictures,
                                    clutch.userId,
                                    clutch.content_type,
                                    content_link,
                                    clutch.cb_id,
                                    clutch.date,
                                    clutch.content_picture_width,
                                    clutch.content_picture_height));
                        }
                        CardAdapter adapter = new CardAdapter(context, array);
                        grid.setAdapter(adapter);
                    } else {
                        Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    String errorMsg = ClutchUtil.Status.check(5000);
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    try {
                        Log.e("Error Reason", retrofitError.getCause().toString());
                    } catch (Exception e) {
                        Log.e("UnKnownError", "ResultClutchFragment / getClutches");
                    }
                }
            });
        } else {
            clutch.searchMine(TaskUtil.getUserId(context), keyword, new Callback<ClutchUtil.Clutch.ClutchResult>() {
                @Override
                public void success(ClutchUtil.Clutch.ClutchResult clutchResult, Response response) {
                    String errorMsg = ClutchUtil.Status.check(clutchResult.status.code);
                    if (errorMsg.equals("")) {
                        ArrayList<Construct.MainCard> array = new ArrayList<Construct.MainCard>();
                        for (ClutchUtil.ClutchArray clutch : clutchResult.clutches) {
                            String content_link = "";
                            if (clutch.content_type >= 1) {
                                content_link = clutch.content_link;
                            }

                            array.add(new Construct.MainCard(clutch.clutchId,
                                    clutch.content_body,
                                    clutch.shared_times,
                                    clutch.shared_times,
                                    clutch.commentCount,
                                    clutch.userName,
                                    clutch.cb_name,
                                    clutch.userPhoto,
                                    clutch.content_pictures,
                                    clutch.userId,
                                    clutch.content_type,
                                    content_link,
                                    clutch.cb_id,
                                    clutch.date,
                                    clutch.content_picture_width,
                                    clutch.content_picture_height));
                        }
                        CardAdapter adapter = new CardAdapter(context, array);
                        grid.setAdapter(adapter);
                    } else {
                        Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    String errorMsg = ClutchUtil.Status.check(5000);
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                    try {
                        Log.e("Error Reason", retrofitError.getCause().toString());
                    } catch (Exception e) {
                        Log.e("UnKnownError", "ResultClutchFragment / getClutches");
                    }
                }
            });
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab_all_clutch:
                getClutches(0, 0, TaskUtil.clutchPageCount, false);
                tabAll.setSelected(true);
                tabMine.setSelected(false);
                break;
            case R.id.tab_my_clutch:
                getClutches(0, 0, TaskUtil.clutchPageCount, true);
                tabAll.setSelected(false);
                tabMine.setSelected(true);
                break;
        }
    }

    class CardAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        public ArrayList<Construct.MainCard> array;

        CardAdapter(Context context, ArrayList<Construct.MainCard> array) {
            mInflater = LayoutInflater.from(context);
            this.array = array;
        }

        @Override
        public int getCount() {
            return array.size();
        }

        @Override
        public Object getItem(int i) {
            return array.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int position, View v, ViewGroup viewGroup) {
            if (v == null) {
                v = mInflater.inflate(R.layout.item_card, null, false);
            }

            final Construct.MainCard mc = array.get(position);
            ImageView cardImageView = (ImageView) v.findViewById(R.id.card_image);
            ((TextView) v.findViewById(R.id.ment)).setText(mc.mentMessage);
            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(mc.content_pictures.get(0), 300), cardImageView, ImageLoaderUtil.imageLoaderOptions());
            //   cardImageView.setImageDrawable((Drawable) mc.thumbImage);
            ((TextView) v.findViewById(R.id.text_card_like_count)).setText(Integer.toString(mc.likeCount));
            ((TextView) v.findViewById(R.id.text_card_clutch_count)).setText(Integer.toString(mc.clutchCount));
            ((TextView) v.findViewById(R.id.text_card_comment_count)).setText(Integer.toString(mc.commentCount));
            ((TextView) v.findViewById(R.id.profile_name)).setText(mc.profileName);
            ((TextView) v.findViewById(R.id.cb_name)).setText(mc.cb_name);
            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(mc.profileImage, 100), ((ImageView) v.findViewById(R.id.profile_image)), ImageLoaderUtil.imageLoaderOptions());
            //    ((ImageView) v.findViewById(R.id.profile_image)).setImageDrawable((Drawable) arrayList.get(0).profileImage);
            ImageView playVideo = (ImageView) v.findViewById(R.id.play_video);
            if(mc.contentType == 1){
                playVideo.setVisibility(View.VISIBLE);
            }
            playVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri uri = Uri.parse(mc.content_link);
                    Intent i = new Intent(Intent.ACTION_VIEW,uri);
                    startActivity(i);
                }
            });
            cardImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Construct.MainCard mc = array.get(position);
                    Intent intent = new Intent(context, MainDetailActivity.class);
                    intent.putExtra("postId", mc.postId);
                    startActivity(intent);
                }
            });

            cardImageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(context, "길게눌렀엉", Toast.LENGTH_SHORT).show();
                    Vibrator vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(50);
                    return true;
                }
            });

            ((ImageView) v.findViewById(R.id.profile_image)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Construct.MainCard mc = array.get(position);
                    Intent intent = new Intent(context, OtherClutchActivity.class);
                    intent.putExtra("cbId", mc.cb_id);
                    intent.putExtra("userId", mc.userId);
                    intent.putExtra("cbName", mc.cb_name);
                    intent.putExtra("userName", mc.profileName);
                    intent.putExtra("userImage", mc.profileImage);
                    startActivity(intent);
                }
            });
            return v;
        }
    }
}