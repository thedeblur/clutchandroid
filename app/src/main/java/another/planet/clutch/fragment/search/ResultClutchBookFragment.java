package another.planet.clutch.fragment.search;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.util.*;
import another.planet.clutch.util.Construct.ClutchBook;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

public class ResultClutchBookFragment extends Fragment {

    int layoutid;
    Context context;

    String keyword;
    ClutchBookAdapter cbAdapter;
    GridView gridView;

    public ResultClutchBookFragment(Context context, String keyword) {
        this.context = context;
        this.layoutid = R.layout.fragment_result_clutchbook;
        this.keyword = keyword;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v;

        if (TaskUtil.isNetworkAvailable(context)) {
            v = inflater.inflate(layoutid, null);
            ImageLoaderUtil.setDefImageConfig(context);
            gridView = (GridView) v.findViewById(R.id.grid);
            searchcbs(0, TaskUtil.clutchPageCount, true);
        } else {
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
            v = inflater.inflate(R.layout.closed_page_unsustainable, null);
        }
        return v;
    }

    private void searchcbs(int pageNo, int pageCount, boolean getFirst) {
        ClutchUtil.ClutchBook cb = ClutchUtil.getClutchBookInstance();
        cb.search(keyword, TaskUtil.temp_sort, pageNo, pageCount, new Callback<ClutchUtil.User.ClutchBookResult>() {
            @Override
            public void success(ClutchUtil.User.ClutchBookResult clutchBookResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(clutchBookResult.status.code);
                if (errorMsg.equals("")) {
                    ArrayList<Construct.ClutchBook> array = new ArrayList<Construct.ClutchBook>();
                    for (ClutchUtil.ClutchBookArray cbs : clutchBookResult.cbs) {
                        array.add(new Construct.ClutchBook(
                                cbs.cb_id,
                                cbs.name,
                                cbs.description,
                                cbs.last_photos,
                                cbs.clutches_count,
                                cbs.followers_count));
                    }
                    cbAdapter = new ClutchBookAdapter(context, array);
                    gridView.setAdapter(cbAdapter);
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    class ClutchBookAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        ArrayList<ClutchBook> array;

        ClutchBookAdapter(Context context, ArrayList<ClutchBook> array) {
            mInflater = LayoutInflater.from(context);
            this.array = array;
        }

        @Override
        public int getCount() {
            return array.size();
        }

        @Override
        public Object getItem(int i) {
            return array.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View v, ViewGroup viewGroup) {
            if (v == null) {
                v = mInflater.inflate(R.layout.item_clutchbook, null, false);
            }
            Construct.ClutchBook cb = array.get(position);
            ImageView thumbBig = (ImageView) v.findViewById(R.id.thumb_image_big);
            ImageView thumb1 = (ImageView) v.findViewById(R.id.thumb_image_1);
            ImageView thumb2 = (ImageView) v.findViewById(R.id.thumb_image_2);
            ImageView thumb3 = (ImageView) v.findViewById(R.id.thumb_image_3);

            ((TextView) v.findViewById(R.id.book_name)).setText(cb.name);

            int thumbCellSize = UnitSizeUtil.dp2px(context, 48);
            thumb1.setLayoutParams(new LinearLayout.LayoutParams(thumbCellSize, thumbCellSize));
            thumb2.setLayoutParams(new LinearLayout.LayoutParams(thumbCellSize, thumbCellSize));
            thumb3.setLayoutParams(new LinearLayout.LayoutParams(thumbCellSize, thumbCellSize));

            try {
                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(cb.last_photos.get(0), 200), thumbBig, ImageLoaderUtil.imageLoaderOptions());
                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(cb.last_photos.get(1), 100), thumb1, ImageLoaderUtil.imageLoaderOptions());
                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(cb.last_photos.get(2), 100), thumb2, ImageLoaderUtil.imageLoaderOptions());
                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(cb.last_photos.get(3), 100), thumb3, ImageLoaderUtil.imageLoaderOptions());
            } catch (NullPointerException e) {

            } catch (ArrayIndexOutOfBoundsException e) {

            } catch (IndexOutOfBoundsException e) {

            }
            return v;
        }
    }

}