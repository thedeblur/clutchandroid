package another.planet.clutch.fragment.main;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import another.planet.clutch.R;
import another.planet.clutch.fragment.board.FreeBoardFragment;
import another.planet.clutch.fragment.board.NewsFragment;
import another.planet.clutch.fragment.board.NoticeFragment;
import another.planet.clutch.widget.NotSwipeyViewPager;

public class BoardFragment extends Fragment implements View.OnClickListener {

    int layoutid;
    Context context;
    NotSwipeyViewPager mPager;
    LinearLayout tabFree, tabNews, tabNotice;

    public BoardFragment(Context context) {
        super();
        this.context = context;
        this.layoutid = R.layout.main_board;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(layoutid, null);
        BoardAdapter adapter = new BoardAdapter(getFragmentManager());
        mPager = (NotSwipeyViewPager) v.findViewById(R.id.board_pager);
        mPager.setAdapter(adapter);

        tabFree = (LinearLayout) v.findViewById(R.id.tab_board_freeboard);
        tabNews = (LinearLayout) v.findViewById(R.id.tab_board_news);
        tabNotice = (LinearLayout) v.findViewById(R.id.tab_board_notice);

        tabFree.setOnClickListener(this);
        tabNews.setOnClickListener(this);
        tabNotice.setOnClickListener(this);

        tabFree.setSelected(true);
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                selectTab(arg0);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab_board_freeboard:
                mPager.setCurrentItem(0);
                break;
            case R.id.tab_board_news:
                mPager.setCurrentItem(1);
                break;
            case R.id.tab_board_notice:
                mPager.setCurrentItem(2);
                break;
        }
    }

    // 현재 보이는 탭을 포커스처리함
    public void selectTab(int position) {
        switch (position) {
            case 0:
                initTabImage();
                tabFree.setSelected(true);
                break;
            case 1:
                initTabImage();
                tabNews.setSelected(true);
                break;
            case 2:
                initTabImage();
                tabNotice.setSelected(true);
                break;
        }
    }

    public void initTabImage() {
        //탭의 배경을 초기화한다.
        tabFree.setSelected(false);
        tabNews.setSelected(false);
        tabNotice.setSelected(false);
    }

    private class BoardAdapter extends FragmentPagerAdapter {

        // The fragment which needs to show on the viewpager.
        private Fragment[] FRAGMENTS = {
                new FreeBoardFragment(context),
                new NewsFragment(context),
                new NoticeFragment(context)
        };

        public BoardAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return FRAGMENTS[position];
        }

        @Override
        public int getCount() {
            return FRAGMENTS.length;
        }

    }

}