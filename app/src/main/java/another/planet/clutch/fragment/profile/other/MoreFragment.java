package another.planet.clutch.fragment.profile.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import another.planet.clutch.R;
import another.planet.clutch.activity.FollowActivity;

public class MoreFragment extends Fragment {

    int layoutid;
    Context context;
    String userId, followerLength, followingLength;

    public MoreFragment(Context context, String userId, String followerLength, String followingLength) {
        super();
        this.context = context;
        this.layoutid = R.layout.profile_more;
        this.userId = userId;
        this.followerLength = followerLength;
        this.followingLength = followingLength;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v;
        v = inflater.inflate(layoutid, null);
        ((TextView) v.findViewById(R.id.myprofile_following_count)).setText(followingLength);
        ((TextView) v.findViewById(R.id.myprofile_follower_count)).setText(followerLength);
        ((FrameLayout) v.findViewById(R.id.layout_myprofile_follower)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, FollowActivity.class);
                intent.putExtra("isFollowing", false);
                intent.putExtra("userId", userId);
                startActivity(intent);
            }
        });

        ((FrameLayout) v.findViewById(R.id.layout_myprofile_following)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, FollowActivity.class);
                intent.putExtra("isFollowing", true);
                intent.putExtra("userId", userId);
                startActivity(intent);
            }
        });

        return v;
    }


}