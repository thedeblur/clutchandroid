package another.planet.clutch.fragment.main;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.activity.SettingActivity;
import another.planet.clutch.dialog.ProfileImageDialog;
import another.planet.clutch.fragment.profile.ClutchBookFragment;
import another.planet.clutch.fragment.profile.ClutchFragment;
import another.planet.clutch.fragment.profile.LikeFragment;
import another.planet.clutch.fragment.profile.MoreFragment;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import another.planet.clutch.widget.NotSwipeyViewPager;
import com.android.camera.CropImageIntentBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class ProfileFragment extends Fragment implements View.OnClickListener, DialogInterface.OnDismissListener {

    int layoutid;
    Context context;
    NotSwipeyViewPager mPager;
    LinearLayout tabLike, tabClutch, tabClutchBook, tabMore;
    TextView txtName, txtMent, txtNick;
    ImageView profileImage;
    public static final int REFRESH_PROFILE = 0;
    private static final int SELECT_PICTURE = 1;
    private static final int CROP_PICTURE = 2;
    private static final String IMAGE_RESULT_PATH = Environment.getExternalStorageDirectory() + "/Clutch";
    private static final String CROP_IMAGE_PATH = "/.tempPic.jpg";
    private Uri mTempOutputUri;
    private boolean isCustomProfile = false;

    public static Handler mDialogHandler;
    public static final int DIALOG_PROFILE_IMAGE = 0;
    public static final String PROFILE_TAKE_PICTURE = "0";
    public static final String PROFILE_CROP_IMAGE = "1";
    public static final String PROFILE_FROM_FACEBOOK = "2";

    public ProfileFragment(Context context) {
        super();
        this.context = context;
        this.layoutid = R.layout.main_profile;
        ImageLoaderUtil.setDefImageConfig(context);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(layoutid, null);

        ProfileMenuAdapter adapter = new ProfileMenuAdapter(getFragmentManager());
        mPager = (NotSwipeyViewPager) v.findViewById(R.id.menu_pager);
        mPager.setAdapter(adapter);

        tabLike = (LinearLayout) v.findViewById(R.id.tab_profile_like);
        tabClutch = (LinearLayout) v.findViewById(R.id.tab_profile_clutch);
        tabClutchBook = (LinearLayout) v.findViewById(R.id.tab_profile_clutchbook);
        tabMore = (LinearLayout) v.findViewById(R.id.tab_profile_more);
        ImageButton btnSetting = (ImageButton) v.findViewById(R.id.profile_setting);
        profileImage = (ImageView) v.findViewById(R.id.profile_image);
        txtName = (TextView) v.findViewById(R.id.profile_name);
        txtMent = (TextView) v.findViewById(R.id.profile_ment);

        getUserData();
        tabLike.setOnClickListener(this);
        tabClutch.setOnClickListener(this);
        tabClutchBook.setOnClickListener(this);
        tabMore.setOnClickListener(this);
        btnSetting.setOnClickListener(this);
        profileImage.setOnClickListener(this);

        tabLike.setSelected(true);

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(TaskUtil.getUserDataString(context, "picture"), 200), profileImage, ImageLoaderUtil.imageLoaderOptions());
            }
        };
        handler.sendEmptyMessageDelayed(1, 0);

        mTempOutputUri = Uri.fromFile(new File(IMAGE_RESULT_PATH + CROP_IMAGE_PATH));

        mDialogHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ProfileFragment.this.handleMessage(msg);
            }
        };

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                selectTab(arg0);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        return v;
    }

    public void getUserData() {
      /*  SharedPreferences sp = context.getSharedPreferences("account_preferences", Context.MODE_PRIVATE);
        String name = sp.getString("name", "");
        String status = sp.getString("status_message", "");            */
        txtName.setText(TaskUtil.getUserDataString(context, "name"));
        txtMent.setText(TaskUtil.getUserDataString(context, "status_message"));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab_profile_like:
                mPager.setCurrentItem(0);
                break;
            case R.id.tab_profile_clutch:
                mPager.setCurrentItem(1);
                break;
            case R.id.tab_profile_clutchbook:
                mPager.setCurrentItem(2);
                break;
            case R.id.tab_profile_more:
                mPager.setCurrentItem(3);
                break;
            case R.id.profile_setting:
                startActivity(new Intent(context, SettingActivity.class));
                break;
            case R.id.profile_image:
                ProfileImageDialog pDialog = new ProfileImageDialog(context);
                pDialog.setOnDismissListener(this);
                pDialog.show();
                break;
        }
    }

    // 현재 보이는 탭을 포커스처리함
    public void selectTab(int position) {
        switch (position) {
            case 0:
                initTabImage();
                tabLike.setSelected(true);
                break;
            case 1:
                initTabImage();
                tabClutch.setSelected(true);
                break;
            case 2:
                initTabImage();
                tabClutchBook.setSelected(true);
                break;
            case 3:
                initTabImage();
                tabMore.setSelected(true);
                break;
        }
    }

    public void initTabImage() {
        //탭의 배경을 초기화한다.
        tabLike.setSelected(false);
        tabClutch.setSelected(false);
        tabClutchBook.setSelected(false);
        tabMore.setSelected(false);
    }


    @Override
    public void onDismiss(DialogInterface dialogInterface) {

    }

    //TODO: 사진 선택 다이얼로그 후 동작
    protected void handleMessage(Message msg) {
        String item = msg.obj.toString();
        if (item.equals(PROFILE_TAKE_PICTURE)) {
            Toast.makeText(context, "Take picture", Toast.LENGTH_SHORT).show();
        } else if (item.equals(PROFILE_CROP_IMAGE)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "프로필 사진 선택"), SELECT_PICTURE);
        } else if (item.equals(PROFILE_FROM_FACEBOOK)) {
            Toast.makeText(context, "From facebook", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) return;

        if (requestCode == SELECT_PICTURE) {
            Uri selectedImage = data.getData();
            CropImageIntentBuilder intentBuilder = new CropImageIntentBuilder(480, 480, 480, 480, mTempOutputUri);
            intentBuilder.setSourceImage(selectedImage);
            intentBuilder.setScale(true);
            startActivityForResult(intentBuilder.getIntent(context), CROP_PICTURE);
        }

        if (requestCode == CROP_PICTURE) {
            //profileImageBitmap = BitmapFactory.decodeFile(IMAGE_RESULT_PATH + CROP_IMAGE_PATH);
            ImageLoaderUtil.imageLoader.displayImage(mTempOutputUri.toString(), profileImage, ImageLoaderUtil.imageLoaderOptions());

            uploadPicture();
        }
    }

    private void uploadPicture() {
        new AsyncTask<String, String, String>() {

            @Override
            protected void onPostExecute(String s) {
                if (!s.contains("error") && !s.equals("")) {
                    Toast.makeText(context, TaskUtil.SuccessMessage, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                String endResult = TaskUtil.uploadImage(context, mTempOutputUri);
                Log.e("endResult", endResult);

                if (!endResult.contains("error") && !endResult.equals("")) {
                    try {
                        JSONObject jObject = new JSONObject(endResult);
                        String imageId = jObject.getString("imageId");
                        endResult = TaskUtil.patchUserData(context, null, null, imageId, null, null);

                        if (!endResult.contains("error") && !endResult.equals(""))
                            TaskUtil.saveUserDataString(context, "picture", imageId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jObject = new JSONObject(endResult);
                        Log.e("ErrorMessage", jObject.getJSONObject("status").getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                return endResult;
            }
        }.execute();
    }

    private class ProfileMenuAdapter extends FragmentPagerAdapter {

        // The fragment which needs to show on the viewpager.
        private Fragment[] FRAGMENTS = {
                new LikeFragment(context, TaskUtil.getUserId(context)),
                new ClutchFragment(context, TaskUtil.getUserId(context)),
                new ClutchBookFragment(context, TaskUtil.getUserId(context)),
                new MoreFragment(context, TaskUtil.getUserId(context))
        };

        public ProfileMenuAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return FRAGMENTS[position];
        }

        @Override
        public int getCount() {
            return FRAGMENTS.length;
        }

    }
}