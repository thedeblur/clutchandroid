package another.planet.clutch.fragment.board;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.activity.MainDetailActivity;
import another.planet.clutch.activity.OtherClutchActivity;
import another.planet.clutch.activity.OtherProfileActivity;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.TaskUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

public class NewsFragment extends Fragment {

    int layoutid;
    Context context;
    ArrayList<Construct.Notification> array;
    NotificationAdapter nAdapter;
    ListView lv;

    public NewsFragment(Context context) {
        super();
        this.context = context;
        this.layoutid = R.layout.board_news;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v;
        if (TaskUtil.isNetworkAvailable(context)) {
            v = inflater.inflate(layoutid, null);
            lv = (ListView) v.findViewById(R.id.lv);
            getNoti(true);
        } else {
            v = inflater.inflate(R.layout.closed_page_unsustainable, null);
        }
        return v;
    }

    private void getNoti(final boolean getFirst) {
        ClutchUtil.Notification noti = ClutchUtil.getNotiInstance();
        noti.getNoti(TaskUtil.getUserToken(context), new Callback<ClutchUtil.Notification.NotificationResult>() {
            @Override
            public void success(ClutchUtil.Notification.NotificationResult notificationResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(notificationResult.status.code);
                if (errorMsg.equals("")) {
                    if (getFirst) array = new ArrayList<Construct.Notification>();
                    for (ClutchUtil.NotificationArray notification : notificationResult.notifications) {
                        String actionBundleName = "";
                        if(!notification.action.equals("OPEN CLUTCH") && !notification.ownerId.equals(TaskUtil.getUserId(context))) actionBundleName = notification.actionBundleName;
                        array.add(new Construct.Notification(
                                notification.ownerId,
                                notification.message,
                                notification.action,
                                notification.actionBundle,
                                actionBundleName,
                                notification.date));
                    }
                    if (getFirst) {
                        nAdapter = new NotificationAdapter(context, 0, array);
                        lv.setAdapter(nAdapter);
                    } else nAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    public class NotificationAdapter extends ArrayAdapter<Object> {
        ArrayList<Construct.Notification> array;

        public NotificationAdapter(Context context, int resource, ArrayList<Construct.Notification> array) {
            super(context, resource);
            this.array = array;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final Construct.Notification notificationItem = array.get(position);
            if (notificationItem.actionBundle.equals(TaskUtil.getUserId(context))) {
                convertView = inflater.inflate(R.layout.item_ballon_message_me, null);
            } else {
                convertView = inflater.inflate(R.layout.item_ballon_message_you, null);
            }
            ((TextView) convertView.findViewById(R.id.txtMessage)).setText(Html.fromHtml(notificationItem.message));
            ((TextView) convertView.findViewById(R.id.txt_send_time)).setText(TaskUtil.getDate(notificationItem.date));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (notificationItem.action.equals("OPEN CB")) {
                        Intent i = new Intent(context, OtherClutchActivity.class);
                        i.putExtra("cbId", notificationItem.actionBundle);
                        i.putExtra("userId", notificationItem.ownerId);
                        i.putExtra("cbName", notificationItem.actionBundleName);
                        startActivity(i);
                    } else if (notificationItem.action.equals("OPEN CLUTCH")) {
                        Intent i = new Intent(context, MainDetailActivity.class);
                        i.putExtra("postId", notificationItem.actionBundle);
                        startActivity(i);
                    } else if (notificationItem.action.equals("OPEN USER")) {
                        Intent i = new Intent(context, OtherProfileActivity.class);
                        i.putExtra("userId", notificationItem.actionBundle);
                        i.putExtra("userName", notificationItem.actionBundleName);
                        Log.e(notificationItem.actionBundleName, notificationItem.actionBundle);
                        startActivity(i);
                    }
                }
            });
            return convertView;
        }

        @Override
        public int getCount() {
            return array.size();
        }
    }
}