package another.planet.clutch.fragment.main;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.adapter.FilterClutchListAdapter;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshStaggeredGridView;
import com.origamilabs.library.views.StaggeredGridView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainFragment extends Fragment {

    int layoutid;
    private Context context;
    LinearLayout sortNew, sortHit;

    int pageNo;

    PullToRefreshStaggeredGridView prGridView;
    StaggeredGridView gridView;
    FilterClutchListAdapter adapter;

    public MainFragment(Context context) {
        super();
        this.context = context;
        this.layoutid = R.layout.main_home;
        ImageLoaderUtil.setDefImageConfig(context);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                getClutch(TaskUtil.getSort(MainFragment.this.context), 0, TaskUtil.clutchPageCount, true);
            }
        };

        adapter = new FilterClutchListAdapter(context, handler );

        View v;
        if (TaskUtil.isNetworkAvailable(context)) {
            v = inflater.inflate(layoutid, null);

            prGridView = (PullToRefreshStaggeredGridView) v.findViewById(R.id.main_grid);
            gridView = prGridView.getRefreshableView();
            prGridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<StaggeredGridView>() {
                @Override
                public void onPullDownToRefresh(PullToRefreshBase<StaggeredGridView> refreshView) {
                    pageNo = 0;
                    getClutch(TaskUtil.getSort(context), pageNo, TaskUtil.clutchPageCount, true);
                }

                @Override
                public void onPullUpToRefresh(PullToRefreshBase<StaggeredGridView> refreshView) {

                }
            });
            gridView.setPadding(16, 0, 16, 0);
            gridView.setItemMargin(16);
            gridView.setOnLoadMoreListener(new StaggeredGridView.OnLoadMoreListener() {
                @Override
                public boolean onLoadMore() {
                    if (!gridView.isLoading()) {
                        getClutch(TaskUtil.getSort(context), pageNo + 1, TaskUtil.clutchPageCount, false);
                        return false;
                    } else {
                        return true;
                    }
                }
            });

            pageNo = 0;
            getClutch(TaskUtil.getSort(context), pageNo, TaskUtil.clutchPageCount, true);
        } else {
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
            v = inflater.inflate(R.layout.closed_page_unsustainable, null);
        }

        return v;
    }

    public void getClutch(int sort, int pageNo, int clutchPageCount, final boolean getFirst) {
        ClutchUtil.Clutch clutch = ClutchUtil.getClutchInstance();

        clutch.get(sort, pageNo, clutchPageCount, "", new Callback<ClutchUtil.Clutch.ClutchResult>() {
            @Override
            public void success(ClutchUtil.Clutch.ClutchResult result, Response response) {
                String errorMsg = ClutchUtil.Status.check(result.status.code);
                if (errorMsg.equals("")) {
                    if (result.clutches.size() == 0) {
                        Toast.makeText(context, "현재 로딩할 데이터가 없습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        if (getFirst) {
                            adapter.clear();
                        }
                        for (ClutchUtil.ClutchArray clutch : result.clutches) {

                            String content_link = "";
                            if (clutch.content_type >= 1) {
                                content_link = clutch.content_link;
                            }

                            adapter.add(new Construct.MainCard(clutch.clutchId,
                                    clutch.content_body,
                                    clutch.likeCount,
                                    clutch.shared_times,
                                    clutch.commentCount,
                                    clutch.userName,
                                    clutch.cb_name,
                                    clutch.userPhoto,
                                    clutch.content_pictures,
                                    clutch.userId,
                                    clutch.content_type,
                                    content_link,
                                    clutch.cb_id,
                                    clutch.date,
                                    clutch.content_picture_width,
                                    clutch.content_picture_height));
                        }

                        if (getFirst) {
                            gridView.setAdapter(adapter);
                            if (prGridView.isRefreshing()) prGridView.onRefreshComplete();
                        } else {
                            adapter.notifyDataSetChanged();
                            MainFragment.this.pageNo++;
                        }
                    }
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
                gridView.loadMoreCompleated();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "MainFragment/Timeline");
                }
                gridView.loadMoreCompleated();
            }
        });
    }

    // 현재 보이는 탭을 포커스처리함
    public void selectTab(int position) {
        switch (position) {
            case 0:
                initTabImage();
                sortHit.setSelected(true);
                break;
            case 1:
                initTabImage();
                sortNew.setSelected(true);
                break;
        }
    }

    public void initTabImage() {
        //탭의 배경을 초기화한다.
        sortNew.setSelected(false);
        sortHit.setSelected(false);
    }
}
