package another.planet.clutch.fragment.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import another.planet.clutch.R;
import another.planet.clutch.activity.MainDetailActivity;
import another.planet.clutch.activity.OtherProfileActivity;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import com.origamilabs.library.views.StaggeredGridView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

public class LikeFragment extends Fragment {

    int layoutid;
    Context context;
    String userId;
    StaggeredGridView gridView;
    StaggeredAdapter adapter;


    public LikeFragment(Context context, String userId) {
        super();
        this.context = context;
        this.layoutid = R.layout.profile_like;
        this.userId = userId;
        ImageLoaderUtil.setDefImageConfig(context);
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v;

        if (TaskUtil.isNetworkAvailable(context)) {
            v = inflater.inflate(layoutid, null);
            ImageLoaderUtil.setDefImageConfig(context);
            gridView = (StaggeredGridView) v.findViewById(R.id.profile_like_grid);
            gridView.setPadding(16, 0, 16, 0);
            gridView.setItemMargin(16);
            getClutches(TaskUtil.temp_sort, 0, TaskUtil.clutchPageCount);
        } else {
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
            v = inflater.inflate(R.layout.closed_page_unsustainable, null);
        }

        return v;
    }

    public void getClutches(int sort, int pageNumber, int pageCount) {
        ClutchUtil.User user = ClutchUtil.getUserInstance();
        user.getUserLikes(userId, sort, pageNumber, pageCount, TaskUtil.getUserToken(context), new Callback<ClutchUtil.Clutch.ClutchResult>() {
            @Override
            public void success(ClutchUtil.Clutch.ClutchResult clutchResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(clutchResult.status.code);
                if (errorMsg.equals("")) {
                    ArrayList<Construct.MainCard> array = new ArrayList<Construct.MainCard>();
                    for (ClutchUtil.ClutchArray clutch : clutchResult.clutches) {

                        String content_link = "";
                        if (clutch.content_type >= 1) {
                            content_link = clutch.content_link;
                        }

                        array.add(new Construct.MainCard(clutch.clutchId,
                                clutch.content_body,
                                clutch.likeCount,
                                clutch.shared_times,
                                clutch.commentCount,
                                clutch.userName,
                                clutch.cb_name,
                                clutch.userPhoto,
                                clutch.content_pictures,
                                clutch.userId,
                                clutch.content_type,
                                content_link,
                                clutch.cb_id,
                                clutch.date,
                                clutch.content_picture_width,
                                clutch.content_picture_height));
                    }
                    adapter = new StaggeredAdapter(context, 0, array);
                    gridView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "LikeFragment / getClutches");
                }
            }
        });
    }

    private class StaggeredAdapter extends ArrayAdapter<Object> implements DialogInterface.OnDismissListener {

        ArrayList<Construct.MainCard> array;
        Construct.MainCard items;

        public StaggeredAdapter(Context context, int textViewResourceId,
                                ArrayList<Construct.MainCard> array) {
            super(context, textViewResourceId);

            this.array = array;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflate = LayoutInflater.from(getContext());
            convertView = inflate.inflate(R.layout.item_main, null);

            ImageView cardImageView = (ImageView) convertView.findViewById(R.id.card_image);
            LinearLayout uploader = (LinearLayout) convertView.findViewById(R.id.writer_info);

            ((View) convertView.findViewById(R.id.filter_card)).setVisibility(View.GONE);
            ((View) convertView.findViewById(R.id.content_card)).setVisibility(View.VISIBLE);
            items = array.get(position);
            ((TextView) convertView.findViewById(R.id.text_card_ment)).setText(items.mentMessage);
            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(items.content_pictures.get(0), 300), cardImageView, ImageLoaderUtil.imageLoaderOptions());
            //cardImageView.setImageUrl(TaskUtil.getCustomImageURL(items.imageURL, 300), imageLoader);
            ((TextView) convertView.findViewById(R.id.text_card_like_count)).setText(Integer.toString(items.likeCount));
            ((TextView) convertView.findViewById(R.id.text_card_clutch_count)).setText(Integer.toString(items.clutchCount));
            ((TextView) convertView.findViewById(R.id.text_card_comment_count)).setText(Integer.toString(items.commentCount));
            ((TextView) convertView.findViewById(R.id.profile_name)).setText(items.profileName);
            ((TextView) convertView.findViewById(R.id.cb_name)).setText(items.cb_name);
            ImageLoaderUtil.imageLoader.displayImage(ImageLoaderUtil.getImageURL(items.profileImage, 100), ((ImageView) convertView.findViewById(R.id.profile_image)), ImageLoaderUtil.imageLoaderOptions());
            //((ImageView) convertView.findViewById(R.id.profile_image)).setImageUrl(TaskUtil.getCustomImageURL(items.profileImage, 100), imageLoader);


            cardImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, MainDetailActivity.class);
                    intent.putExtra("postId", array.get(position).postId);
                    startActivity(intent);
                }
            });

            cardImageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(context, "길게눌렀엉", Toast.LENGTH_SHORT).show();
                    Vibrator vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(50);
                    return true;
                }
            });

            uploader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, OtherProfileActivity.class);
                    startActivity(intent);
                }
            });

            return convertView;
        }

        @Override
        public int getCount() {
            return array.size();
        }

        @Override
        public void onDismiss(DialogInterface dialogInterface) {

        }
    }
}