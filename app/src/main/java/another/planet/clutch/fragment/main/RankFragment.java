package another.planet.clutch.fragment.main;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import another.planet.clutch.R;
import another.planet.clutch.adapter.RankListAdapter;
import another.planet.clutch.dialog.UserBallonRankDialog;
import another.planet.clutch.util.ClutchUtil;
import another.planet.clutch.util.Construct;
import another.planet.clutch.util.ImageLoaderUtil;
import another.planet.clutch.util.TaskUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

public class RankFragment extends Fragment implements AdapterView.OnItemClickListener{

    int layoutid;
    Context context;
    RankListAdapter adapter;
    ListView lv;
    ArrayList<Construct.RankList> array;

    public RankFragment(Context context) {
        super();
        this.context = context;
        this.layoutid = R.layout.main_rank;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v;

        if (TaskUtil.isNetworkAvailable(context)) {
            v = inflater.inflate(layoutid, null);
            View header = View.inflate(context, R.layout.item_rank_header, null);

            ((TextView)header.findViewById(R.id.text_rank_list_part)).setText("2014년 1월 1~2주째");
            
            header.findViewById(R.id.btn_rank_prev).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "prev", Toast.LENGTH_SHORT).show();
                }
            });

            header.findViewById(R.id.btn_rank_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "next", Toast.LENGTH_SHORT).show();
                }
            });

            lv = (ListView) v.findViewById(R.id.rank_list);
            lv.addHeaderView(header);
            lv.setOnItemClickListener(this);

            ImageLoaderUtil.setDefImageConfig(context);
            getRanking(0, 0, TaskUtil.rankListCount);
        } else {
            Toast.makeText(context, TaskUtil.NetworkMessage, Toast.LENGTH_SHORT).show();
            v = inflater.inflate(R.layout.closed_page_unsustainable, null);
        }

        return v;
    }

    private void getRanking(int prevweek, int pageNo, int pageCount) {
        ClutchUtil.Ranking ranking = ClutchUtil.getRankingInstance();
        ranking.getRanking(prevweek, pageNo, pageCount, new Callback<ClutchUtil.Ranking.RankingResult>() {
            @Override
            public void success(ClutchUtil.Ranking.RankingResult rankingResult, Response response) {
                String errorMsg = ClutchUtil.Status.check(rankingResult.status.code);
                if (errorMsg.equals("")) {
                    array = new ArrayList<Construct.RankList>();
                    for (ClutchUtil.RankingArray rankingArray : rankingResult.ranks) {
                        array.add(new Construct.RankList(
                                rankingArray.rank,
                                rankingArray.name,
                                rankingArray.profile,
                                rankingArray.star,
                                rankingArray.artistId));
                    }
                    adapter = new RankListAdapter(context, R.layout.item_rank_row, array);
                    lv.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                String errorMsg = ClutchUtil.Status.check(5000);
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                try {
                    Log.e("Error Reason", retrofitError.getCause().toString());
                } catch (Exception e) {
                    Log.e("UnKnownError", "RankFragment / getRanking");
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserBallonRankDialog dialog = new UserBallonRankDialog(context, array.get(position).artistId);
        dialog.show();
    }
}