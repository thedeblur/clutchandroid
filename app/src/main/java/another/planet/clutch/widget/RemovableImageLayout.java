package another.planet.clutch.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import another.planet.clutch.R;
import another.planet.clutch.util.UnitSizeUtil;

/**
 * Created by fleeon on 12/15/14.
 */
public class RemovableImageLayout extends RelativeLayout {

    private Context context;

    private ImageView imageView, frameView, removeView;

    public RemovableImageLayout(Context context) {
        super(context);
        init(context);
    }

    public RemovableImageLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RemovableImageLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        int size = UnitSizeUtil.dp2px(context, 68);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(size, size);
        setLayoutParams(layoutParams);

        imageView = new ImageView(context);
        int imageSize = UnitSizeUtil.dp2px(context, 52);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        RelativeLayout.LayoutParams imageLayoutParams = new LayoutParams(imageSize, imageSize);
        imageLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        imageView.setLayoutParams(imageLayoutParams);
        addView(imageView);

        frameView = new ImageView(context);
        frameView.setImageResource(R.drawable.favorite_frame);
        RelativeLayout.LayoutParams frameLayoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        frameLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        frameView.setLayoutParams(frameLayoutParams);
        addView(frameView);

        removeView = new ImageView(context);
        removeView.setImageResource(R.drawable.favorite_remove);
        RelativeLayout.LayoutParams removeLayoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        removeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        removeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        removeView.setLayoutParams(removeLayoutParams);
        addView(removeView);
    }

    public void setImageBitmap(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    public void setImageResource(int resource) {
        imageView.setImageResource(resource);
    }

    public void setRemoveButtonClickListener(OnClickListener listener) {
        removeView.setOnClickListener(listener);
    }
}
