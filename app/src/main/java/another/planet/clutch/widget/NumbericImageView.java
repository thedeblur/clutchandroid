package another.planet.clutch.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import another.planet.clutch.R;

/**
 * Copyright 2014 Deblur. All rights reserved.
 *
 * @author Shin Gwangsu (maruroid@gmail.com)
 * @author Park Jinseo (parkjs814@gmail.com)
 * @since 14. 12. 3.
 */
public class NumbericImageView extends LinearLayout {

    public static final int ACTIONBAR_BALLON_COUNT = 0;
    public static final int CARD_BALLON_COUNT = 1;
    public static final int RANK_NUMBER_TOP = 2;
    public static final int RANK_NUMBER_DEFAULT = 3;
    
    private Context context;
    private String numberText;
    private int numberTextStyle = 0;

    public NumbericImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.NumbericImageView);
        numberText = typedArray.getString(R.styleable.NumbericImageView_setNumberText);
 
        numberTextStyle = typedArray.getInt(R.styleable.NumbericImageView_numberTextStyle, 0);
        
        initializeView(numberTextStyle);
    }

    public NumbericImageView(Context context) {
        super(context);
        this.context = context;
    }


    private void initializeView(int numberTextStyle) {
        removeAllViews();

        for(int i = 0; i < numberText.length(); i++){
            ImageView numberView = new ImageView(context);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), getNumberRes(numberTextStyle ,numberText.charAt(i)));
            numberView.setLayoutParams(new LayoutParams(bitmap.getWidth(), bitmap.getHeight()));
            numberView.setImageBitmap(bitmap);
            setGravity(Gravity.BOTTOM);
            addView(numberView);
        }
    }

    
    public String getNumberText() {
        return numberText;
    }

    public void setNumberText(String numberText) {
        this.numberText = numberText;

        initializeView(numberTextStyle);
    }

    public int getNumberTextStyle() {
        return numberTextStyle;
    }

    public void setNumberTextStyle(int numberTextStyle) {
        this.numberTextStyle = numberTextStyle;

        initializeView(numberTextStyle);
    }

    private int getNumberRes(int style, char numText) {

        if(style == ACTIONBAR_BALLON_COUNT){
            switch(numText){
                case '0':
                    return R.drawable.rank_number_0;
                case '1':
                    return R.drawable.rank_number_1;
                case '2':
                    return R.drawable.rank_number_2;
                case '3':
                    return R.drawable.rank_number_3;
                case '4':
                    return R.drawable.rank_number_4;
                case '5':
                    return R.drawable.rank_number_5;
                case '6':
                    return R.drawable.rank_number_6;
                case '7':
                    return R.drawable.rank_number_7;
                case '8':
                    return R.drawable.rank_number_8;
                case '9':
                    return R.drawable.rank_number_9;
                default:
                    return R.drawable.rank_number_dot;
            }
        }else if(style == CARD_BALLON_COUNT){
                switch(numText){
                    case '0':
                        return R.drawable.rank_number_0;
                    case '1':
                        return R.drawable.rank_number_1;
                    case '2':
                        return R.drawable.rank_number_2;
                    case '3':
                        return R.drawable.rank_number_3;
                    case '4':
                        return R.drawable.rank_number_4;
                    case '5':
                        return R.drawable.rank_number_5;
                    case '6':
                        return R.drawable.rank_number_6;
                    case '7':
                        return R.drawable.rank_number_7;
                    case '8':
                        return R.drawable.rank_number_8;
                    case '9':
                        return R.drawable.rank_number_9;
                    default:
                        return R.drawable.rank_number_dot;
                }
        }else if(style == RANK_NUMBER_TOP){
            switch(numText){
                case '0':
                    return R.drawable.rank_top_number_10;
                case '1':
                    return R.drawable.rank_top_number_1;
                case '2':
                    return R.drawable.rank_top_number_2;
                case '3':
                    return R.drawable.rank_top_number_3;
                case '4':
                    return R.drawable.rank_top_number_4;
                case '5':
                    return R.drawable.rank_top_number_5;
                case '6':
                    return R.drawable.rank_top_number_6;
                case '7':
                    return R.drawable.rank_top_number_7;
                case '8':
                    return R.drawable.rank_top_number_8;
                case '9':
                    return R.drawable.rank_top_number_9;
                default:
                    return R.drawable.rank_number_dot;
            }
        }else if(style == RANK_NUMBER_DEFAULT){
            switch(numText){
                case '0':
                    return R.drawable.rank_number_0;
                case '1':
                    return R.drawable.rank_number_1;
                case '2':
                    return R.drawable.rank_number_2;
                case '3':
                    return R.drawable.rank_number_3;
                case '4':
                    return R.drawable.rank_number_4;
                case '5':
                    return R.drawable.rank_number_5;
                case '6':
                    return R.drawable.rank_number_6;
                case '7':
                    return R.drawable.rank_number_7;
                case '8':
                    return R.drawable.rank_number_8;
                case '9':
                    return R.drawable.rank_number_9;
                default:
                    return R.drawable.rank_number_dot;
            }
        }else{
            switch(numText){
                case '0':
                    return R.drawable.rank_number_0;
                case '1':
                    return R.drawable.rank_number_1;
                case '2':
                    return R.drawable.rank_number_2;
                case '3':
                    return R.drawable.rank_number_3;
                case '4':
                    return R.drawable.rank_number_4;
                case '5':
                    return R.drawable.rank_number_5;
                case '6':
                    return R.drawable.rank_number_6;
                case '7':
                    return R.drawable.rank_number_7;
                case '8':
                    return R.drawable.rank_number_8;
                case '9':
                    return R.drawable.rank_number_9;
                default:
                    return R.drawable.rank_number_dot;
            }
        }
    }
}
