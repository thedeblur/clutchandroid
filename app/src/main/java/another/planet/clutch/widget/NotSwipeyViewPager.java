package another.planet.clutch.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Marutian on 2014. 1. 23..
 */
public class NotSwipeyViewPager extends ViewPager {

    public NotSwipeyViewPager(Context context) {
        super(context);
    }

    public NotSwipeyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        // Never allow swiping to switch_chkbox between pages
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch_chkbox between pages
        return false;

    }
}