package another.planet.clutch.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Copyright 2014 Deblur. All rights reserved.
 *
 * @author Shin Gwangsu (maruroid@gmail.com)
 * @since 14. 12. 10.
 */
public class RoundedImageView extends ImageView {

    static float strokeWidth;

    public RoundedImageView(Context context) {
        super(context);
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RoundedImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();

        strokeWidth = 1;
        if(drawable == null) return;
        if(getWidth() == 0 || getHeight() == 0) return;

        Bitmap b = ((BitmapDrawable)drawable).getBitmap();
        Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);

        int width = getWidth();
        int height = getHeight();

        Bitmap roundBitmap = getCroppedBitmap(bitmap, width);

        Paint strokePaint = new Paint();
        strokePaint.setAntiAlias(true);
        strokePaint.setColor(0xffe7e7e7);
        strokePaint.setStrokeWidth(strokeWidth);
        strokePaint.setStyle(Paint.Style.STROKE);

        canvas.drawCircle(getWidth() / 2, getWidth() / 2, getWidth() / 2, strokePaint)
        ;

        canvas.drawBitmap(roundBitmap, 0, 0, null);
    }

    public static Bitmap getCroppedBitmap(Bitmap bitmap, int radius){
        Bitmap sbmp;
        if(bitmap.getWidth() != radius || bitmap.getHeight() != radius)
            sbmp = Bitmap.createScaledBitmap(bitmap, radius, radius, false);
        else
            sbmp = bitmap;

        Bitmap outputBitmap = Bitmap.createBitmap(sbmp.getWidth(), sbmp.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(outputBitmap);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, sbmp.getWidth(), sbmp.getHeight());

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.parseColor("#FFFFFF"));
        canvas.drawCircle(sbmp.getWidth() / 2 , sbmp.getHeight() / 2 ,
                sbmp.getWidth() / 2  - strokeWidth, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        canvas.drawBitmap(sbmp, rect, rect, paint);


        return outputBitmap;
    }
}
